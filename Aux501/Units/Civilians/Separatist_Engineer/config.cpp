class cfgPatches
{
    class Aux501_Patch_Units_Civilians_Separatist_Engineer
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units",
            "A3_data_F",
            "A3_anims_F",
            "A3_weapons_F",
            "A3_characters_F"
        };
        units[] = 
        {
            "Aux501_Units_Civilians_Separatist_Engineer"
        };
        weapons[] = {};
    };
};

class CfgVehicles
{
    class Aux501_Units_Civilians_Republic_Engineer;

    class Aux501_Units_Civilians_Separatist_Engineer: Aux501_Units_Civilians_Republic_Engineer
    {
        model = "\a3\Characters_F_Enoch\Uniforms\CBRN_Suit_01_F.p3d";
        hiddenSelectionsTextures[] = {"\a3\Characters_F_Enoch\Uniforms\Data\CBRN_Suit_01_CO.paa"};
        uniformClass = "U_C_CBRN_Suit_01_Blue_F";
        editorSubcategory = "Aux501_Editor_Subcategory_Confederacy";
        headgearList[] = {};
        linkedItems[] =  
        {
            "V_Pocketed_black_F",
            "G_AirPurifyingRespirator_01_F",
            "ItemWatch"
        };
        respawnlinkedItems[] =  
        {
            "V_Pocketed_black_F",
            "G_AirPurifyingRespirator_01_F",
            "ItemWatch"
        };
        Items[] =
        {
            "JLTS_intel_holoProjector",
            "JLTS_intel_datacard",
            "JLTS_intel_datapad_civ",
            "JLTS_credit_card"
        };
        RespawnItems[] =
        {
            "JLTS_intel_holoProjector",
            "JLTS_intel_datacard",
            "JLTS_intel_datapad_civ",
            "JLTS_credit_card"
        };
        class Wounds
        {
            tex[] = {};
            mat[] = 
            {
                "a3\Characters_F_Enoch\Uniforms\Data\CBRN_Suit_01_F.rvmat",
                "a3\Characters_F_Enoch\Uniforms\Data\CBRN_Suit_01_injury_F.rvmat",
                "a3\Characters_F_Enoch\Uniforms\Data\CBRN_Suit_01_injury_F.rvmat",
                "A3\Characters_F\Common\Data\basicbody.rvmat",
                "A3\Characters_F\Common\Data\basicbody_injury.rvmat",
                "A3\Characters_F\Common\Data\basicbody_injury.rvmat",
                "a3\characters_f\heads\data\hl_white.rvmat",
                "a3\characters_f\heads\data\hl_white_injury.rvmat",
                "a3\characters_f\heads\data\hl_white_injury.rvmat",
                "A3\Characters_F\Heads\Data\hl_white_bald_muscular.rvmat",
                "A3\Characters_F\Heads\Data\hl_white_bald_muscular_injury.rvmat",
                "A3\Characters_F\Heads\Data\hl_white_bald_muscular_injury.rvmat",
                "A3\Characters_F\Heads\Data\hl_white_02_bald_muscular.rvmat",
                "A3\Characters_F\Heads\Data\hl_white_02_bald_muscular_injury.rvmat",
                "A3\Characters_F\Heads\Data\hl_white_02_bald_muscular_injury.rvmat",
                "A3\Characters_F\Heads\Data\hl_black_bald_muscular.rvmat",
                "A3\Characters_F\Heads\Data\hl_black_bald_muscular_injury.rvmat",
                "A3\Characters_F\Heads\Data\hl_black_bald_muscular_injury.rvmat",
                "A3\Characters_F\Heads\Data\hl_white_hairy_muscular.rvmat",
                "A3\Characters_F\Heads\Data\hl_white_hairy_muscular_injury.rvmat",
                "A3\Characters_F\Heads\Data\hl_white_hairy_muscular_injury.rvmat",
                "A3\Characters_F\Heads\Data\hl_white_old.rvmat",
                "A3\Characters_F\Heads\Data\hl_white_old_injury.rvmat",
                "A3\Characters_F\Heads\Data\hl_white_old_injury.rvmat",
                "A3\Characters_F\Heads\Data\hl_asian_bald_muscular.rvmat",
                "A3\Characters_F\Heads\Data\hl_asian_bald_muscular_injury.rvmat",
                "A3\Characters_F\Heads\Data\hl_asian_bald_muscular_injury.rvmat",
                "A3\Characters_F_Exp\Heads\Data\hl_tanoan_bald_muscular.rvmat",
                "A3\Characters_F_Exp\Heads\Data\hl_tanoan_bald_muscular_injury.rvmat",
                "A3\Characters_F_Exp\Heads\Data\hl_tanoan_bald_muscular_injury.rvmat",
                "A3\Characters_F_Exp\Heads\Data\hl_asian_02_bald_muscular.rvmat",
                "A3\Characters_F_Exp\Heads\Data\hl_asian_02_bald_muscular_injury.rvmat",
                "A3\Characters_F_Exp\Heads\Data\hl_asian_02_bald_muscular_injury.rvmat"
            };
        };
    };
};