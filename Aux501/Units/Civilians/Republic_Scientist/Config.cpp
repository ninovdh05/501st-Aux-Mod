class cfgPatches
{
    class Aux501_Patch_Units_Civilians_Republic_Scientist
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units",
            "Aux501_Patch_Units_Civilians_Republic_Engineer",
            "A3_data_F",
            "A3_anims_F",
            "A3_weapons_F",
            "A3_characters_F"
        };
        units[] = 
        {
            "Aux501_Units_Civilians_Republic_Scientist"
        };
        weapons[] = {};
    };
};

class CfgVehicles
{
    class Aux501_Units_Civilians_Republic_Engineer;

    class Aux501_Units_Civilians_Republic_Scientist: Aux501_Units_Civilians_Republic_Engineer
    {
        scope = 2;
        scopecurator = 2;
        displayname = "Scientist";
        model = "\optre_unsc_units\oni\research.p3d";
        hiddenSelectionsTextures[] = {"Aux501\Units\Civilians\Gear\Uniforms\data\textures\republic_scientist_uniform.paa"};
        hiddenSelectionsMaterials[] = {};
        uniformClass = "Aux501_Units_Civilians_Uniforms_Republic_Scientist";
        headgearList[] = {};
        identityTypes[] = 
        {
            "LanguageENGB_F",
            "Head_EURO",
            "Head_Asian",
            "Head_Greek",
            "Head_m_mirialan",
            "Head_m_zelosian",
            "Head_m_Zabrak"
        };
        linkedItems[] =  
        {
            "ItemWatch"
        };
        respawnlinkedItems[] =  
        {
            "ItemWatch"
        };
        Items[] =
        {
            "JLTS_intel_holoProjector",
            "JLTS_intel_datacard",
            "JLTS_intel_datapad_civ",
            "JLTS_credit_card",
            "JLTS_ids_gar_army"
        };
        RespawnItems[] =
        {
            "JLTS_intel_holoProjector",
            "JLTS_intel_datacard",
            "JLTS_intel_datapad_civ",
            "JLTS_credit_card",
            "JLTS_ids_gar_army"
        };
    };
};