class cfgPatches
{
    class Aux501_Patch_Units_Republic_91_Vests_Phase2
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units",
            "A3_data_F",
            "A3_anims_F",
            "A3_weapons_F",
            "A3_characters_F",
            "Aux501_Patch_Units_Republic_501_Infantry_Vests_Phase2"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_Units_Republic_91_Vests_Trooper",
            "Aux501_Units_Republic_91_Vests_CP",
            "Aux501_Units_Republic_91_Vests_CS",
            "Aux501_Units_Republic_91_Vests_2Lt"
        };
    };
};

class CfgWeapons
{
    class Aux501_Units_Republic_501_Infantry_Vests_Vet_Trooper;
    class Aux501_Units_Republic_501_Infantry_Vests_CP;
    class Aux501_Units_Republic_501_Infantry_Vests_CS;
    class Aux501_Units_Republic_501_Infantry_Vests_2LT;
    
    class Aux501_Units_Republic_91_Vests_Trooper: Aux501_Units_Republic_501_Infantry_Vests_Vet_Trooper
    {
        scope = 2;
        scopeArsenal = 2;
        displayname = "[91st] VEST 01 - Trooper";
        hiddenSelectionsTextures[] = {"Aux501\Units\Republic\91st\Vests\Phase2\data\textures\91_nco_vest_co.paa"};
    };

    class Aux501_Units_Republic_91_Vests_CP: Aux501_Units_Republic_501_Infantry_Vests_CP
    {
        scope = 2;
        scopeArsenal = 2;
        displayname = "[91st] VEST 02 - CP";
        hiddenSelectionsTextures[] = {"Aux501\Units\Republic\91st\Vests\Phase2\data\textures\91_nco_vest_co.paa"};
    };
    
    class Aux501_Units_Republic_91_Vests_CS: Aux501_Units_Republic_501_Infantry_Vests_CS
    {
        scope = 2;
        scopeArsenal = 2;
        displayname = "[91st] VEST 03 - CS";
        hiddenSelectionsTextures[] = 
        {
            "MRC\JLTS\characters\CloneArmor\data\Clone_vest_officer_co.paa",
            "Aux501\Units\Republic\91st\Vests\Phase2\data\textures\91_nco_vest_co.paa"
        };
    };

    class Aux501_Units_Republic_91_Vests_2Lt: Aux501_Units_Republic_501_Infantry_Vests_2LT
    {
        scope = 2;
        scopeArsenal = 2;
        displayname = "[91st] VEST 04 - Officer";
        hiddenSelectionsTextures[] = 
        {
            "\MRC\JLTS\characters\CloneLegions\data\Clone_91stOfficer_vest_co.paa"
        };
    };
};