class cfgPatches
{
    class Aux501_Patch_Units_Republic_91_Helmets_Phase2
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units",
            "A3_data_F",
            "A3_anims_F",
            "A3_weapons_F",
            "A3_characters_F",
            "Aux501_Patch_Units_Republic_501_Infantry_Helmets_Phase2"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_Units_Republic_91_Infantry_Helmet_Trooper"
        };
    };
};

class CfgWeapons
{
    class Aux501_Units_Republic_501_Infantry_Helmet_Base;

    class Aux501_Units_Republic_91_Infantry_Helmet_Trooper: Aux501_Units_Republic_501_Infantry_Helmet_Base
    {
        scope = 2;
        scopeArsenal = 2;
        picture = "\SWLB_units\data\ui\icon_SWLB_clone_91stTrooper_helmet_ca.paa";   
        displayName = "[91st] P2 HELM 01";
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\91st\Helmets\Phase2\data\textures\91_ct_helmet_co.paa"};
    };
};