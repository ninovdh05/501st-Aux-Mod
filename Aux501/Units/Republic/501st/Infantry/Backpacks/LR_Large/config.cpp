class cfgPatches
{
    class Aux501_Patch_Units_Republic_501_Infantry_Backpacks_LR_Large
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units",
            "A3_data_F",
            "A3_anims_F",
            "A3_weapons_F",
            "A3_characters_F"
        };
        units[] = 
        {
            "Aux501_Units_Republic_501_Infantry_Backpacks_LR_Large",
            "Aux501_Units_Republic_501_Infantry_Backpacks_LR_Large_Straps",
            "Aux501_Units_Republic_501_Infantry_Backpacks_LR_Large_Medic",
            "Aux501_Units_Republic_501_Infantry_Backpacks_LR_Large_Medic_Straps"
        };
        weapons[] = {};
    };
};

class CfgVehicles
{
    class Aux501_Units_Republic_501_Infantry_Backpacks_base;

    class Aux501_Units_Republic_501_Infantry_Backpacks_LR_Large: Aux501_Units_Republic_501_Infantry_Backpacks_base
    {
        scope = 1;
        scopeArsenal = 1;
        picture = "\MRC\JLTS\characters\CloneArmor\data\ui\Clone_backpack_ui_ca.paa";
        displayName = "[501st] RTO LR Large 01";
        model = "\MRC\JLTS\characters\CloneArmor\CloneBackpackRTO.p3d";
        hiddenSelectionsTextures[] = {"\MRC\JLTS\characters\CloneArmor\data\Clone_backpack_co.paa"};
        tf_dialog = "JLTS_clone_rto_radio_dialog";
        tf_dialogUpdate = "call TFAR_fnc_updateLRDialogToChannel;";
        tf_encryptionCode = "tf_west_radio_code";
        tf_hasLRradio = 1;
        tf_range = 35000;
        tf_subtype = "digital_lr";
        class Attributes
        {
            class staticRadioFrequency
            {
                displayName = "$STR_tfar_static_radios_moduleStaticRadio_FreqTitle";
                tooltip = "$STR_tfar_static_radios_moduleStaticRadio_ATT_Frequency_tooltip";
                property = "staticRadioFrequency";
                control = "Edit";
                expression = "if (isMultiplayer) then {[_this,call compile _value] call TFAR_static_radios_fnc_setFrequencies}";
                defaultValue = "str (_this call TFAR_static_radios_fnc_generateFrequencies)";
                validate = "none";
                condition = "objectHasInventoryCargo";
                typeName = "STRING";
            };
            class staticRadioChannel
            {
                displayName = "$STR_tfar_static_radios_moduleStaticRadio_ChannelTitle";
                tooltip = "$STR_tfar_static_radios_moduleStaticRadio_ATT_Channel_tooltip";
                property = "staticRadioChannel";
                control = "Edit";
                expression = "if (isMultiplayer) then {[_this,_value] call TFAR_static_radios_fnc_setActiveChannel}";
                defaultValue = "'1'";
                validate = "none";
                condition = "objectHasInventoryCargo";
                typeName = "NUMBER";
            };
            class staticRadioSpeaker
            {
                displayName = "$STR_tfar_static_radios_moduleStaticRadio_ATT_SpeakerEnabled";
                tooltip = "$STR_tfar_static_radios_moduleStaticRadio_ATT_SpeakerEnabled_tooltip";
                property = "staticRadioSpeaker";
                control = "Checkbox";
                expression = "if (isMultiplayer) then {[_this,_value] call TFAR_static_radios_fnc_setSpeakers}";
                defaultValue = "false";
                validate = "none";
                condition = "objectHasInventoryCargo";
                typeName = "NUMBER";
            };
            class staticRadioVolume
            {
                displayName = "$STR_tfar_static_radios_moduleStaticRadio_ATT_RadioVolume";
                tooltip = "$STR_tfar_static_radios_moduleStaticRadio_ATT_RadioVolume_tooltip";
                property = "staticRadioVolume";
                control = "tfar_static_radios_volumeSlider";
                expression = "if (isMultiplayer) then {[_this,_value] call TFAR_static_radios_fnc_setVolume}";
                defaultValue = 7;
                validate = "none";
                condition = "objectHasInventoryCargo";
                typeName = "NUMBER";
            };
        };
    };
    class Aux501_Units_Republic_501_Infantry_Backpacks_LR_Large_Straps: Aux501_Units_Republic_501_Infantry_Backpacks_LR_Large
    {
        displayName = "[501st] RTO LR Large 02 - Straps";
        model = "\MRC\JLTS\characters\CloneArmor\CloneBackpackRTOStraps.p3d";
    };
    class Aux501_Units_Republic_501_Infantry_Backpacks_LR_Large_Medic: Aux501_Units_Republic_501_Infantry_Backpacks_LR_Large
    {
        displayName = "[501st] MED LR Large 01";
        hiddenSelectionsTextures[] = {"\MRC\JLTS\characters\CloneArmor\data\Clone_backpack_medic_co.paa"};
        maximumload = 1000;
    };
    class Aux501_Units_Republic_501_Infantry_Backpacks_LR_Large_Medic_Straps: Aux501_Units_Republic_501_Infantry_Backpacks_LR_Large_Medic
    {
        displayName = "[501st] MED LR Large 02 - Straps";
        model = "\MRC\JLTS\characters\CloneArmor\CloneBackpackRTOStraps.p3d";
    };
};