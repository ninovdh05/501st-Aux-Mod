class cfgPatches
{
    class Aux501_Patch_Units_Republic_501_Infantry
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units",
            "A3_data_F",
            "A3_anims_F",
            "A3_weapons_F",
            "A3_characters_F"
        };
        units[] = 
        {
            "Aux501_Units_Republic_501st_Unit_Base",
            "Aux501_Units_Republic_501st_Recruit_Unit",
            "Aux501_Units_Republic_501st_Cadet_Unit",
            "Aux501_Units_Republic_501st_Trooper_Unit",
            "Aux501_Units_Republic_501st_Snr_Trooper_Unit",
            "Aux501_Units_Republic_501st_Vet_Trooper_Unit",
            "Aux501_Units_Republic_501st_Lance_CP_Unit",
            "Aux501_Units_Republic_501st_CP_Unit",
            "Aux501_Units_Republic_501st_Snr_CP_Unit",
            "Aux501_Units_Republic_501st_CS_Unit",
            "Aux501_Units_Republic_501st_Snr_CS_Unit",
            "Aux501_Units_Republic_501st_Platoon_CSM_Unit",
            "Aux501_Units_Republic_501st_2LT_Unit",
            "Aux501_Units_Republic_501st_1LT_Unit",
            "Aux501_Units_Republic_501st_Captain_Unit"
        };
        weapons[] = {};
    };
};

class CfgVehicles
{
    class B_Soldier_base_f;
    class B_Soldier_f: B_Soldier_base_f
    {
        class HitPoints;
    };

    //Troopers
    class Aux501_Units_Republic_501st_Unit_Base: B_Soldier_f
    {
        scope = 1;
        scopecurator = 1;
        author = "501st Aux Team";
        displayName = "";
        genericNames = "Aux501_CloneRecruit_Names";
        identityTypes[] = {"LanguageENGB_F","Head_LSD_CLONE"};
        faction = "Aux501_FactionClasses_Republic";
        editorCategory = "Aux501_Editor_Category_Republic";
        editorSubcategory = "Aux501_Editor_Subcategory_501st_Infantry_Clones";
        editorPreview = "";
        model = "\MRC\JLTS\characters\CloneArmor\CloneArmor.p3d";
        hiddenSelections[] = {"camo1","camo2","insignia"};
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\inf_cr_armor_upper_co.paa",
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\inf_cr_armor_lower_co.paa"
        };
        hiddenSelectionsMaterials[] = 
        {
            "MRC\JLTS\characters\CloneArmor\data\clone_armor1_clean.rvmat",
            "MRC\JLTS\characters\CloneArmor\data\clone_armor2_clean.rvmat",
            "MRC\JLTS\Core_mod\data\insignias\insignia_CloneArmor.rvmat"
        };
        uniformClass = "Aux501_Units_Republic_501st_Standard_Uniform";
        nakedUniform = "Aux501_Units_Republic_Undersuit_Uniform";
        backpack = "";
        linkedItems[] = {"ItemMap","JLTS_clone_comlink","ItemCompass","ItemWatch"};
        respawnLinkedItems[] = {"ItemMap","JLTS_clone_comlink","ItemCompass","ItemWatch"};
        weapons[] = {"Throw","Put"};
        respawnWeapons[] = {"Throw","Put"};
        magazines[] = {};
        respawnMagazines[] = {};
        items[] = {};
        respawnItems[] = {};

        impactEffectsBlood = "ImpactPlastic";
        impactEffectsNoBlood = "ImpactPlastic";
        maxSpeed = 24;
        maxTurnAngularVelocity = 3;
        lyingLimitSpeedHiding = 0.8;
        crouchProbabilityHiding = 0.8;
        lyingLimitSpeedCombat = 1.8;
        crouchProbabilityCombat = 0.4;
        crouchProbabilityEngage = 0.75;
        lyingLimitSpeedStealth = 2;
        cost = 100000;
        canHideBodies = 1;
        detectSkill = 12;
        audible = 0.04;
        camouflage = 0.4;
        accuracy = 0.7;
        sensitivityEar = 0.125;
        sensitivity = 1.75;
        threat[] = {1,0.5,0.1};
        role = "Rifleman";
        canCarryBackPack = 1;

        armor = 2;
        armorStructural = 4;
        explosionShielding = 40.4;
        minTotalDamageThreshold = 0.001;
        impactDamageMultiplier = 0.5;
        class HitPoints: HitPoints
        {
            class HitFace
            {
                armor = 1;
                material = -1;
                name = "face_hub";
                passThrough = 0.8;
                radius = 0.08;
                explosionShielding = 0.1;
                minimalHit = 0.01;
            };
            class HitNeck: HitFace
            {
                armor = 1;
                material = -1;
                name = "neck";
                passThrough = 0.8;
                radius = 0.1;
                explosionShielding = 0.5;
                minimalHit = 0.01;
            };
            class HitHead: HitNeck
            {
                armor = 1;
                material = -1;
                name = "head";
                passThrough = 0.8;
                radius = 0.2;
                explosionShielding = 0.5;
                minimalHit = 0.01;
                depends = "HitFace max HitNeck";
            };
            class HitPelvis: HitHead
            {
                armor = 8;
                material = -1;
                name = "pelvis";
                passThrough = 0.8;
                radius = 0.24;
                explosionShielding = 3;
                visual = "injury_body";
                minimalHit = 0.01;
                depends = "";
            };
            class HitAbdomen: HitPelvis
            {
                armor = 6;
                material = -1;
                name = "spine1";
                passThrough = 0.8;
                radius = 0.16;
                explosionShielding = 3;
                visual = "injury_body";
                minimalHit = 0.01;
            };
            class HitDiaphragm: HitAbdomen
            {
                armor = 6;
                material = -1;
                name = "spine2";
                passThrough = 0.33;
                radius = 0.18;
                explosionShielding = 6;
                visual = "injury_body";
                minimalHit = 0.01;
            };
            class HitChest: HitDiaphragm
            {
                armor = 8;
                material = -1;
                name = "spine3";
                passThrough = "0.33000001radius = 0.18";
                explosionShielding = 6;
                visual = "injury_body";
                minimalHit = 0.01;
            };
            class HitBody: HitChest
            {
                armor = 1000;
                material = -1;
                name = "body";
                passThrough = 1;
                radius = 0;
                explosionShielding = 6;
                visual = "injury_body";
                minimalHit = 0.01;
                depends = "HitPelvis max HitAbdomen max HitDiaphragm max HitChest";
            };
            class HitArms: HitBody
            {
                armor = 6;
                material = -1;
                name = "arms";
                passThrough = 1;
                radius = 0.1;
                explosionShielding = 3;
                visual = "injury_hands";
                minimalHit = 0.01;
                depends = "0";
            };
            class HitHands: HitArms
            {
                armor = 6;
                material = -1;
                name = "hands";
                passThrough = 1;
                radius = 0.1;
                explosionShielding = 1;
                visual = "injury_hands";
                minimalHit = 0.01;
                depends = "HitArms";
            };
            class HitLegs: HitHands
            {
                armor = 6;
                material = -1;
                name = "legs";
                passThrough = 1;
                radius = 0.14;
                explosionShielding = 3;
                visual = "injury_legs";
                minimalHit = 0.01;
                depends = "0";
            };
            class Incapacitated: HitLegs
            {
                armor = 1000;
                material = -1;
                name = "body";
                passThrough = 1;
                radius = 0;
                explosionShielding = 3;
                visual = "";
                minimalHit = 0;
                depends = "(((Total - 0.25) max 0) + ((HitHead - 0.25) max 0) + ((HitBody - 0.25) max 0)) * 2";
            };
            class HitLeftArm
            {
                armor = 6;
                material = -1;
                name = "hand_l";
                passThrough = 1;
                radius = 0.08;
                explosionShielding = 3;
                visual = "injury_hands";
                minimalHit = 0.01;
            };
            class HitRightArm: HitLeftArm
            {
                name = "hand_r";
            };
            class HitLeftLeg
            {
                armor = 6;
                material = -1;
                name = "leg_l";
                passThrough = 1;
                radius = 0.1;
                explosionShielding = 3;
                visual = "injury_legs";
                minimalHit = 0.01;
            };
            class HitRightLeg: HitLeftLeg
            {
                name = "leg_r";
            };
        };
    };
    class Aux501_Units_Republic_501st_Recruit_Unit: Aux501_Units_Republic_501st_Unit_Base
    {
        scope = 2;
        scopecurator = 2;
        displayName = "Recruit";
        weapons[] = {"Aux501_Weaps_DC15A","Throw","Put"};
        respawnWeapons[] = {"Aux501_Weaps_DC15A","Throw","Put"};
        magazines[] = 
        {
            "Aux501_Weapons_Mags_20mwdp30",
            "Aux501_Weapons_Mags_20mwdp30",
            "Aux501_Weapons_Mags_20mwdp30",
            "Aux501_Weapons_Mags_20mwdp30",
            "Aux501_Weapons_Mags_20mwdp30",
            "Aux501_Weapons_Mags_20mwdp30",
            "Aux501_Weapons_Mags_20mwdp30",
            "Aux501_Weapons_Mags_20mwdp30",
            "Aux501_Weapons_Mags_20mwdp30",
            "Aux501_Weapons_Mags_20mwdp30",
            "Aux501_Weapons_Mags_20mwdp30"
        };
        respawnMagazines[] = 
        {
            "Aux501_Weapons_Mags_20mwdp30",
            "Aux501_Weapons_Mags_20mwdp30",
            "Aux501_Weapons_Mags_20mwdp30",
            "Aux501_Weapons_Mags_20mwdp30",
            "Aux501_Weapons_Mags_20mwdp30",
            "Aux501_Weapons_Mags_20mwdp30",
            "Aux501_Weapons_Mags_20mwdp30",
            "Aux501_Weapons_Mags_20mwdp30",
            "Aux501_Weapons_Mags_20mwdp30",
            "Aux501_Weapons_Mags_20mwdp30",
            "Aux501_Weapons_Mags_20mwdp30"
        };
        linkedItems[] = 
        {
            "Aux501_Units_Republic_501_Infantry_Blank_Helmet",

            "Aux501_Units_Republic_501_Infantry_Vests_Nanoweave",

            "Aux501_Republic_501_Infantry_Equipment_NVGs_Integrated_NV",

            "JLTS_clone_comlink",
            "ACE_Altimeter",
            "ItemMap",
            "ItemCompass",
            "ItemMicroDAGR"
        };
        respawnLinkedItems[] =
        {
            "Aux501_Units_Republic_501_Infantry_Blank_Helmet",

            "Aux501_Units_Republic_501_Infantry_Vests_Nanoweave",

            "Aux501_Republic_501_Infantry_Equipment_NVGs_Integrated_NV",

            "JLTS_clone_comlink",
            "ACE_Altimeter",
            "ItemMap",
            "ItemCompass",
            "ItemMicroDAGR"
        };
        Items[] =
        {
            "JLTS_intel_holoProjector",
            "JLTS_ids_gar_army"
        };
        respawnItems[] =
        {
            "JLTS_intel_holoProjector",
            "JLTS_ids_gar_army"
        };
        uniformClass = "Aux501_Units_Republic_501st_Standard_Uniform";
        backpack = "Aux501_Units_Republic_501_Infantry_Backpacks_Invisible";
    };
    class Aux501_Units_Republic_501st_Cadet_Unit: Aux501_Units_Republic_501st_Recruit_Unit
    {
        displayName = "Cadet";
        genericNames = "Aux501_CloneCadet_Names";
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\inf_crc_armor_upper_co.paa",
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\inf_crc_armor_lower_co.paa"
        };
        weapons[] = {"Aux501_Weaps_DC15C","Throw","Put"};
        respawnWeapons[] = {"Aux501_Weaps_DC15C","Throw","Put"};
        magazines[] = 
        {
            "Aux501_Weapons_Mags_20mw40",
            "Aux501_Weapons_Mags_20mw40",
            "Aux501_Weapons_Mags_20mw40",
            "Aux501_Weapons_Mags_20mw40",
            "Aux501_Weapons_Mags_20mw40",
            "Aux501_Weapons_Mags_20mw40",
            "Aux501_Weapons_Mags_20mw40",
            "Aux501_Weapons_Mags_20mw40",
            "Aux501_Weapons_Mags_20mw40",
            "Aux501_Weapons_Mags_20mw40",
            "Aux501_Weapons_Mags_20mw40"
        };
        respawnMagazines[] = 
        {
            "Aux501_Weapons_Mags_20mw40",
            "Aux501_Weapons_Mags_20mw40",
            "Aux501_Weapons_Mags_20mw40",
            "Aux501_Weapons_Mags_20mw40",
            "Aux501_Weapons_Mags_20mw40",
            "Aux501_Weapons_Mags_20mw40",
            "Aux501_Weapons_Mags_20mw40",
            "Aux501_Weapons_Mags_20mw40",
            "Aux501_Weapons_Mags_20mw40",
            "Aux501_Weapons_Mags_20mw40",
            "Aux501_Weapons_Mags_20mw40"
        };
        uniformClass = "Aux501_Units_Republic_501st_Cadet_Uniform";
    };
    class Aux501_Units_Republic_501st_Trooper_Unit: Aux501_Units_Republic_501st_Recruit_Unit
    {
        displayName = "Trooper";
        genericNames = "Aux501_Clonetrooper_Names";
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\inf_ct_armor_upper_co.paa",
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\inf_ct_armor_lower_co.paa"
        };
        weapons[] = {"Aux501_Weaps_DC15S","Throw","Put"};
        respawnWeapons[] = {"Aux501_Weaps_DC15S","Throw","Put"};
        magazines[] = 
        {
            "Aux501_Weapons_Mags_10mw50",
            "Aux501_Weapons_Mags_10mw50",
            "Aux501_Weapons_Mags_10mw50",
            "Aux501_Weapons_Mags_10mw50",
            "Aux501_Weapons_Mags_10mw50",
            "Aux501_Weapons_Mags_10mw50",
            "Aux501_Weapons_Mags_10mw50",
            "Aux501_Weapons_Mags_10mw50",
            "Aux501_Weapons_Mags_10mw50",
            "Aux501_Weapons_Mags_10mw50",
            "Aux501_Weapons_Mags_10mw50",

            "Aux501_Weapons_Mags_Thermal_Detonator"
        };
        respawnMagazines[] = 
        {
            "Aux501_Weapons_Mags_10mw50",
            "Aux501_Weapons_Mags_10mw50",
            "Aux501_Weapons_Mags_10mw50",
            "Aux501_Weapons_Mags_10mw50",
            "Aux501_Weapons_Mags_10mw50",
            "Aux501_Weapons_Mags_10mw50",
            "Aux501_Weapons_Mags_10mw50",
            "Aux501_Weapons_Mags_10mw50",
            "Aux501_Weapons_Mags_10mw50",
            "Aux501_Weapons_Mags_10mw50",
            "Aux501_Weapons_Mags_10mw50",

            "Aux501_Weapons_Mags_Thermal_Detonator"
        };
        linkedItems[] = 
        {
            "Aux501_Units_Republic_501_Infantry_Helmet_Trooper",

            "Aux501_Units_Republic_501_Infantry_Vests_Nanoweave",

            "Aux501_Republic_501_Infantry_Equipment_NVGs_Integrated_NV",

            "JLTS_clone_comlink",
            "ACE_Altimeter",
            "ItemMap",
            "ItemCompass",
            "ItemMicroDAGR"
        };
        respawnLinkedItems[] =
        {
            "Aux501_Units_Republic_501_Infantry_Helmet_Trooper",

            "Aux501_Units_Republic_501_Infantry_Vests_Nanoweave",

            "Aux501_Republic_501_Infantry_Equipment_NVGs_Integrated_NV",

            "JLTS_clone_comlink",
            "ACE_Altimeter",
            "ItemMap",
            "ItemCompass",
            "ItemMicroDAGR"
        };
        uniformClass = "Aux501_Units_Republic_501st_Trooper_Uniform";
    };
    class Aux501_Units_Republic_501st_Snr_Trooper_Unit: Aux501_Units_Republic_501st_Trooper_Unit
    {
        displayName = "Senior Trooper";
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\inf_snr_ct_armor_upper_co.paa",
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\inf_snr_ct_armor_lower_co.paa"
        };
        weapons[] = {"Aux501_Weaps_DC15C","Aux501_Weaps_DC17","Throw","Put"};
        respawnWeapons[] = {"Aux501_Weaps_DC15C","Aux501_Weaps_DC17","Throw","Put"};
        magazines[] = 
        {
            "Aux501_Weapons_Mags_20mw40",
            "Aux501_Weapons_Mags_20mw40",
            "Aux501_Weapons_Mags_20mw40",
            "Aux501_Weapons_Mags_20mw40",
            "Aux501_Weapons_Mags_20mw40",
            "Aux501_Weapons_Mags_20mw40",
            "Aux501_Weapons_Mags_20mw40",
            "Aux501_Weapons_Mags_20mw40",
            "Aux501_Weapons_Mags_20mw40",
            "Aux501_Weapons_Mags_20mw40",
            "Aux501_Weapons_Mags_20mw40",
            "Aux501_Weapons_Mags_20mwdp20",
            "Aux501_Weapons_Mags_20mwdp20",
            "Aux501_Weapons_Mags_Thermal_Detonator"
        };
        respawnMagazines[] = 
        {
            "Aux501_Weapons_Mags_20mw40",
            "Aux501_Weapons_Mags_20mw40",
            "Aux501_Weapons_Mags_20mw40",
            "Aux501_Weapons_Mags_20mw40",
            "Aux501_Weapons_Mags_20mw40",
            "Aux501_Weapons_Mags_20mw40",
            "Aux501_Weapons_Mags_20mw40",
            "Aux501_Weapons_Mags_20mw40",
            "Aux501_Weapons_Mags_20mw40",
            "Aux501_Weapons_Mags_20mw40",
            "Aux501_Weapons_Mags_20mw40",

            "Aux501_Weapons_Mags_20mwdp20",
            "Aux501_Weapons_Mags_20mwdp20",

            "Aux501_Weapons_Mags_Thermal_Detonator"
        };
        linkedItems[] = 
        {
            "Aux501_Units_Republic_501_Infantry_Helmet_Trooper",

            "Aux501_Units_Republic_501_Infantry_Vests_Standard_Holster",

            "Aux501_Republic_501_Infantry_Equipment_NVGs_Integrated_NV",

            "JLTS_clone_comlink",
            "ACE_Altimeter",
            "ItemMap",
            "ItemCompass",
            "ItemMicroDAGR"
        };
        respawnLinkedItems[] =
        {
            "Aux501_Units_Republic_501_Infantry_Helmet_Trooper",

            "Aux501_Units_Republic_501_Infantry_Vests_Standard_Holster",

            "Aux501_Republic_501_Infantry_Equipment_NVGs_Integrated_NV",

            "JLTS_clone_comlink",
            "ACE_Altimeter",
            "ItemMap",
            "ItemCompass",
            "ItemMicroDAGR"
        };
        uniformClass = "Aux501_Units_Republic_501st_Snr_Trooper_Uniform";
        backpack = "Aux501_Units_Republic_501_Infantry_Backpacks_Beltbag";
        cost = 100100;
    };
    class Aux501_Units_Republic_501st_Vet_Trooper_Unit: Aux501_Units_Republic_501st_Trooper_Unit
    {
        displayName = "Veteran Trooper";
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\inf_vet_ct_armor_upper_co.paa",
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\inf_vet_ct_armor_lower_co.paa"
        };
        weapons[] = {"Aux501_Weaps_DC15A","Aux501_Weaps_DC17","Throw","Put"};
        respawnWeapons[] = {"Aux501_Weaps_DC15A","Aux501_Weaps_DC17","Throw","Put"};
        magazines[] = 
        {
            "Aux501_Weapons_Mags_20mwdp30",
            "Aux501_Weapons_Mags_20mwdp30",
            "Aux501_Weapons_Mags_20mwdp30",
            "Aux501_Weapons_Mags_20mwdp30",
            "Aux501_Weapons_Mags_20mwdp30",
            "Aux501_Weapons_Mags_20mwdp30",
            "Aux501_Weapons_Mags_20mwdp30",
            "Aux501_Weapons_Mags_20mwdp30",
            "Aux501_Weapons_Mags_20mwdp30",
            "Aux501_Weapons_Mags_20mwdp30",
            "Aux501_Weapons_Mags_20mwdp30",
            "Aux501_Weapons_Mags_20mwdp20",
            "Aux501_Weapons_Mags_20mwdp20",

            "Aux501_Weapons_Mags_20mwdp20",
            "Aux501_Weapons_Mags_20mwdp20",

            "Aux501_Weapons_Mags_Thermal_Detonator"
        };
        respawnMagazines[] = 
        {
            "Aux501_Weapons_Mags_20mwdp30",
            "Aux501_Weapons_Mags_20mwdp30",
            "Aux501_Weapons_Mags_20mwdp30",
            "Aux501_Weapons_Mags_20mwdp30",
            "Aux501_Weapons_Mags_20mwdp30",
            "Aux501_Weapons_Mags_20mwdp30",
            "Aux501_Weapons_Mags_20mwdp30",
            "Aux501_Weapons_Mags_20mwdp30",
            "Aux501_Weapons_Mags_20mwdp30",
            "Aux501_Weapons_Mags_20mwdp30",
            "Aux501_Weapons_Mags_20mwdp30",

            "Aux501_Weapons_Mags_20mwdp20",
            "Aux501_Weapons_Mags_20mwdp20",

            "Aux501_Weapons_Mags_Thermal_Detonator"
        };
        linkedItems[] = 
        {
            "Aux501_Units_Republic_501_Infantry_Helmet_Trooper",

            "Aux501_Units_Republic_501_Infantry_Vests_Vet_Trooper",

            "Aux501_Republic_501_Infantry_Equipment_NVGs_Integrated_NV",

            "JLTS_clone_comlink",
            "ACE_Altimeter",
            "ItemMap",
            "ItemCompass",
            "ItemMicroDAGR"
        };
        respawnLinkedItems[] =
        {
            "Aux501_Units_Republic_501_Infantry_Helmet_Trooper",

            "Aux501_Units_Republic_501_Infantry_Vests_Vet_Trooper",

            "Aux501_Republic_501_Infantry_Equipment_NVGs_Integrated_NV",

            "JLTS_clone_comlink",
            "ACE_Altimeter",
            "ItemMap",
            "ItemCompass",
            "ItemMicroDAGR"
        };
        uniformClass = "Aux501_Units_Republic_501st_Vet_Trooper_Uniform";
        cost = 100250;
    };
    class Aux501_Units_Republic_501st_Lance_CP_Unit: Aux501_Units_Republic_501st_Trooper_Unit
    {
        displayName = "Lance Corporal";
        cost = 110000;
        icon = "JLTS_iconManHeavy";
        genericNames = "Aux501_CloneLanceCorporal_Names";
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\inf_clc_armor_upper_co.paa",
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\inf_clc_armor_lower_co.paa"
        };
        weapons[] = {"Aux501_Weaps_DC15C","Aux501_Weaps_DC17","Throw","Put"};
        respawnWeapons[] = {"Aux501_Weaps_DC15C","Aux501_Weaps_DC17","Throw","Put"};
        magazines[] = 
        {
            "Aux501_Weapons_Mags_20mw40",
            "Aux501_Weapons_Mags_20mw40",
            "Aux501_Weapons_Mags_20mw40",
            "Aux501_Weapons_Mags_20mw40",
            "Aux501_Weapons_Mags_20mw40",
            "Aux501_Weapons_Mags_20mw40",
            "Aux501_Weapons_Mags_20mw40",
            "Aux501_Weapons_Mags_20mw40",
            "Aux501_Weapons_Mags_20mw40",
            "Aux501_Weapons_Mags_20mw40",
            "Aux501_Weapons_Mags_20mw40",
            
            "Aux501_Weapons_Mags_20mwdp20",
            "Aux501_Weapons_Mags_20mwdp20",

            "Aux501_Weapons_Mags_Thermal_Detonator"
        };
        respawnMagazines[] = 
        {
            "Aux501_Weapons_Mags_20mw40",
            "Aux501_Weapons_Mags_20mw40",
            "Aux501_Weapons_Mags_20mw40",
            "Aux501_Weapons_Mags_20mw40",
            "Aux501_Weapons_Mags_20mw40",
            "Aux501_Weapons_Mags_20mw40",
            "Aux501_Weapons_Mags_20mw40",
            "Aux501_Weapons_Mags_20mw40",
            "Aux501_Weapons_Mags_20mw40",
            "Aux501_Weapons_Mags_20mw40",
            "Aux501_Weapons_Mags_20mw40",

            "Aux501_Weapons_Mags_20mwdp20",
            "Aux501_Weapons_Mags_20mwdp20",
            
            "Aux501_Weapons_Mags_Thermal_Detonator"
        };
        linkedItems[] = 
        {
            "Aux501_Units_Republic_501_Infantry_Helmet_Trooper",
            
            "Aux501_Units_Republic_501_Infantry_Vests_Lance_CP",

            "Aux501_Republic_501_Infantry_Equipment_NVGs_Integrated_NV_TI",

            "JLTS_clone_comlink",
            "ACE_Altimeter",
            "ItemMap",
            "ItemCompass",
            "ItemMicroDAGR"
        };
        respawnLinkedItems[] =
        {
            "Aux501_Units_Republic_501_Infantry_Helmet_Trooper",

            "Aux501_Units_Republic_501_Infantry_Vests_Lance_CP",

            "Aux501_Republic_501_Infantry_Equipment_NVGs_Integrated_NV_TI",

            "JLTS_clone_comlink",
            "ACE_Altimeter",
            "ItemMap",
            "ItemCompass",
            "ItemMicroDAGR"
        };
        uniformClass = "Aux501_Units_Republic_501st_Lance_CP_Uniform";
    };
    
    //NCOs
    class Aux501_Units_Republic_501st_CP_Unit: Aux501_Units_Republic_501st_Lance_CP_Unit
    {
        displayName = "Corporal";
        icon = "iconManLeader";
        cost = 115000;
        genericNames = "Aux501_CloneCorporal_Names";
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\inf_cp_armor_upper_co.paa",
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\inf_cp_armor_lower_co.paa"
        };
        weapons[] = {"Aux501_Weaps_DC15A","Aux501_Weaps_DC17","Throw","Put"};
        respawnWeapons[] = {"Aux501_Weaps_DC15A","Aux501_Weaps_DC17","Throw","Put"};
        magazines[] = 
        {
            "Aux501_Weapons_Mags_20mwdp30",
            "Aux501_Weapons_Mags_20mwdp30",
            "Aux501_Weapons_Mags_20mwdp30",
            "Aux501_Weapons_Mags_20mwdp30",
            "Aux501_Weapons_Mags_20mwdp30",
            "Aux501_Weapons_Mags_20mwdp30",
            "Aux501_Weapons_Mags_20mwdp30",
            "Aux501_Weapons_Mags_20mwdp30",
            "Aux501_Weapons_Mags_20mwdp30",
            "Aux501_Weapons_Mags_20mwdp30",
            "Aux501_Weapons_Mags_20mwdp30",

            "Aux501_Weapons_Mags_20mwdp20",
            "Aux501_Weapons_Mags_20mwdp20",

            "Aux501_Weapons_Mags_Thermal_Detonator"
        };
        respawnMagazines[] = 
        {
            "Aux501_Weapons_Mags_20mwdp30",
            "Aux501_Weapons_Mags_20mwdp30",
            "Aux501_Weapons_Mags_20mwdp30",
            "Aux501_Weapons_Mags_20mwdp30",
            "Aux501_Weapons_Mags_20mwdp30",
            "Aux501_Weapons_Mags_20mwdp30",
            "Aux501_Weapons_Mags_20mwdp30",
            "Aux501_Weapons_Mags_20mwdp30",
            "Aux501_Weapons_Mags_20mwdp30",
            "Aux501_Weapons_Mags_20mwdp30",
            "Aux501_Weapons_Mags_20mwdp30",

            "Aux501_Weapons_Mags_20mwdp20",
            "Aux501_Weapons_Mags_20mwdp20",

            "Aux501_Weapons_Mags_Thermal_Detonator"
        };
        linkedItems[] = 
        {
            "Aux501_Units_Republic_501_Infantry_Helmet_Trooper",
            
            "Aux501_Units_Republic_501_Infantry_Vests_CP",
            
            "Aux501_Republic_501_Infantry_Equipment_NVGs_Macrobino_501",

            "JLTS_clone_comlink",
            "ACE_Altimeter",
            "ItemMap",
            "ItemCompass",
            "ItemMicroDAGR"
        };
        respawnLinkedItems[] =
        {
            "Aux501_Units_Republic_501_Infantry_Helmet_Trooper",
            
            "Aux501_Units_Republic_501_Infantry_Vests_CP",
            
            "Aux501_Republic_501_Infantry_Equipment_NVGs_Macrobino_501",

            "JLTS_clone_comlink",
            "ACE_Altimeter",
            "ItemMap",
            "ItemCompass",
            "ItemMicroDAGR"
        };
        uniformClass = "Aux501_Units_Republic_501st_CP_Uniform";
        backpack = "Aux501_Units_Republic_501_Infantry_Backpacks_LR_Small";
    };
    class Aux501_Units_Republic_501st_Snr_CP_Unit: Aux501_Units_Republic_501st_CP_Unit
    {
        displayName = "Senior Corporal";
        cost = 125000;
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\inf_snr_cp_nco_armor_upper_co.paa",
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\inf_snr_cp_nco_armor_lower_co.paa"
        };
        linkedItems[] = 
        {
            "Aux501_Units_Republic_501_Infantry_Helmet_Trooper",
            
            "Aux501_Units_Republic_501_Infantry_Vests_snr_CP",
            
            "Aux501_Republic_501_Infantry_Equipment_NVGs_Macrobino_501",

            "JLTS_clone_comlink",
            "ACE_Altimeter",
            "ItemMap",
            "ItemCompass",
            "ItemMicroDAGR"
        };
        respawnLinkedItems[] =
        {
            "Aux501_Units_Republic_501_Infantry_Helmet_Trooper",
            
            "Aux501_Units_Republic_501_Infantry_Vests_snr_CP",
            
            "Aux501_Republic_501_Infantry_Equipment_NVGs_Macrobino_501",

            "JLTS_clone_comlink",
            "ACE_Altimeter",
            "ItemMap",
            "ItemCompass",
            "ItemMicroDAGR"
        };
        uniformClass = "Aux501_Units_Republic_501st_Snr_CP_Uniform";
    };
    class Aux501_Units_Republic_501st_CS_Unit: Aux501_Units_Republic_501st_CP_Unit
    {
        displayName = "Sergeant";
        cost = 125000;
        genericNames = "Aux501_CloneSergeant_Names";
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\inf_cs_armor_upper_co.paa",
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\inf_cp_armor_lower_co.paa"
        };
        weapons[] = {"Aux501_Weaps_DC15S","Aux501_Weaps_DC17","Throw","Put"};
        respawnWeapons[] = {"Aux501_Weaps_DC15S","Aux501_Weaps_DC17","Throw","Put"};
        magazines[] = 
        {
            "Aux501_Weapons_Mags_10mw50",
            "Aux501_Weapons_Mags_10mw50",
            "Aux501_Weapons_Mags_10mw50",
            "Aux501_Weapons_Mags_10mw50",
            "Aux501_Weapons_Mags_10mw50",
            "Aux501_Weapons_Mags_10mw50",
            "Aux501_Weapons_Mags_10mw50",
            "Aux501_Weapons_Mags_10mw50",
            "Aux501_Weapons_Mags_10mw50",
            "Aux501_Weapons_Mags_10mw50",
            "Aux501_Weapons_Mags_10mw50",

            "Aux501_Weapons_Mags_20mwdp20",
            "Aux501_Weapons_Mags_20mwdp20",

            "Aux501_Weapons_Mags_Thermal_Detonator"
        };
        respawnMagazines[] = 
        {
            "Aux501_Weapons_Mags_10mw50",
            "Aux501_Weapons_Mags_10mw50",
            "Aux501_Weapons_Mags_10mw50",
            "Aux501_Weapons_Mags_10mw50",
            "Aux501_Weapons_Mags_10mw50",
            "Aux501_Weapons_Mags_10mw50",
            "Aux501_Weapons_Mags_10mw50",
            "Aux501_Weapons_Mags_10mw50",
            "Aux501_Weapons_Mags_10mw50",
            "Aux501_Weapons_Mags_10mw50",
            "Aux501_Weapons_Mags_10mw50",

            "Aux501_Weapons_Mags_20mwdp20",
            "Aux501_Weapons_Mags_20mwdp20",

            "Aux501_Weapons_Mags_Thermal_Detonator"
        };
        linkedItems[] = 
        {
            "Aux501_Units_Republic_501_Infantry_Helmet_Sergeant",
            
            "Aux501_Units_Republic_501_Infantry_Vests_CS",
            
            "Aux501_Republic_501_Infantry_Equipment_NVGs_Macrobino_501",

            "JLTS_clone_comlink",
            "ACE_Altimeter",
            "ItemMap",
            "ItemCompass",
            "ItemMicroDAGR"
        };
        respawnLinkedItems[] =
        {
            "Aux501_Units_Republic_501_Infantry_Helmet_Sergeant",
            
            "Aux501_Units_Republic_501_Infantry_Vests_CS",
            
            "Aux501_Republic_501_Infantry_Equipment_NVGs_Macrobino_501",

            "JLTS_clone_comlink",
            "ACE_Altimeter",
            "ItemMap",
            "ItemCompass",
            "ItemMicroDAGR"
        };
        uniformClass = "Aux501_Units_Republic_501st_CS_Uniform";
    };
    class Aux501_Units_Republic_501st_Snr_CS_Unit: Aux501_Units_Republic_501st_CS_Unit
    {
        displayName = "Senior Sergeant";
        cost = 150000;
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\inf_snr_cs_nco_armor_upper_co.paa",
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\inf_snr_cs_nco_armor_lower_co.paa"
        };
        weapons[] = {"Aux501_Weaps_DC15A","Aux501_Weaps_DC17","Throw","Put"};
        respawnWeapons[] = {"Aux501_Weaps_DC15A","Aux501_Weaps_DC17","Throw","Put"};
        magazines[] = 
        {
            "Aux501_Weapons_Mags_20mwdp30",
            "Aux501_Weapons_Mags_20mwdp30",
            "Aux501_Weapons_Mags_20mwdp30",
            "Aux501_Weapons_Mags_20mwdp30",
            "Aux501_Weapons_Mags_20mwdp30",
            "Aux501_Weapons_Mags_20mwdp30",
            "Aux501_Weapons_Mags_20mwdp30",
            "Aux501_Weapons_Mags_20mwdp30",
            "Aux501_Weapons_Mags_20mwdp30",
            "Aux501_Weapons_Mags_20mwdp30",
            "Aux501_Weapons_Mags_20mwdp30",

            "Aux501_Weapons_Mags_20mwdp20",
            "Aux501_Weapons_Mags_20mwdp20",

            "Aux501_Weapons_Mags_Thermal_Detonator"
        };
        respawnMagazines[] = 
        {
            "Aux501_Weapons_Mags_20mwdp30",
            "Aux501_Weapons_Mags_20mwdp30",
            "Aux501_Weapons_Mags_20mwdp30",
            "Aux501_Weapons_Mags_20mwdp30",
            "Aux501_Weapons_Mags_20mwdp30",
            "Aux501_Weapons_Mags_20mwdp30",
            "Aux501_Weapons_Mags_20mwdp30",
            "Aux501_Weapons_Mags_20mwdp30",
            "Aux501_Weapons_Mags_20mwdp30",
            "Aux501_Weapons_Mags_20mwdp30",
            "Aux501_Weapons_Mags_20mwdp30",

            "Aux501_Weapons_Mags_20mwdp20",
            "Aux501_Weapons_Mags_20mwdp20",

            "Aux501_Weapons_Mags_Thermal_Detonator"
        };
        linkedItems[] = 
        {
            "Aux501_Units_Republic_501_Infantry_Helmet_Sergeant",
            
            "Aux501_Units_Republic_501_Infantry_Vests_snr_CS",
            
            "Aux501_Republic_501_Infantry_Equipment_NVGs_Macrobino_501",

            "JLTS_clone_comlink",
            "ACE_Altimeter",
            "ItemMap",
            "ItemCompass",
            "ItemMicroDAGR"
        };
        respawnLinkedItems[] =
        {
            "Aux501_Units_Republic_501_Infantry_Helmet_Sergeant",
            
            "Aux501_Units_Republic_501_Infantry_Vests_snr_CS",
            
            "Aux501_Republic_501_Infantry_Equipment_NVGs_Macrobino_501",

            "JLTS_clone_comlink",
            "ACE_Altimeter",
            "ItemMap",
            "ItemCompass",
            "ItemMicroDAGR"
        };
        uniformClass = "Aux501_Units_Republic_501st_Snr_CS_Uniform";
    };
    class Aux501_Units_Republic_501st_Platoon_CSM_Unit: Aux501_Units_Republic_501st_Snr_CS_Unit
    {
        displayName = "Sergeant Major";
        cost = 175000;
        genericNames = "Aux501_CloneSergeantMajor_Names";
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\inf_plt_csm_armor_upper_co.paa",
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\inf_plt_csm_armor_lower_co.paa"
        };
        linkedItems[] = 
        {
            "Aux501_Units_Republic_501_Infantry_Helmet_Sergeant",
            
            "Aux501_Units_Republic_501_Infantry_Vests_CSM_Platoon",
            
            "Aux501_Republic_501_Infantry_Equipment_NVGs_Rangefinder",

            "JLTS_clone_comlink",
            "ACE_Altimeter",
            "ItemMap",
            "ItemCompass",
            "ItemMicroDAGR"
        };
        respawnLinkedItems[] =
        {
            "Aux501_Units_Republic_501_Infantry_Helmet_Sergeant",
            
            "Aux501_Units_Republic_501_Infantry_Vests_CSM_Platoon",
            
            "Aux501_Republic_501_Infantry_Equipment_NVGs_Rangefinder",

            "JLTS_clone_comlink",
            "ACE_Altimeter",
            "ItemMap",
            "ItemCompass",
            "ItemMicroDAGR"
        };
        uniformClass = "Aux501_Units_Republic_501st_Platoon_CSM_Uniform";
    };

    //Officers
    class Aux501_Units_Republic_501st_2LT_Unit: Aux501_Units_Republic_501st_Snr_CS_Unit
    {
        displayName = "2nd Lieutenant";
        icon = "iconManOfficer";
        cost = 450000;
        genericNames = "Aux501_CloneOfficer_Names";
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\inf_2LT_armor_upper_co.paa",
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\inf_2LT_armor_lower_co.paa"
        };
        linkedItems[] = 
        {
            "Aux501_Units_Republic_501_Infantry_Helmet_Sergeant",
            
            "Aux501_Units_Republic_501_Infantry_Vests_2LT",
            
            "Aux501_Republic_501_Infantry_Equipment_NVGs_Off_visor",

            "JLTS_clone_comlink",
            "ACE_Altimeter",
            "ItemMap",
            "ItemCompass",
            "ItemMicroDAGR"
        };
        respawnLinkedItems[] =
        {
            "Aux501_Units_Republic_501_Infantry_Helmet_Sergeant",
            
            "Aux501_Units_Republic_501_Infantry_Vests_2LT",
            
            "Aux501_Republic_501_Infantry_Equipment_NVGs_Off_visor",

            "JLTS_clone_comlink",
            "ACE_Altimeter",
            "ItemMap",
            "ItemCompass",
            "ItemMicroDAGR"
        };
        uniformClass = "Aux501_Units_Republic_501st_2LT_Uniform";
    };

    class Aux501_Units_Republic_501st_1LT_Unit: Aux501_Units_Republic_501st_2LT_Unit
    {
        displayName = "1st Lieutenant";
        cost = 475000;
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\inf_1LT_armor_upper_co.paa",
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\inf_1LT_armor_lower_co.paa"
        };
        weapons[] = {"Aux501_Weaps_DC15S","Aux501_Weaps_DC17","Throw","Put"};
        respawnWeapons[] = {"Aux501_Weaps_DC15S","Aux501_Weaps_DC17","Throw","Put"};
        magazines[] = 
        {
            "Aux501_Weapons_Mags_10mw50",
            "Aux501_Weapons_Mags_10mw50",
            "Aux501_Weapons_Mags_10mw50",
            "Aux501_Weapons_Mags_10mw50",
            "Aux501_Weapons_Mags_10mw50",
            "Aux501_Weapons_Mags_10mw50",
            "Aux501_Weapons_Mags_10mw50",
            "Aux501_Weapons_Mags_10mw50",
            "Aux501_Weapons_Mags_10mw50",
            "Aux501_Weapons_Mags_10mw50",
            "Aux501_Weapons_Mags_10mw50",

            "Aux501_Weapons_Mags_20mwdp20",
            "Aux501_Weapons_Mags_20mwdp20",

            "Aux501_Weapons_Mags_Thermal_Detonator"
        };
        respawnMagazines[] = 
        {
            "Aux501_Weapons_Mags_10mw50",
            "Aux501_Weapons_Mags_10mw50",
            "Aux501_Weapons_Mags_10mw50",
            "Aux501_Weapons_Mags_10mw50",
            "Aux501_Weapons_Mags_10mw50",
            "Aux501_Weapons_Mags_10mw50",
            "Aux501_Weapons_Mags_10mw50",
            "Aux501_Weapons_Mags_10mw50",
            "Aux501_Weapons_Mags_10mw50",
            "Aux501_Weapons_Mags_10mw50",
            "Aux501_Weapons_Mags_10mw50",

            "Aux501_Weapons_Mags_20mwdp20",
            "Aux501_Weapons_Mags_20mwdp20",

            "Aux501_Weapons_Mags_Thermal_Detonator"
        };
        linkedItems[] = 
        {
            "Aux501_Units_Republic_501_Infantry_Helmet_Sergeant",
            
            "Aux501_Units_Republic_501_Infantry_Vests_1LT_3",
            
            "Aux501_Republic_501_Infantry_Equipment_NVGs_Integrated_NV_TI",

            "JLTS_clone_comlink",
            "ACE_Altimeter",
            "ItemMap",
            "ItemCompass",
            "ItemMicroDAGR"
        };
        respawnLinkedItems[] =
        {
            "Aux501_Units_Republic_501_Infantry_Helmet_Sergeant",
            
            "Aux501_Units_Republic_501_Infantry_Vests_1LT_3",
            
            "Aux501_Republic_501_Infantry_Equipment_NVGs_Integrated_NV_TI",

            "JLTS_clone_comlink",
            "ACE_Altimeter",
            "ItemMap",
            "ItemCompass",
            "ItemMicroDAGR"
        };
        uniformClass = "Aux501_Units_Republic_501st_1LT_Uniform";
    };

    class Aux501_Units_Republic_501st_Captain_Unit: Aux501_Units_Republic_501st_2LT_Unit
    {
        displayName = "Captain";
        cost = 500000;
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\inf_cpt_armor_upper_co.paa",
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\inf_cpt_armor_lower_co.paa"
        };
        weapons[] = {"Aux501_Weaps_DC15C","Aux501_Weaps_DC17","Throw","Put"};
        respawnWeapons[] = {"Aux501_Weaps_DC15C","Aux501_Weaps_DC17","Throw","Put"};
        magazines[] = 
        {
            "Aux501_Weapons_Mags_20mw40",
            "Aux501_Weapons_Mags_20mw40",
            "Aux501_Weapons_Mags_20mw40",
            "Aux501_Weapons_Mags_20mw40",
            "Aux501_Weapons_Mags_20mw40",
            "Aux501_Weapons_Mags_20mw40",
            "Aux501_Weapons_Mags_20mw40",
            "Aux501_Weapons_Mags_20mw40",
            "Aux501_Weapons_Mags_20mw40",
            "Aux501_Weapons_Mags_20mw40",
            "Aux501_Weapons_Mags_20mw40",
            
            "Aux501_Weapons_Mags_20mwdp20",
            "Aux501_Weapons_Mags_20mwdp20",

            "Aux501_Weapons_Mags_Thermal_Detonator"
        };
        respawnMagazines[] = 
        {
            "Aux501_Weapons_Mags_20mw40",
            "Aux501_Weapons_Mags_20mw40",
            "Aux501_Weapons_Mags_20mw40",
            "Aux501_Weapons_Mags_20mw40",
            "Aux501_Weapons_Mags_20mw40",
            "Aux501_Weapons_Mags_20mw40",
            "Aux501_Weapons_Mags_20mw40",
            "Aux501_Weapons_Mags_20mw40",
            "Aux501_Weapons_Mags_20mw40",
            "Aux501_Weapons_Mags_20mw40",
            "Aux501_Weapons_Mags_20mw40",

            "Aux501_Weapons_Mags_20mwdp20",
            "Aux501_Weapons_Mags_20mwdp20",
            
            "Aux501_Weapons_Mags_Thermal_Detonator"
        };
        linkedItems[] = 
        {
            "Aux501_Units_Republic_501_Infantry_Helmet_Sergeant",
            
            "Aux501_Units_Republic_501_Infantry_Vests_Captain_2",
            
            "Aux501_Republic_501_Infantry_Equipment_NVGs_Off_visor_01",

            "JLTS_clone_comlink",
            "ACE_Altimeter",
            "ItemMap",
            "ItemCompass",
            "ItemMicroDAGR"
        };
        respawnLinkedItems[] =
        {
            "Aux501_Units_Republic_501_Infantry_Helmet_Sergeant",
            
            "Aux501_Units_Republic_501_Infantry_Vests_Captain_2",
            
            "Aux501_Republic_501_Infantry_Equipment_NVGs_Off_visor_01",

            "JLTS_clone_comlink",
            "ACE_Altimeter",
            "ItemMap",
            "ItemCompass",
            "ItemMicroDAGR"
        };
        uniformClass = "Aux501_Units_Republic_501st_Captain_Uniform";
    };

    //RTOs
    

    //Medics

};