class cfgPatches
{
    class Aux501_Patch_Units_Republic_501_Infantry_Uniforms
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units",
            "A3_data_F",
            "A3_anims_F",
            "A3_weapons_F",
            "A3_characters_F"
        };
        units[] = {};
        weapons[] = {};
    };
};

class CfgWeapons
{
    class Uniform_Base;
    class UniformItem;
    class Aux501_Units_Republic_Undersuit_Uniform: Uniform_Base
    {
        scope = 1;
        scopeArsenal = 0;
        author = "501st Aux Team";
        vehicleClass = "ItemsUniforms";
        displayName = "Republic Clone Trooper Undersuit";
        picture = "\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\UI\CloneUndersuit_ca.paa";
        model = "\A3\Characters_F\Common\Suitpacks\suitpack_blufor_diver";
        class ItemInfo: UniformItem
        {
            uniformModel = "-";
            uniformClass = "Aux501_Units_Republic_Undersuit_Unit";
            containerClass = "Supply0";
            mass = 5;
        };
    };
};

class CfgVehicles
{
    class Underwear_F;
    class Aux501_Units_Republic_Undersuit_Unit: Underwear_F
    {
        author = "501st Aux Team";
        uniformClass = "Aux501_Units_Republic_Undersuit_Uniform";
        model = "ls_armor_bluefor\uniform\gar\phase2\lsd_gar_naked_uniform.p3d";
        hiddenSelections[] = {"bodyGlove","camo_legs"};
        hiddenSelectionsTextures[] = {"ls_armor_bluefor\uniform\gar\phase2\data\bodyGlove_co.paa",""};
    };
};