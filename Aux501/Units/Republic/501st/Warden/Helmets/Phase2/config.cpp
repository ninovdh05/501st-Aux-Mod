class cfgPatches
{
    class Aux501_Patch_Units_Republic_501_Warden_Helmets_Phase2
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units",
            "A3_data_F",
            "A3_anims_F",
            "A3_weapons_F",
            "A3_characters_F"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_Units_Republic_501_Warden_Helmet"
        };
    };
};

class CfgWeapons
{
    class H_HelmetB;

    class H_HelmetO_ViperSP_hex_F;
	class HeadgearItem;

    class Aux501_Units_Republic_501_Infantry_Helmet_Base: H_HelmetB
    {
        class ItemInfo;
    };

    class Aux501_Units_Republic_501_Warden_Helmet: Aux501_Units_Republic_501_Infantry_Helmet_Base
    {
        scope = 2;
        scopeArsenal = 2;
        displayName = "[501st] WRDN HELM 01";
        picture = "\Aux501\Units\Republic\501st\Warden\Helmets\Phase2\data\textures\UI\Warden_Helmet_ui_ca.paa";
        model = "Aux501\Units\Republic\501st\Warden\Helmets\Phase2\data\Aux501_Warden_Helmet.p3d";
        hiddenSelections[] = {"camo","visor"};
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Warden\Helmets\Phase2\data\textures\warden_helmet_CO.paa",
            "\Aux501\Units\Republic\501st\Warden\Helmets\Phase2\data\textures\warden_visor_CO.paa"
        };
        hiddenSelectionsMaterials[]= {};
        class ItemInfo: ItemInfo
        {
            uniformmodel = "Aux501\Units\Republic\501st\Warden\Helmets\Phase2\data\Aux501_Warden_Helmet.p3d";
            hiddenSelections[] = {"camo","visor"};
        };
    };
};