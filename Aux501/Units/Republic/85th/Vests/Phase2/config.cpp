class cfgPatches
{
    class Aux501_Patch_Units_Republic_85_Vests_Phase2
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units",
            "A3_data_F",
            "A3_anims_F",
            "A3_weapons_F",
            "A3_characters_F",
            "Aux501_Patch_Units_Republic_501_Infantry_Vests_Phase2"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_Units_Republic_85th_Vest"
        };
    };
};

class cfgWeapons
{
    class Aux501_Units_Republic_501_Infantry_Vests_2LT_2;

    class Aux501_Units_Republic_85th_Vest: Aux501_Units_Republic_501_Infantry_Vests_2LT_2
    {
        scope = 2;
        scopeArsenal = 2;
        displayname = "[85th] Pauldron 01";
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Vests\Phase2\data\textures\inf_comp_csm_vest_co.paa"};
    };
};