class cfgPatches
{
    class Aux501_Patch_Units_Republic_327_Vests_Phase2
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units",
            "A3_data_F",
            "A3_anims_F",
            "A3_weapons_F",
            "A3_characters_F",
            "Aux501_Patch_Units_Republic_501_Infantry_Vests_Phase2"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_Units_Republic_327th_Vest_Yellow",
            "Aux501_Units_Republic_327th_Vest_Brown"
        };
    };
};

class cfgWeapons
{
    class Aux501_Units_Republic_501_Infantry_Vests_2LT_4;

    class Aux501_Units_Republic_327th_Vest_Yellow: Aux501_Units_Republic_501_Infantry_Vests_2LT_4
    {
        scope = 2;
        scopeArsenal = 2;
        displayname = "[327th] Pauldron 01 - Yellow";
        hiddenSelectionsTextures[] = {"\MRC\JLTS\characters\CloneLegions\data\Clone_327thTrooper_vest_co.paa"};
    };
    class Aux501_Units_Republic_327th_Vest_Brown: Aux501_Units_Republic_327th_Vest_Yellow
    {
        displayname = "[327th] Pauldron 02 - Brown";
        hiddenSelectionsTextures[] = {"\MRC\JLTS\characters\CloneLegions\data\Clone_327thOfficer_vest_co.paa"};
    };
};