class cfgPatches
{
    class Aux501_Patch_E5C
    {
        addonRootClass = "Aux501_Patch_Weapons";
        requiredAddons[]=
        {
            "Aux501_Patch_Weapons",
            "A3_Weapons_F"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_Weaps_E5C"
        };
        magazines[] = 
        {
            "Aux501_Weapons_Mags_E5C150"
        };
    };
};

class Mode_FullAuto;

class cfgWeapons
{
    class Aux501_rifle_base_stunless;

    class Aux501_Weaps_E5C: Aux501_rifle_base_stunless
    {
        scope = 2;
        displayName = "[501st] E5C";
        baseWeapon = "Aux501_Weaps_E5C";
        picture = "\MRC\JLTS\weapons\e5c\data\ui\e5c_stock_ui_ca.paa";
        model = "\MRC\JLTS\weapons\e5c\e5c_stock.p3d";
        handAnim[] = {"OFP2_ManSkeleton","\MRC\JLTS\weapons\e5c\anims\e5c_stock_handanim.rtm"};
        magazines[]=
        {
            "Aux501_Weapons_Mags_E5C150"
        };
        fireLightDiffuse[] = {1,0,0};
        fireLightAmbient[] = {1,0,0};
        reloadAction = "3AS_GestureReloadDC15S";
        modes[] = {"FullAuto","aiclose","aimedium","aifar"};
        class FullAuto: Mode_FullAuto
        {
            sounds[] = {"StandardSound"};
            class BaseSoundModeType
            {
                weaponSoundEffect = "";
                begin1[] = {"\Aux501\Weapons\CIS\E5C\data\sound\E5c_shot.wss",+3db,1,2200};
                closure1[] = {};
                closure2[] = {};
                soundClosure[] = {};
            };
            class StandardSound: BaseSoundModeType
            {
                weaponSoundEffect = "";
                begin1[] = {"\Aux501\Weapons\CIS\E5C\data\sound\E5c_shot.wss",+3db,1,2200};
                soundBegin[] = {"begin1",1};
                closure1[] = {};
                closure2[] = {};
                soundClosure[] = {};
            };
            reloadTime = 0.1;
            dispersion = 0.00102;
            soundBurst = 0;
            ffCount = 1;
            soundContinuous = 0;
            minRange = 0;
            minRangeProbab = 0.9;
            midRange = 15;
            midRangeProbab = 0.7;
            maxRange = 30;
            maxRangeProbab = 0.1;
            aiRateOfFire = 1e-06;
            aiRateOfFireDistance = 50;
            showToPlayer = 1;
        };
        class aiclose: FullAuto
        {
            burst = 10;
            aiRateOfFire = 1e-06;
            aiRateOfFireDistance = 50;
            minRange = 0;
            minRangeProbab = 0.05;
            midRange = 70;
            midRangeProbab = 0.7;
            maxRange = 150;
            maxRangeProbab = 0.04;
            showToPlayer = 0;
        };
        class aimedium: aiclose
        {
            burst = 10;
            aiRateOfFireDistance = 300;
            minRange = 75;
            minRangeProbab = 0.05;
            midRange = 250;
            midRangeProbab = 0.7;
            maxRange = 500;
            maxRangeProbab = 0.04;
        };
        class aifar: aiclose
        {
            burst = 10;
            aiRateOfFireDistance = 600;
            minRange = 300;
            minRangeProbab = 0.05;
            midRange = 700;
            midRangeProbab = 0.6;
            maxRange = 1000;
            maxRangeProbab = 0.1;
        };
    };    
};

class CfgMagazines
{
    class Aux501_Weapons_Mags_E5100;

    class Aux501_Weapons_Mags_E5C150: Aux501_Weapons_Mags_E5100
    {
        displayName = "[501st] E5C Blaster Cell";
        displayNameShort = "E5C Cell";
        picture = "\MRC\JLTS\weapons\e5c\data\ui\e5c_mag_ui_ca.paa";
        model = "\MRC\JLTS\weapons\e5c\e5c_mag.p3d";
        count = 150;
        ammo = "Aux501_Weapons_Ammo_E5_Blaster";
        descriptionShort = "E5C Medium Power Magazine";
    };
};