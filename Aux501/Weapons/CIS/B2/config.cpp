class cfgPatches
{
    class Aux501_Patch_B2_Weapons
    {
        addonRootClass = "Aux501_Patch_Weapons";
        requiredAddons[]=
        {
            "Aux501_Patch_Weapons",
            "A3_Weapons_F"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_Weaps_B2_blaster",
            "Aux501_Weaps_B2_rocket_cannon",
            "Aux501_Weaps_B2_GL"
        };
        magazines[] = 
        {
            "Aux501_Weapons_Mags_B2_blaster60",
            "Aux501_Weapons_Mags_B2_rocket3",
            "Aux501_Weapons_Mags_B2_GL3"
        };
        ammo[] = 
        {
            "Aux501_Weapons_Ammo_B2_Blaster",
            "Aux501_Weapons_Ammo_B2_rocket",
            "Aux501_Weapons_Ammo_B2_GL_HE"
        };
    };
};

class CowsSlot;
class MuzzleSlot;
class PointerSlot;
class UnderBarrelSlot;

class Mode_SemiAuto;
class Mode_Burst;
class Mode_FullAuto;

class cfgWeapons
{
    class Rifle_Long_Base_F;
    class Aux501_rifle_base;
    class Aux501_rifle_base_stunless: Aux501_rifle_base
    {
        class WeaponSlotsInfo;
    };

    class Aux501_Weaps_B2_blaster: Aux501_rifle_base_stunless
    {
        scope = 2;
        scopeArsenal = 2;
        displayName="[501st] B2 Blaster";
        descriptionShort = "B2 Arm Blaster";
        model="WebKnightsRobotics\WBK_b2_weap.p3d";
        picture = "\Aux501\Weapons\CIS\B2\data\UI\b2_weapon_ui_ca.paa";
        muzzles[] = {"this","Secondary"};
        magazines[] = {"Aux501_Weapons_Mags_B2_blaster60"};
        modelOptics = "\A3\weapons_f\reticle\Optics_Gunner_02_F";
        reloadAction = "ReloadMagazine";
        hiddenSelections[] = {""};
        changeFiremodeSound[] = {"",1,1};
        useModelOptics = 1;
        drySound[] = {"A3\Sounds_F_Mark\arsenal\weapons\Machineguns\MMG_02_SPGM\MMG_02_Dry",0.177828,1,10};
        reloadMagazineSound[] = {"\SWLW_droids\smgs\e5\sounds\e5_reload.wss",1.5,1,20};
        soundBullet[] = {};
        modes[] = {"Single","FullAuto","close","short","medium"};
        recoil = "recoil_mmg_01";
        simulation = "Weapon";
        handAnim[] = {};
        fireLightDuration = 0.05;
        fireLightIntensity = 0.2;
        fireLightDiffuse[] = {1,0,0};
        fireLightAmbient[] = {1,0,0};		
        class Secondary: Rifle_Long_Base_F
        {
            ACE_overheating_allowSwapBarrel = 1;
            ACE_Overheating_Dispersion = 0;
            ACE_Overheating_JamChance = 0;
            ACE_overheating_mrbs = 3e+09;
            ACE_overheating_slowdownFactor = 0;
            ace_overpressure_angle = 0;
            ace_overpressure_damage = 0;
            ace_overpressure_priority = 1;
            ace_overpressure_range = 0;
            author = "501st Aux Team";
            scope = 2;
            scopeArsenal = 2;
            model = "";
            muzzles[] = {"this"};
            magazines[] = {"Aux501_Weapons_Mags_B2_rocket3"};
            reloadAction = "ReloadMagazine";
            Modeloptics = "\A3\weapons_f\reticle\Optics_Gunner_02_F";
            displayname = "B2 Wrist Rockets";
            descriptionShort = "Standard B2 Rockers";
            hiddenSelections[] = {""};
            changeFiremodeSound[] = {"",1,1};
            useModelOptics = 1;
            drySound[] = {"A3\Sounds_F_Mark\arsenal\weapons\Machineguns\MMG_02_SPGM\MMG_02_Dry",0.177828,1,10};
            reloadMagazineSound[] = {"\SWLW_droids\smgs\e5\sounds\e5_reload.wss",1.5,1,20};
            soundBullet[] = {};
            modes[] = {"Single"};
            recoil = "recoil_mmg_01";
            simulation = "Weapon";
            handAnim[] = {};
            fireLightDuration = 0.05;
            fireLightIntensity = 0.2;
            fireLightDiffuse[] = {1,0,0};
            fireLightAmbient[] = {1,0,0};		
            class Single: Mode_SemiAuto
            {
                sounds[] = {"StandardSound"};
                class BaseSoundModeType{};
                class StandardSound: BaseSoundModeType
                {
                    begin1[] = {"swlw_rework\sounds\launcher\PLX_shot.wss",10,1,2200};
                    soundBegin[] = {"begin1",1};
                };
                recoil = "recoil_auto_primary_3outof10";
                recoilProne = "recoil_single_prone_mx";
                reloadTime = 0.5;
                burst = 1;
                dispersion = 0.00073;
                minRange = 5;
                minRangeProbab = 0.1;
                midRange = 150;
                midRangeProbab = 0.6;
                maxRange = 300;
                maxRangeProbab = 0.3;
                aiRateOfFire = 1;
                aiRateOfFireDistance = 500;
            };
            inertia = 1.1;
            dexterity = 1.7;
            initSpeed = -1;
            maxRecoilSway = 0.008;
            swayDecaySpeed = 2;
            UIPicture = "\A3\weapons_f\data\UI\icon_mg_CA.paa";
            class WeaponSlotsInfo: WeaponSlotsInfo
            {
                mass = 150;
                holsterScale = 0.9;
                class MuzzleSlot: MuzzleSlot
                {
                    compatibleItems[] = {};
                };
                class CowsSlot: CowsSlot
                {
                    compatibleItems[] = {};
                };
                class UnderBarrelSlot: UnderBarrelSlot
                {
                    compatibleItems[] = {};
                };
            };
        };
        class Single: Mode_SemiAuto
        {
            sounds[] = {"StandardSound"};
            class BaseSoundModeType
            {
                weaponSoundEffect = "";
                closure1[] = {};
                closure2[] = {};
                soundClosure[] = {};
            };
            class StandardSound: BaseSoundModeType
            {
                weaponSoundEffect = "";
                begin1[] = {"\SWLW_droids\smgs\e5\sounds\e5",1,1,1800};
                begin2[] = {"\SWLW_droids\smgs\e5\sounds\e5",1,1,1800};
                begin3[] = {"\SWLW_droids\smgs\e5\sounds\e5",1,1,1800};
                soundBegin[] = {"begin1",0.33,"begin2",0.33,"begin3",0.33};
            };
            recoil = "recoil_auto_primary_3outof10";
            recoilProne = "recoil_single_prone_mx";
            reloadTime = 0.2;
            burst = 1;
            dispersion = 0.00073;
            minRange = 5;
            minRangeProbab = 0.1;
            midRange = 25;
            midRangeProbab = 0.6;
            maxRange = 50;
            maxRangeProbab = 0.3;
            aiRateOfFire = 1;
            aiRateOfFireDistance = 500;
        };
        class close: Single
        {
            showToPlayer = 0;
            aiRateOfFire = 0.5;
            aiRateOfFireDistance = 50;
            dispersion = 0.00073;
            minRange = 0;
            minRangeProbab = 0.05;
            midRange = 200;
            midRangeProbab = 0.7;
            maxRange = 400;
            maxRangeProbab = 0.2;
        };
        class short: close
        {
            aiRateOfFire = 0.5;
            aiRateOfFireDistance = 200;
            minRange = 50;
            minRangeProbab = 0.2;
            midRange = 200;
            midRangeProbab = 0.7;
            maxRange = 300;
            maxRangeProbab = 0.2;
        };
        class medium: close
        {
            aiRateOfFire = 1;
            aiRateOfFireDistance = 600;
            minRange = 300;
            minRangeProbab = 0.2;
            midRange = 450;
            midRangeProbab = 0.7;
            maxRange = 600;
            maxRangeProbab = 0.2;
        };
        class FullAuto: Mode_FullAuto
        {
            aiRateOfFireDistance = 900;
            aiRateOfFireDispersion = 1;
            autoFire = 1;
            dispersion = "2*0.00087";
            burst = "";
            maxRange = 50;
            maxRangeProbab = 0.1;
            midRange = 25;
            midRangeProbab = 0.6;
            minRange = 5;
            reloadTime = 0.25;
            minRangeProbab = 0.3;
            recoil = "recoil_auto_pdw";
            recoilProne = "recoil_auto_prone_pdw";
            sounds[] = {"StandardSound"};
            class BaseSoundModeType
            {
                weaponSoundEffect = "";
                closure1[] = {};
                closure2[] = {};
                soundClosure[] = {};
            };
            class StandardSound: BaseSoundModeType
            {
                weaponSoundEffect = "";
                begin1[] = {"\SWLW_droids\smgs\e5\sounds\e5",1,1,1800};
                begin2[] = {"\SWLW_droids\smgs\e5\sounds\e5",1,1,1800};
                begin3[] = {"\SWLW_droids\smgs\e5\sounds\e5",1,1,1800};
                soundBegin[] = {"begin1",0.33,"begin2",0.33,"begin3",0.33};
            };
        };
        inertia = 1.1;
        dexterity = 1.7;
        initSpeed = -1;
        maxRecoilSway = 0.008;
        swayDecaySpeed = 2;
        UIPicture = "\A3\weapons_f\data\UI\icon_mg_CA.paa";
        class WeaponSlotsInfo: WeaponSlotsInfo
        {
            mass = 150;
            holsterScale = 0.9;
            class MuzzleSlot: MuzzleSlot
            {
                compatibleItems[] = {};
            };
            class CowsSlot: CowsSlot
            {
                compatibleItems[] = {};
            };
            class UnderBarrelSlot: UnderBarrelSlot
            {
                compatibleItems[] = {};
            };
        };
    };
    class Aux501_Weaps_B2_rocket_cannon: Aux501_Weaps_B2_blaster
    {
        displayName="[501st] B2 Rocket Cannon";
        magazines[]=
        {
            "Aux501_Weapons_Mags_B2_rocket3"
        };	
        muzzles[] = {"this"};
        modes[] = {"Single"};
        class Single: Single
        {
            sounds[] = {"StandardSound"};
            class BaseSoundModeType
            {
                weaponSoundEffect = "";
                closure1[] = {};
                closure2[] = {};
                soundClosure[] = {};
            };
            class StandardSound: BaseSoundModeType
            {
                weaponSoundEffect = "";
                begin1[] = {"\swlw_rework\sounds\trando\ls150_shot.wss",10,1,2000};
                begin2[] = {"\swlw_rework\sounds\trando\ls150_shot.wss",10,1,2000};
                begin3[] = {"\swlw_rework\sounds\trando\ls150_shot.wss",10,1,2000};
                soundBegin[] = {"begin1",0.33,"begin2",0.33,"begin3",0.34};
            };
            reloadTime= 2;
            burst = 1;
            minRange = 5;
            minRangeProbab = 0.1;
            midRange = 150;
            midRangeProbab = 0.6;
            maxRange = 1000;
            maxRangeProbab = 0.3;
            aiRateOfFire = 1;
            aiRateOfFireDistance = 1000;
        };
    };
    class Aux501_Weaps_B2_GL: Aux501_Weaps_B2_blaster
    {
        displayName="[501st] B2 Grenade Launcher";
        magazines[]=
        {
            "Aux501_Weapons_Mags_B2_GL3"
        };
        burst = 1;
        aiRateOfFire = 1;
        modes[] = {"Single"};
        muzzles[] = {"this"};
        class Single: Single
        {
            reloadTime = 2.5;
            minRange = 0;
            minRangeProbab = 0.1;
            midRange = 0;
            midRangeProbab = 0.6;
            maxRange = 500;
            maxRangeProbab = 0.3;
            sounds[] = {"StandardSound"};
            class BaseSoundModeType
            {
                weaponSoundEffect = "";
                closure1[] = {};
                closure2[] = {};
                soundClosure[] = {};
            };
            class StandardSound: BaseSoundModeType
            {
                weaponSoundEffect = "";
                begin1[] = {"\swlw_rework\sounds\e-series\E5S_shot.wss",10,1,2000};
                begin2[] = {"\swlw_rework\sounds\e-series\E5S_shot.wss",10,1,2000};
                begin3[] = {"\swlw_rework\sounds\e-series\E5S_shot.wss",10,1,2000};
                soundBegin[] = {"begin1",0.33,"begin2",0.33,"begin3",0.34};
            };
        };
    };
};

class CfgMagazines
{
    class CA_Magazine;
    class 1Rnd_HE_Grenade_shell;

    class Aux501_Weapons_Mags_B2_blaster60: CA_Magazine
    {
        count = 60;
        displayName = "[501st] 60Rnd B2 Magazine";
        descriptionShort = "60Rnd B2 Blaster Charge";
        picture = "\MRC\JLTS\weapons\DC15A\data\ui\DC15A_mag_ui_ca.paa";
        model = "\MRC\JLTS\weapons\DC15A\DC15A_mag.p3d";
        author = "501st Aux Team";
        initSpeed = 310;
        mass = 15;
        scope = 2;
        type = 16;
        ammo = "Aux501_Weapons_Ammo_B2_Blaster";
        tracersEvery = 1;
    };
    class Aux501_Weapons_Mags_B2_rocket3: CA_Magazine
    {
        count = 3;
        displayName = "[501st] 3Rnd B2 Wrist Rockets";
        descriptionShort = "3Rnd Wrist Rockets";
        picture = "\MRC\JLTS\weapons\DC15A\data\ui\DC15A_mag_ui_ca.paa";
        model = "\MRC\JLTS\weapons\DC15A\DC15A_mag.p3d";
        author = "501st Aux Team";
        initSpeed = 100;
        mass = 15;
        soundFly[] = {"A3\Sounds_F\weapons\Rockets\rocket_fly_2",0.501187,1.3,400};
        scope = 2;
        type = 16;
        ammo = "Aux501_Weapons_Ammo_B2_rocket";
        tracersEvery = 1;
    };
    class Aux501_Weapons_Mags_B2_GL3: 1Rnd_HE_Grenade_shell
    {
        displayName = "[501st] 3Rnd B2 Grenades";
        displayNameShort = "3Rnd HE Grenades";
        model = "\SWLW_clones\machineguns\Z6\Z6_g_mag.p3d";
        picture = "\SWLW_clones\machineguns\Z6\data\ui\Z6_g_mag_ui.paa";
        count=3;
        ammo = "Aux501_Weapons_Ammo_B2_GL_HE";
    };
};

class CfgAmmo
{
    class Aux501_Weapons_Ammo_base_red;
    class G_40mm_HE;

    class Aux501_Weapons_Ammo_B2_Blaster: Aux501_Weapons_Ammo_base_red
    {
        cost=1;
        caliber=3;
        hit=20;
        model = "SWLW_main\Effects\laser_red.p3d";
        tracerscale = 1.5;
        typicalSpeed = 60;
    };
    
    class Aux501_Weapons_Ammo_B2_rocket: Aux501_Weapons_Ammo_base_red
    {
        cost = 200;
        caliber = 3;
        dangerRadiusBulletClose = 16;
        dangerRadiusHit = 40;
        effectFly = "Aux501_particle_effect_b2_rocket_fly";
        explosionAngle = 60;
        explosive = 0.8;
        hit = 100;
        effectFlare = "FlareShell";
        effectsFire = "CannonFire";
        explosionDir = "explosionDir";
        explosionEffects = "ATMissileExplosion";
        explosionEffectsDir = "explosionDir";
        explosionPos = "explosionPos";
        explosionType = "explosive";
        indirectHit = 5;
        indirectHitRange = 3;
        model = "SWLW_main\Effects\laser_red.p3d";
        muzzleEffect = "BIS_fnc_effectFiredRocket";
        nameSound = "missiles";
        simulation = "shotBullet";
        soundFly[] = {"swlw_rework\sounds\launcher\PLX_fly.wss",6,1.5,700};
        soundSetBulletFly[] = {"bulletFlyBy_SoundSet"};
        soundSetSonicCrack[] = {"bulletSonicCrack_SoundSet","bulletSonicCrackTail_SoundSet"};
        supersonicCrackFar[] = {"A3\sounds_f\arsenal\sfx\supersonic_crack\scrack_middle",3.16228,1,500};
        supersonicCrackNear[] = {"A3\sounds_f\arsenal\sfx\supersonic_crack\scrack_close",3.16228,1,500};
        soundHit[] = {"",100,1};
        soundHit1[] = {"A3\Sounds_F\arsenal\weapons\Launchers\Titan\Explosion_titan_missile_01",2.51189,1,2000};
        soundHit2[] = {"A3\Sounds_F\arsenal\weapons\Launchers\Titan\Explosion_titan_missile_02",2.51189,1,2000};
        soundHit3[] = {"A3\Sounds_F\arsenal\weapons\Launchers\Titan\Explosion_titan_missile_03",2.51189,1,2000};
        SoundSetExplosion[] = {"RocketsHeavy_Exp_SoundSet","RocketsHeavy_Tail_SoundSet","Explosion_Debris_SoundSet"};
        thrust = 210;
        thrustTime = 1.5;
        timeToLive = 6;
        tracerColor[] = {0.7,0.7,0.5,0.04};
        tracerColorR[] = {0.7,0.7,0.5,0.04};
        tracerScale = 1;
        tracerStartTime = 0;
        tracerEndTime = 10;
        typicalSpeed = 30;
    };

    class Aux501_Weapons_Ammo_B2_GL_HE: G_40mm_HE
    {
        hit = 10;
        indirectHit = 8;
        indirectHitRange = 6;
        dangerRadiusHit = 30;
        suppressionRadiusHit = 30;
        explosionEffectsRadius = 20;
        typicalspeed = 30;
        model = "kobra\442_weapons\explosive\thermal_det.p3d";
        visibleFire = 0.5;
        audibleFire = 0.05;
        visibleFireTime = 1;
        fuseDistance = 0;
        class NVGMarkers
        {
            class Blinking1
            {
                name = "blinkpos1";
                color[] = {0.01,0.01,0.01,1};
                ambient[] = {0.005,0.005,0.005,1};
                blinking = 1;
                brightness = 0.002;
                onlyInNvg = 1;
            };
            class Blinking2
            {
                color[] = {0.9,0.1,0.1};
                ambient[] = {0.1,0.1,0.1};
                name = "blinkpos2";
                blinking = 1;
                blinkingStartsOn = 1;
                blinkingPattern[] = {0.1,0.9};
                blinkingPatternGuarantee = 1;
                drawLightSize = 0.35;
                drawLightCenterSize = 0.05;
                brightness = 0.002;
                dayLight = 1;
                onlyInNvg = 0;
                intensity = 75;
                drawLight = 1;
                activeLight = 0;
                useFlare = 0;
            };
        };
        ace_frag_enabled = 0;
        ace_frag_skip = 0;
        ace_frag_force = 0;
        explosionEffects = "VehicleExplosionEffects";
        soundFly[] = {"kobra\442_weapons\sounds\thermal_det\thermal_det.wss",1.5,1,90};
        timeToLive =30;
        explosionTime= 5;
    };
};

class Aux501_particle_effect_b2_rocket_fly
{
    class Light
    {
        simulation="light";
        type="Aux501_light_B2_rocket_light";
        position[]={0,0,0};
        qualityLevel = -1;
    };
    class Smoke
    {
        simulation="particles";
        type="Aux501_cloudlet_B2_rocket_smoke";
        position[]={0,0,0};
        qualityLevel = -1;
    };
    class Sparks
    {
        simulation="particles";
        type="Aux501_cloudlet_B2_rocket_sparks";
        position[]={0,0,0};
        qualityLevel=2;
    };
};

class CfgCloudlets
{
    class Default;
    class Missile3;

    class Aux501_cloudlet_B2_rocket_sparks: Default
    {
        interval = 0.0009;
        lifeTime = 2.5;
        circleRadius = 0;
        circleVelocity[] = {0,0,0};
        particleShape = "\A3\data_f\ParticleEffects\Universal\Universal";
        particleFSNtieth = 16;
        particleFSIndex = 13;
        particleFSFrameCount = 2;
        particleFSLoop = 0;
        angleVar = 360;
        animationName = "";
        particleType = "Billboard";
        timerPeriod = 3;
        moveVelocity[] = {0,0,0};
        rotationVelocity = 1;
        weight = 100;
        volume = 0.01;
        rubbing = 0.3;
        size[] = {0.12,0};
        sizeCoef = 1;
        color[] = {{1,0.6,0.4,-50}};
        colorCoef[] = {1,1,1,1};
        emissiveColor[] = {{10,6,4,1}};
        animationSpeed[] = {1000};
        animationSpeedCoef = 1;
        randomDirectionPeriod = 0;
        randomDirectionIntensity = 0;
        onTimerScript = "";
        beforeDestroyScript = "";
        blockAIVisibility = 0;
        bounceOnSurface = 0.1;
        bounceOnSurfaceVar = 0.1;
        lifeTimeVar = 5;
        position[] = {0,0,0};
        positionVar[] = {0.01,0.01,0.01};
        moveVelocityVar[] = {1,3,1};
        rotationVelocityVar = 0;
        sizeVar = 0;
        colorVar[] = {0.05,0.05,0.05,5};
        randomDirectionPeriodVar = 0;
        randomDirectionIntensityVar = 0;
    };
    class Aux501_cloudlet_B2_rocket_smoke: Missile3
    {
        color[]={{0,0,0,0.5},{120,120,120,1},{120,120,120,1}};
        size[]={0.3,0.5,0.5};
        lifeTime = 1;
        interval = 0.0005;
    };
};

class CfgLights
{
    class RocketLight;
    class Aux501_light_B2_rocket_light : RocketLight
    {
        color[]={212,59,64};
        intensity = 1000;
        dayLight = 1;
        useFlare = 1;
        flareSize = 1.5;
        flareMaxDistance = 6000;
    };
};