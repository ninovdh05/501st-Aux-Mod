class cfgPatches
{
    class Aux501_Patch_e60r
    {
        addonRootClass = "Aux501_Patch_Weapons";
        requiredAddons[]=
        {
            "Aux501_Patch_Weapons",
            "A3_Weapons_F"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_Weaps_e60r_at",
            "Aux501_Weaps_e60r_aa"
        };
        magazines[] = 
        {
            "Aux501_Weapons_Mags_e60r_at",
            "Aux501_Weapons_Mags_e60r_he",
            "Aux501_Weapons_Mags_e60r_aa"
        };
        ammo[] = 
        {
            "Aux501_Weapons_Ammo_e60r_at",
            "Aux501_Weapons_Ammo_e60r_at_sub",

            "Aux501_Weapons_Ammo_e60r_he",
            
            "Aux501_Weapons_Ammo_e60r_aa"
        };
    };
};

class CfgWeapons
{
    class Launcher_Base_F;
    class launch_RPG32_F: Launcher_Base_F
    {
        class Single;
    };

    class launch_Titan_base;
    class launch_Titan_short_base: launch_Titan_base
    {
        class Single;
        class TopDown;
    };

    class Aux501_Weaps_e60r_at: launch_RPG32_F
    {
        scope = 2;
        author = "501st Aux Team";
        displayName = "[501st] E60-R Unguided Launcher";
        descriptionShort = "Seperatist Anti-Tank Weapon";
        picture = "\MRC\JLTS\weapons\E60R\data\ui\E60R_ui_ca.paa";
        uiPicture = "\MRC\JLTS\weapons\E60R\data\ui\E60R_ui_ca.paa";
        model = "\MRC\JLTS\weapons\E60R\E60R.p3d";
        modelSpecial = "";
        handAnim[] = {"OFP2_ManSkeleton","\MRC\JLTS\weapons\E60R\anims\E60R_handanim.rtm"};
        hiddenSelections[] = {"camo1","illum"};
        hiddenSelectionsTextures[] = {"\MRC\JLTS\weapons\E60R\data\E60R_co.paa"};
        hiddenSelectionsMaterials[] = {"","\a3\characters_f_bootcamp\common\data\vrarmoremmisive.rvmat"};
        magazines[] = {"Aux501_Weapons_Mags_e60r_at","Aux501_Weapons_Mags_e60r_he"};
        magazineWell[] = {};
        class Single: Single
        {
            class BaseSoundModeType{};
            class StandardSound: BaseSoundModeType
            {
                begin1[] = {"swlw_rework\sounds\launcher\E60R_shot.wss",10,1,2200};
                soundBegin[] = {"begin1",1};
            };
        };
        magazineReloadTime = 60;
    };
    class Aux501_Weaps_e60r_aa: launch_Titan_short_base
    {
        scope = 2;
        author = "501st Aux Team";
        displayName = "[501st] E60-AA Guided Launcher";
        descriptionShort = "Seperatist Anti-Air Weapon";
        picture = "\MRC\JLTS\weapons\E60R\data\ui\E60R_ui_ca.paa";
        uiPicture = "MRC\JLTS\weapons\E60R\data\ui\E60R_ui_ca.paa";
        model = "\MRC\JLTS\weapons\E60R\E60R.p3d";
        modelSpecial = "";
        handAnim[] = {"OFP2_ManSkeleton","\MRC\JLTS\weapons\E60R\anims\E60R_handanim.rtm"};
        hiddenSelections[] = {"camo1","illum"};
        hiddenSelectionsTextures[] = {"\MRC\JLTS\weapons\E60R\data\E60R_co.paa"};
        hiddenSelectionsMaterials[] = {"","\a3\characters_f_bootcamp\common\data\vrarmoremmisive.rvmat"};
        magazines[] = {"Aux501_Weapons_Mags_e60r_aa"};
        magazineWell[] = {};
        magazineReloadTime = 60;
        class Single: Single
        {
            class BaseSoundModeType{};
            class StandardSound: BaseSoundModeType
            {
                begin1[] = {"swlw_rework\sounds\launcher\E60R_shot.wss",10,1,2000};
                soundBegin[] = {"begin1",1};
            };
        };
        class TopDown: TopDown
        {
            class BaseSoundModeType{};
            class StandardSound: BaseSoundModeType
            {
                begin1[] = {"swlw_rework\sounds\launcher\E60R_shot.wss",10,1,2000};
                soundBegin[] = {"begin1",1};
            };
        };
    };
};

class CfgMagazines
{
    class RPG32_F;
    class RPG32_HE_F;
    class Titan_AA;

    class Aux501_Weapons_Mags_e60r_at: RPG32_F
    {
        author = "501st Aux Team";
        displayName = "[501st] E-60R AT Rocket";
        displayNameShort = "AT";
        descriptionShort = "CIS AT Rocket";
        ammo = "Aux501_Weapons_Ammo_e60r_at";
    };

    class Aux501_Weapons_Mags_e60r_he: RPG32_HE_F
    {
        author = "501st Aux Team";
        displayName = "[501st] E-60R HE Rocket";
        descriptionShort = "CIS HE Rocket";
        ammo = "Aux501_Weapons_Ammo_e60r_he";
    };

    class Aux501_Weapons_Mags_e60r_aa: Titan_AA
    {
        author = "501st Aux Team";
        displayName = "[501st] E-60R AA Rocket";
        descriptionShort = "CIS AA Rocket";
        ammo = "Aux501_Weapons_Ammo_e60r_aa";
        mass = 60;
    };
};

class CfgAmmo
{
    class ammo_Penetrator_RPG32V;
    class R_PG32V_F;
    class R_TBG32V_F;

    class M_Titan_AA;

    class Aux501_Weapons_Ammo_e60r_at: R_PG32V_F
    {
        soundFly[] = {"swlw_rework\sounds\launcher\E60R_fly.wss",6,1.5,700};
        effectsMissile = "Aux501_particle_effect_E60R_fly";
        thrust = 100;
        thrustTime = 5;
        timeToLive = 5;
        submunitionAmmo = "Aux501_Weapons_Ammo_e60r_at_sub";
    };	
    class Aux501_Weapons_Ammo_e60r_at_sub: ammo_Penetrator_RPG32V
    {
        hit = 650;
        caliber = 43.3333;
        airFriction = -0.28;
        thrust = 210;
        thrustTime = 1.5;
        typicalSpeed = 1000;
    };

    class Aux501_Weapons_Ammo_e60r_he: R_TBG32V_F
    {
        soundFly[] = {"swlw_rework\sounds\launcher\E60R_fly.wss",6,1.5,700};
        effectsMissile = "Aux501_particle_effect_E60R_fly";
        thrust = 100;
        thrustTime = 5;
        timeToLive = 5;
    };

    class Aux501_Weapons_Ammo_e60r_aa: M_Titan_AA
    {
        cmImmunity = 0.5;
        soundFly[] = {"swlw_rework\sounds\launcher\E60R_fly.wss",6,1.5,700};
        effectsMissile = "Aux501_particle_effect_E60R_fly";
    };
};

class Aux501_particle_effect_E60R_fly
{
    class Light
    {
        simulation = "light";
        type = "Aux501_light_E60R";
        position[] = {0,0,0};
    };
    class Smoke
    {
        simulation = "particles";
        type = "Aux501_cloudlet_E60R_smoke";
        position[] = {0,0,0};
        qualityLevel = -1;
    };
    class Sparks
    {
        simulation = "particles";
        type = "Aux501_cloudlet_E60R_sparks";
        position[] = {0,0,0};
        qualityLevel = 2;
    };
};

class CfgCloudlets
{
    class Default;
    class Missile3;

    class Aux501_cloudlet_E60R_sparks: Default
    {
        interval = 0.0009;
        lifeTime = 2.5;
        circleRadius = 0;
        circleVelocity[] = {0,0,0};
        particleShape = "\A3\data_f\ParticleEffects\Universal\Universal";
        particleFSNtieth = 16;
        particleFSIndex = 13;
        particleFSFrameCount = 2;
        particleFSLoop = 0;
        angleVar = 360;
        animationName = "";
        particleType = "Billboard";
        timerPeriod = 3;
        moveVelocity[] = {0,0,0};
        rotationVelocity = 1;
        weight = 100;
        volume = 0.01;
        rubbing = 0.3;
        size[] = {0.12,0};
        sizeCoef = 1;
        color[] = {{1,0.6,0.4,-50}};
        colorCoef[] = {1,1,1,1};
        emissiveColor[] = {{10,6,4,1}};
        animationSpeed[] = {1000};
        animationSpeedCoef = 1;
        randomDirectionPeriod = 0;
        randomDirectionIntensity = 0;
        onTimerScript = "";
        beforeDestroyScript = "";
        blockAIVisibility = 0;
        bounceOnSurface = 0.1;
        bounceOnSurfaceVar = 0.1;
        lifeTimeVar = 5;
        position[] = {0,0,0};
        positionVar[] = {0.01,0.01,0.01};
        moveVelocityVar[] = {1,3,1};
        rotationVelocityVar = 0;
        sizeVar = 0;
        colorVar[] = {0.05,0.05,0.05,5};
        randomDirectionPeriodVar = 0;
        randomDirectionIntensityVar = 0;
    };
    class Aux501_cloudlet_E60R_smoke: Missile3
    {
        color[]={{0,0,0,0.5},{0.08,0.08,0.08,0.3},{0.08,0.08,0.08,0.3}};
        size[]={2,4,4};
        lifeTime = 3;
        interval = 0.0005;
    };
};

class CfgLights
{
    class RocketLight;

    class Aux501_light_E60R: RocketLight
    {
        color[]={0.941,0.443,1};
        intensity = 1e8;
        dayLight = 1;
        useFlare = 1;
        flareSize = 1.5;
        flareMaxDistance = 6000;
    };
};