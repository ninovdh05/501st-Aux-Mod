class cfgPatches
{
    class Aux501_Patch_ZH73
    {
        addonRootClass = "Aux501_Patch_Weapons";
        requiredAddons[]=
        {
            "Aux501_Patch_Weapons",
            "A3_Weapons_F"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_Weaps_ZH73",
            "Aux501_Weaps_ZH73_scoped"
        };
        magazines[] = 
        {
            "Aux501_Weapons_Mags_ZH7315"
        };
        ammo[] = 
        {          
            "Aux501_Weapons_Ammo_ZH73_Blaster"
        };
    };
};

class CowsSlot;
class MuzzleSlot;
class Mode_SemiAuto;

class cfgWeapons
{
    class Rifle_Long_Base_F
    {
        class WeaponSlotsInfo
        {
            class SlotInfo;
        };
        class GunParticles;
        class AnimationSources;
    };

    class Aux501_Weaps_ZH73: Rifle_Long_Base_F
    {
        scope = 2;
        displayName = "[501st] ZH-73";
        baseWeapon = "Aux501_Weaps_ZH73";
        author = "501st Aux Team";
        picture = "\SWLW_merc\rifles\ZH73\data\ui\SWLW_ZH73_ui.paa";
        model = "\SWLW_merc\rifles\ZH73\ZH73.p3d";
        handAnim[] = {"OFP2_ManSkeleton","\A3\Weapons_F_epa\LongRangeRifles\DMR_01\Data\Anim\dmr_01.rtm"};
        magazines[]=
        {
            "Aux501_Weapons_Mags_ZH7315"
        };
        descriptionShort = "";
        selectionFireAnim = "zasleh";
        class Library
        {
            libTextDesc = "";
        };
        drySound[] = {"",0.39810717,1,20};
        reloadMagazineSound[] = {"SWLW_clones\pistols\dc17\sounds\DC17_reload.wss",0.56234133,1,30};
        soundBullet[] = {};
        modes[] = {"Single","close","short","medium"};
        fireLightDuration = 0.05;
        fireLightIntensity = 0.2;
        fireLightDiffuse[] = {1,0,0};
        fireLightAmbient[] = {1,0,0};
        class Single: Mode_SemiAuto
        {
            sounds[] = {"StandardSound"};
            class BaseSoundModeType
            {
                weaponSoundEffect = "";
                closure1[] = {};
                closure2[] = {};
                soundClosure[] = {};
            };
            class StandardSound: BaseSoundModeType
            {
                weaponSoundEffect = "";
                begin1[] = {"SWLW_merc\rifles\zh73\sounds\zh73",+3db,1,2200};
                begin2[] = {"SWLW_merc\rifles\zh73\sounds\zh73",+3db,1,2200};
                begin3[] = {"SWLW_merc\rifles\zh73\sounds\zh73",+3db,1,2200};
                soundBegin[] = {"begin1",0.33,"begin2",0.33,"begin3",0.33};
            };
            reloadTime = 0.15;
            dispersion = 0.003015;
            minRange = 0;
            minRangeProbab = 0.3;
            midRange = 25;
            midRangeProbab = 0.6;
            maxRange = 50;
            maxRangeProbab = 0.1;
            aiRateOfFire = 2;
            aiRateOfFireDistance = 25;
        };
        class close: Single
        {
            showToPlayer = 0;
            aiRateOfFire = 0.25;
            aiRateOfFireDistance = 400;
            minRange = 0;
            minRangeProbab = 0.05;
            midRange = 200;
            midRangeProbab = 0.7;
            maxRange = 400;
            maxRangeProbab = 0.2;
        };
        class short: close
        {
            aiRateOfFire = 0.5;
            aiRateOfFireDistance = 500;
            minRange = 300;
            minRangeProbab = 0.2;
            midRange = 400;
            midRangeProbab = 0.7;
            maxRange = 500;
            maxRangeProbab = 0.2;
        };
        class medium: close
        {
            aiRateOfFire = 1;
            aiRateOfFireDistance = 900;
            minRange = 400;
            minRangeProbab = 0.2;
            midRange = 700;
            midRangeProbab = 0.7;
            maxRange = 900;
            maxRangeProbab = 0.2;
        };
        class WeaponSlotsInfo: WeaponSlotsInfo
        {
            class CowsSlot: CowsSlot
            {
                displayName = "Optics Slot";
                iconPicture = "\A3\Weapons_F\Data\UI\attachment_top.paa";
                iconPinpoint = "Bottom";
                iconPosition[] = {0.5,0.35};
                iconScale = 0.2;
                linkProxy = "\a3\data_f\proxies\weapon_slots\TOP";
                scope = 0;
                compatibleItems[] = 
                {
                    "SWLW_ZH73_scope"
                };
            };
            class MuzzleSlot: MuzzleSlot
            {
                compatibleItems[] = {};
            };
        };
    };

    class Aux501_Weaps_ZH73_scoped: Aux501_Weaps_ZH73
    {
        class LinkedItems
        {
            class LinkedItemsOptic
            {
                item = "SWLW_ZH73_scope";
                slot = "CowsSlot";
            };
        };
    };
};

class CfgMagazines
{
    class Aux501_Weapons_Mags_30mw10;

    class Aux501_Weapons_Mags_ZH7315: Aux501_Weapons_Mags_30mw10
    {
        displayName = "[501st] ZH-73 Blaster Cell";
        displayNameShort = "15Rnd 30MW";
        picture = "\MRC\JLTS\weapons\E5S\data\ui\E5S_mag_ui_ca.paa";
        count = 15;
        ammo = "Aux501_Weapons_Ammo_ZH73_Blaster";
        descriptionShort = "ZH73 High Power Magazine";
        model = "\MRC\JLTS\weapons\E5S\E5S_mag.p3d";
    };
};

class CfgAmmo
{
    class Aux501_Weapons_Ammo_base_red;

    class Aux501_Weapons_Ammo_ZH73_Blaster: Aux501_Weapons_Ammo_base_red
    {
        hit = 45;
        typicalSpeed = 1500;
        caliber = 2.4;
        airFriction = 0;
        waterFriction = -0.009;
        model = "SWLW_main\Effects\laser_red.p3d";
        tracerscale = 1.5;
    };
};