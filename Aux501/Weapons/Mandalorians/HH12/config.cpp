class cfgPatches
{
    class Aux501_Patch_hh12
    {
        addonRootClass = "Aux501_Patch_Weapons";
        requiredAddons[]=
        {
            "Aux501_Patch_Weapons",
            "A3_Weapons_F"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_Weaps_hh12_at",
            "Aux501_Weaps_hh12_aa"
        };
        magazines[] = 
        {
            "Aux501_Weapons_Mags_hh12_at",
            "Aux501_Weapons_Mags_hh12_he",
            "Aux501_Weapons_Mags_hh12_aa"
        };
    };
};

class CfgWeapons
{
    class launch_RPG32_F;
    class Aux501_Weaps_e60r_at: launch_RPG32_F
    {
        class Single;
    };

    class launch_Titan_short_base;
    class Aux501_Weaps_e60r_aa: launch_Titan_short_base
    {
        class Single;
        class TopDown;
    };

    class Aux501_Weaps_hh12_at: Aux501_Weaps_e60r_at
    {
        displayName = "[501st] HH-12 Unguided Launcher";
        descriptionShort = "Portable Anti-Tank Launcher";
        picture = "\IBL\weapons\HH12\data\ui\HH12_ui_ca.paa";
        uiPicture = "\IBL\weapons\HH12\data\ui\HH12_ui_ca.paa";
        model = "\IBL\weapons\HH12\HH12.p3d";
        handAnim[] = {"OFP2_ManSkeleton","\IBL\weapons\HH12\anims\HH12_handanim.rtm"};
        hiddenSelections[] = {};
        hiddenSelectionsTextures[] = {};
        magazines[] = 
        {
            "Aux501_Weapons_Mags_hh12_at",
            "Aux501_Weapons_Mags_hh12_he"
        };
        reloadAction = "GestureReloadRPG7";
        class Single: Single
        {
            class BaseSoundModeType{};
            class StandardSound: BaseSoundModeType
            {
                begin1[] = {"swlw_rework\sounds\westar\35\Westar-35X.wss",5,1,2200};
                soundBegin[] = {"begin1",1};
            };
        };
        weaponInfoType = "RscOptics_HH12";
        class GunParticles 
        {
            class effect1 
            {
                directionName = "usti hlavne";
                effectName = "";
                positionName = "konec hlavne";
            };
        };
    };

    class Aux501_Weaps_hh12_aa: Aux501_Weaps_e60r_aa
    {
        displayName = "[501st] HH-12-AA Guided Launcher";
        descriptionShort = "Portable Anti-Air Launcher";
        picture = "\IBL\weapons\HH12\data\ui\HH12_ui_ca.paa";
        uiPicture = "\IBL\weapons\HH12\data\ui\HH12_ui_ca.paa";
        model = "\IBL\weapons\HH12\HH12.p3d";
        handAnim[] = {"OFP2_ManSkeleton","\IBL\weapons\HH12\anims\HH12_handanim.rtm"};
        hiddenSelections[] = {};
        hiddenSelectionsTextures[] = {};
        magazines[] = {"Aux501_Weapons_Mags_hh12_aa"};
        reloadAction = "GestureReloadRPG7";
        weaponInfoType = "RscOptics_HH12";
        class Single: Single
        {
            class BaseSoundModeType{};
            class StandardSound: BaseSoundModeType
            {
                begin1[] = {"swlw_rework\sounds\westar\35\Westar-35X.wss",5,1,2200};
                soundBegin[] = {"begin1",1};
            };
        };
        class TopDown: TopDown
        {
            class BaseSoundModeType{};
            class StandardSound: BaseSoundModeType
            {
                begin1[] = {"swlw_rework\sounds\westar\35\Westar-35X.wss",5,1,2200};
                soundBegin[] = {"begin1",1};
            };
        };
    };
};

class CfgMagazines
{
    class Aux501_Weapons_Mags_e60r_at;
    class Aux501_Weapons_Mags_e60r_he;
    class Aux501_Weapons_Mags_e60r_aa;

    class Aux501_Weapons_Mags_hh12_at: Aux501_Weapons_Mags_e60r_at
    {
        displayName = "[501st] HH-12 AT Rocket";
        displayNameShort = "AT";
        descriptionShort = "AT Rocket";
        ammo = "Aux501_Weapons_Ammo_hh12_at";
    };
    class Aux501_Weapons_Mags_hh12_he: Aux501_Weapons_Mags_e60r_he
    {
        displayName = "[501st] HH-12 HE Rocket";
        descriptionShort = "HE Rocket";
        ammo = "Aux501_Weapons_Ammo_hh12_he";
    };
    class Aux501_Weapons_Mags_hh12_aa: Aux501_Weapons_Mags_e60r_aa
    {
        displayName = "[501st] HH-12 AA Rocket";
        descriptionShort = "AA Rocket";
        ammo = "Aux501_Weapons_Ammo_hh12_aa";
    };
};

class CfgAmmo
{
    class ammo_Penetrator_RPG32V;
    class R_PG32V_F;
    class R_TBG32V_F;

    class M_Titan_AA;

    class Aux501_Weapons_Ammo_hh12_at_sub: ammo_Penetrator_RPG32V
    {
        hit = 650;
        caliber = 43.3333;
        airFriction = -0.28;
        thrust = 210;
        thrustTime = 1.5;
        typicalSpeed = 1000;
    };
    class Aux501_Weapons_Ammo_hh12_at: R_PG32V_F
    {
        soundFly[] = {"swlw_rework\sounds\launcher\E60R_fly.wss",6,1.5,700};
        effectsMissile = "Aux501_particle_effect_HH12_fly";
        thrust = 100;
        thrustTime = 5;
        timeToLive = 5;
        submunitionAmmo = "Aux501_Weapons_Ammo_hh12_at_sub";
    };	
    class Aux501_Weapons_Ammo_hh12_he: R_TBG32V_F
    {
        soundFly[] = {"swlw_rework\sounds\launcher\E60R_fly.wss",6,1.5,700};
        effectsMissile = "Aux501_particle_effect_HH12_fly";
        thrust = 100;
        thrustTime = 5;
        timeToLive = 5;
    };
    class Aux501_Weapons_Ammo_hh12_aa: M_Titan_AA
    {
        cmImmunity = 0.5;
        soundFly[] = {"swlw_rework\sounds\launcher\E60R_fly.wss",6,1.5,700};
        effectsMissile = "Aux501_particle_effect_HH12_fly";
    };
};

class Aux501_particle_effect_HH12_fly
{
    class Light
    {
        simulation = "light";
        type = "Aux501_light_HH12";
        position[] = {0,0,0};
    };
    class Smoke
    {
        simulation = "particles";
        type = "Aux501_cloudlet_E60R_smoke";
        position[] = {0,0,0};
        qualityLevel = -1;
    };
    class Sparks
    {
        simulation = "particles";
        type = "Aux501_cloudlet_E60R_sparks";
        position[] = {0,0,0};
        qualityLevel = 2;
    };
};

class CfgLights
{
    class RocketLight;

    class Aux501_light_HH12: RocketLight
    {
        color[]={166,63,26};
        intensity = 1e8;
        dayLight = 1;
        useFlare = 1;
        flareSize = 1.5;
        flareMaxDistance = 6000;
    };
};