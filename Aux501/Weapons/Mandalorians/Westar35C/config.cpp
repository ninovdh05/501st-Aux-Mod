class cfgPatches
{
    class Aux501_Patch_Westar35C
    {
        addonRootClass = "Aux501_Patch_Weapons";
        requiredAddons[]=
        {
            "Aux501_Patch_Weapons",
            "A3_Weapons_F"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_Weaps_Westar35C"
        };
    };
};

class CowsSlot;
class UnderBarrelSlot;

class cfgWeapons
{
    class Aux501_rifle_base;
    class Aux501_rifle_base_stunless : Aux501_rifle_base
    {
        class FullAuto;
        class Single;
        class single_medium; 
        class single_far;
        class WeaponSlotsInfo;
    };

    class Aux501_Weaps_Westar35C: Aux501_rifle_base_stunless
    {
        scope = 2;
        displayName = "[501st] Westar 35C";
        baseWeapon = "Aux501_Weaps_Westar35C";
        model = "\SWLW_merc_mando\smgs\westar35c\westar35c.p3d";
		handAnim[] = {"OFP2_ManSkeleton","\A3\Weapons_F_epa\LongRangeRifles\DMR_01\Data\Anim\dmr_01.rtm"};
		picture = "\SWLW_merc_mando\smgs\westar35c\data\ui\SWLW_Westar35C_ui.paa";
        recoil = "recoil_mxm";
        fireLightDiffuse[] = {0.5,0.5,0.25};
        fireLightAmbient[] = {0.5,0.5,0.25};
        magazines[]= 
        {
            "Aux501_Weapons_Mags_Westar35C100"
        };
        modes[] = {"FullAuto","Single","single_medium","single_far"};
        class FullAuto: FullAuto
        {
            reloadTime = 0.096;
            dispersion = 0.002;
            sounds[] = {"StandardSound"};
			class BaseSoundModeType
			{
				weaponSoundEffect = "";
				closure1[] = {};
				closure2[] = {};
				soundClosure[] = {};
			};
			class StandardSound: BaseSoundModeType
			{
				weaponSoundEffect = "";
				begin1[] = {"SWLW_merc_mando\rifles\westar35s\sounds\westar35s",+3db,1,2200};
				begin2[] = {"SWLW_merc_mando\rifles\westar35s\sounds\westar35s",+3db,1,2200};
				begin3[] = {"SWLW_merc_mando\rifles\westar35s\sounds\westar35s",+3db,1,2200};
				soundBegin[] = {"begin1",0.33,"begin2",0.33,"begin3",0.33};
			};
        };
        class Single: Single
        {
            sounds[] = {"StandardSound"};
			class BaseSoundModeType
			{
				weaponSoundEffect = "";
				closure1[] = {};
				closure2[] = {};
				soundClosure[] = {};
			};
			class StandardSound: BaseSoundModeType
			{
				weaponSoundEffect = "";
				begin1[] = {"SWLW_merc_mando\rifles\westar35s\sounds\westar35s",+3db,1,2200};
				begin2[] = {"SWLW_merc_mando\rifles\westar35s\sounds\westar35s",+3db,1,2200};
				begin3[] = {"SWLW_merc_mando\rifles\westar35s\sounds\westar35s",+3db,1,2200};
				soundBegin[] = {"begin1",0.33,"begin2",0.33,"begin3",0.33};
			};
        };
        class single_medium: single_medium
        {
            sounds[] = {"StandardSound"};
			class BaseSoundModeType
			{
				weaponSoundEffect = "";
				closure1[] = {};
				closure2[] = {};
				soundClosure[] = {};
			};
			class StandardSound: BaseSoundModeType
			{
				weaponSoundEffect = "";
				begin1[] = {"SWLW_merc_mando\rifles\westar35s\sounds\westar35s",+3db,1,2200};
				begin2[] = {"SWLW_merc_mando\rifles\westar35s\sounds\westar35s",+3db,1,2200};
				begin3[] = {"SWLW_merc_mando\rifles\westar35s\sounds\westar35s",+3db,1,2200};
				soundBegin[] = {"begin1",0.33,"begin2",0.33,"begin3",0.33};
			};
        };
        class single_far: single_far
        {
            sounds[] = {"StandardSound"};
			class BaseSoundModeType
			{
				weaponSoundEffect = "";
				closure1[] = {};
				closure2[] = {};
				soundClosure[] = {};
			};
			class StandardSound: BaseSoundModeType
			{
				weaponSoundEffect = "";
				begin1[] = {"SWLW_merc_mando\rifles\westar35s\sounds\westar35s",+3db,1,2200};
				begin2[] = {"SWLW_merc_mando\rifles\westar35s\sounds\westar35s",+3db,1,2200};
				begin3[] = {"SWLW_merc_mando\rifles\westar35s\sounds\westar35s",+3db,1,2200};
				soundBegin[] = {"begin1",0.33,"begin2",0.33,"begin3",0.33};
			};
        };
    };
};

class CfgMagazines
{
    class Aux501_Weapons_Mags_E5100;

    class Aux501_Weapons_Mags_Westar35C100: Aux501_Weapons_Mags_E5100
    {
        displayName = "[501st] Westar 35C Blaster Cell";
        displayNameShort = "Westar 35C Cell";
        ammo = "Aux501_Weapons_Ammo_Westar35C";
        descriptionShort = "Westar 35C Medium Power Magazine";
    };
};

class CfgAmmo
{
    class Aux501_Weapons_Ammo_base_yellow;

    class Aux501_Weapons_Ammo_Westar35C: Aux501_Weapons_Ammo_base_yellow
    {
        hit = 10;
        dangerRadiusBulletClose = 8;
        dangerRadiusHit = 12;
        typicalSpeed = 820;
        caliber = 1;
        waterFriction = -0.009;
    };
};