class cfgPatches
{
    class Aux501_Patch_explosives
    {
        addonRootClass = "Aux501_Patch_Weapons";
        requiredAddons[]=
        {
            "Aux501_Patch_Weapons",
            "A3_Weapons_F",
            "A3_Weapons_F_Explosives"
        };
        units[] = {};
        weapons[] = {};
    };
};