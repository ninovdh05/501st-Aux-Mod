class cfgPatches
{
    class Aux501_Patch_RPS4
    {
        addonRootClass = "Aux501_Patch_Weapons";
        requiredAddons[]=
        {
            "Aux501_Patch_Weapons",
            "A3_Weapons_F"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_Weaps_RPS4"
        };
        magazines[] = 
        {
            "Aux501_Weapons_Mags_rps4_heat",
            "Aux501_Weapons_Mags_rps4_he"
        };
        ammo[] = 
        {
            "Aux501_Weapons_Ammo_rps4_heat",
            "Aux501_Weapons_Ammo_rps4_he"
        };
    };
};

class CfgWeapons
{
    class Launcher_Base_F;
    class Aux501_launcher_base: Launcher_Base_F
    {
        class Single;
    };

    class Aux501_Weaps_RPS4: Aux501_launcher_base
    {
        scope = 2;
        displayName = "[501st] RPS-4 Unguided Launcher";
        baseWeapon = "Aux501_Weaps_RPS4";
        picture = "\Aux501\Weapons\Republic\RPS4\data\textures\UI\rps4_ui_ca.paa";
        uiPicture = "\Aux501\Weapons\Republic\RPS4\data\textures\UI\rps4_ui_ca.paa";
        model = "\MRC\JLTS\weapons\RPS6\rps6.p3d";
        handAnim[] = {"OFP2_ManSkeleton","\MRC\JLTS\weapons\RPS6\anims\RPS6_handanim.rtm"};
        hiddenSelections[]={"camo1"};
        hiddenSelectionsTextures[]={"\Aux501\Weapons\Republic\RPS4\data\textures\rps4_co.paa"};
        weaponInfoType="RscOpticsRangeFinderNLAW";
        recoil="";
        magazines[] = 
        {
            "Aux501_Weapons_Mags_rps4_heat",
            "Aux501_Weapons_Mags_rps4_he"
        };
        magazineWell[] = {};
        class Single: Single
        {
            sounds[] = {"StandardSound"};
            class BaseSoundModeType{};
            class StandardSound: BaseSoundModeType
            {
                begin1[] = {"A3\Sounds_F\arsenal\weapons\Launchers\RPG32\rpg32",1.99526,1,1500};
                soundBegin[] = {"begin1",1};
                soundSetShot[] = {"Launcher_RPG32_Shot_SoundSet","Launcher_RPG32_Tail_SoundSet"};
            };
            maxRange = 700;
            maxRangeProbab = 0.1;
            midRange = 400;
            midRangeProbab = 0.8;
            minRange = 50;
            minRangeProbab = 0.3;
        };
        modelOptics = "SWLW_clones\launchers\rps6\scope.p3d";
        class OpticsModes 
        {
            class optic 
            {
                cameraDir = "look";
                distanceZoomMax = 300;
                distanceZoomMin = 300;
                memoryPointCamera = "eye";
                opticsDisablePeripherialVision = 1;
                opticsFlare = 1;
                opticsID = 1;
                opticsPPEffects = ["OpticsCHAbera1","OpticsBlur1"];
                opticsZoomInit = 0.0875;
                opticsZoomMax = 0.0875;
                opticsZoomMin = 0.0875;
                useModelOptics = 1;
                visionMode = ["Normal","NVG"];
            };
        };
    };
};

class CfgMagazines
{
    class RPG32_F;
    class RPG32_HE_F;

    class Aux501_Weapons_Mags_rps4_heat: RPG32_F 
    {
        displayName = "[501st] RPS-4 HEAT Rocket";
        descriptionShort = "HEAT Rocket";
        author = "501st Aux Team";
        ammo= "Aux501_Weapons_Ammo_rps4_heat";
        mass = 70;
    };
    class Aux501_Weapons_Mags_rps4_he: RPG32_HE_F
    {
        displayName = "[501st] RPS-4 HE Rocket";
        descriptionShort = "HE Rocket";
        author = "501st Aux Team";
        ammo= "Aux501_Weapons_Ammo_rps4_he";
    };
};

class CfgAmmo
{
    class R_PG32V_F;
    class R_TBG32V_F;

    class Aux501_Weapons_Ammo_rps4_heat: R_PG32V_F
    {
        hit = 150;
        submunitionAmmo = "ammo_Penetrator_Titan_AT";
    };
    class Aux501_Weapons_Ammo_rps4_he: R_TBG32V_F
    {
        
    };
};