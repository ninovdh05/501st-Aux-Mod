class cfgPatches
{
    class Aux501_Patch_Razorblade
    {
        addonRootClass = "Aux501_Patch_Weapons";
        requiredAddons[]=
        {
            "Aux501_Patch_Weapons",
            "A3_Weapons_F"
        };
        units[] = {};
        weapons[] = 
        {
           "Aux501_Weaps_Razorblade"
        };
        magazines[] = 
        {
            "Aux501_Weapons_Mags_30mw7"
        };
    };
};

class cfgWeapons
{
    class Pistol_Base_F;
    class hgun_P07_F;
    class Aux501_stun_muzzle;
    class Aux501_pistol_base: hgun_P07_F
    {
        class Single;
        class WeaponSlotsInfo;
    };

    class Aux501_Weaps_Razorblade: Aux501_pistol_base
    {
        scope = 2;
        displayName = "[501st] DC-15SA 'Razorblade'";
        descriptionShort = "Heavy Caliber Sidearm";
        baseWeapon = "Aux501_Weaps_Razorblade";
        model = "\SWLW_clones_spec\DC15SA.p3d";
        picture = "\SWLW_clones_spec\data\ui\DC15SA_ui.paa";
        reloadMagazineSound[] = {"SWLW_clones_spec\sounds\DC15SA_reload.wss",3,1,30};
        magazines[] =
        {
            "Aux501_Weapons_Mags_30mw7"
        };
        class Single: Single
        {
            reloadTime = 0.75;
            sounds[] = {"StandardSound"};
            class BaseSoundModeType
            {
                weaponSoundEffect = "";
                closure1[] = {};
                closure2[] = {};
                soundClosure[] = {};
            };
            class StandardSound: BaseSoundModeType
            {
                weaponSoundEffect = "";
                begin1[] = {"SWLW_clones_spec\sounds\DC15SA_fire.wss",+3db,1,2200};
                begin2[] = {"SWLW_clones_spec\sounds\DC15SA_fire.wss",+3db,1,2200};
                begin3[] = {"SWLW_clones_spec\sounds\DC15SA_fire.wss",+3db,1,2200};
                begin4[] = {"SWLW_clones_spec\sounds\DC15SA_fire.wss",+3db,1,2200};
                soundBegin[] = {"begin1",0.25,"begin2",0.25,"begin3",0.25,"begin4",0.25};
                closure1[] = {};
                closure2[] = {};
                soundClosure[] = {};
            };
        };
        class WeaponSlotsInfo: WeaponSlotsInfo{};
    };
};

class CfgMagazines
{
    class Aux501_Weapons_Mags_20mw40;

    class Aux501_Weapons_Mags_30mw7: Aux501_Weapons_Mags_20mw40
    {
        displayName = "[501st] 7Rnd 30MW Cell";
        displayNameShort = "7Rnd 30MW";
        picture = "\Aux501\Weapons\Magazines\data\Aux501_icon_mag_razorblade_ca.paa";
        count = 10;
        ammo = "Aux501_Weapons_Ammo_30mw";
        initSpeed = 900;
        model = "\MRC\JLTS\weapons\DC17SA\DC17SA_mag.p3d";
        descriptionShort = "Razorblade magazine";
    };
};