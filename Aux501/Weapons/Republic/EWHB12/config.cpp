class cfgPatches
{
    class Aux501_Patch_EWHB12
    {
        addonRootClass = "Aux501_Patch_Weapons";
        requiredAddons[]=
        {
            "Aux501_Patch_Weapons",
            "A3_Weapons_F"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_Weaps_EWHB12",
            "Aux501_Weaps_EWHB12_carry"
        };
        magazines[] = 
        {
            "Aux501_Weapons_Mags_EWHB12",
            "Aux501_Weapons_Mags_EWHB12_csw",

            "Aux501_Weapons_Mags_EWHB12_GL",
            "Aux501_Weapons_Mags_EWHB12_GL_csw"
        };
    };
};

class Mode_SemiAuto;

class cfgWeapons
{
    class MGun;
    class LMG_RCWS: MGun
    {
        class manual: Mgun{};
        class close: manual{};
        class short: close{};
        class medium: close{};
        class far: close{};
    };
    class Launcher_Base_F;

    class Aux501_Weaps_EWHB12: LMG_RCWS
    {
        author = "501st Aux Team";
        displayName = "EWHB-12 'Boomer' Cannon";
        magazineReloadTime = 2;
        maxZeroing = 2000;
        class GunParticles{};
        showAimCursorInternal = 0;
        magazines[]=
        {
            "Aux501_Weapons_Mags_EWHB12_MG",
            "Aux501_Weapons_Mags_EWHB12_GL"
        };
        modes[] = 
        {
            "manual",
            "Single",
            "close",
            "short",
            "medium",
            "far"
        };
        class Single: Mode_SemiAuto
        {
            reloadTime = 0.09;
            sounds[] = {"StandardSound"};
            class BaseSoundModeType
            {
                weaponSoundEffect = "";
                closure1[] = {};
                closure2[] = {};
                soundClosure[] = {};
            };
            class StandardSound: BaseSoundModeType
            {
                weaponSoundEffect = "";
                begin1[] = {"\SWLW_merc_trando\smgs\acpr\sounds\acpr",1,1,1800};
                begin2[] = {"\SWLW_merc_trando\smgs\acpr\sounds\acpr",1,1,1800};
                begin3[] = {"\SWLW_merc_trando\smgs\acpr\sounds\acpr",1,1,1800};
                soundBegin[] = {"begin1",0.33,"begin2",0.33,"begin3",0.33};
            };
        };
        class manual: MGun
        {
            displayName = "EWHB-12 Cannon";
            dispersion = 0.011;
            reloadTime = 0.05;
            sounds[] = {"StandardSound"};
            class BaseSoundModeType
            {
                weaponSoundEffect = "";
                closure1[] = {};
                closure2[] = {};
                soundClosure[] = {};
            };
            class StandardSound: BaseSoundModeType
            {
                weaponSoundEffect = "";
                begin1[] = {"\SWLW_merc_trando\smgs\acpr\sounds\acpr",1,1,1800};
                begin2[] = {"\SWLW_merc_trando\smgs\acpr\sounds\acpr",1,1,1800};
                begin3[] = {"\SWLW_merc_trando\smgs\acpr\sounds\acpr",1,1,1800};
                soundBegin[] = {"begin1",0.33,"begin2",0.33,"begin3",0.33};
            };
        };
        class close: manual
        {
            aiBurstTerminable = 1;
            showToPlayer = 0;
            burst = 8;
            burstRangeMax = 16;
            aiRateOfFire = 0.5;
            aiRateOfFireDispersion = 1.5;
            aiRateOfFireDistance = 50;
            minRange = 0;
            minRangeProbab = 0.7;
            midRange = 100;
            midRangeProbab = 0.7;
            maxRange = 200;
            maxRangeProbab = 0.2;
        };
        class short: close
        {
            aiBurstTerminable = 1;
            showToPlayer = 0;
            burst = 6;
            burstRangeMax = 16;
            aiRateOfFire = 1;
            aiRateOfFireDispersion = 2;
            aiRateOfFireDistance = 150;
            minRange = 100;
            minRangeProbab = 0.7;
            midRange = 400;
            midRangeProbab = 0.75;
            maxRange = 800;
            maxRangeProbab = 0.2;
        };
        class medium: close
        {
            aiBurstTerminable = 1;
            showToPlayer = 0;
            burst = 4;
            burstRangeMax = 12;
            aiRateOfFire = 2;
            aiRateOfFireDispersion = 2;
            aiRateOfFireDistance = 400;
            minRange = 400;
            minRangeProbab = 0.75;
            midRange = 800;
            midRangeProbab = 0.7;
            maxRange = 1500;
            maxRangeProbab = 0.1;
        };
        class far: close
        {
            aiBurstTerminable = 1;
            showToPlayer = 0;
            burst = 3;
            burstRangeMax = 12;
            aiRateOfFire = 4;
            aiRateOfFireDispersion = 4;
            aiRateOfFireDistance = 800;
            minRange = 800;
            minRangeProbab = 0.6;
            midRange = 1500;
            midRangeProbab = 0.25;
            maxRange = 2000;
            maxRangeProbab = 0.05;
        };
    };
    class Aux501_Weaps_EWHB12_carry: Launcher_Base_F
    {
        scope = 2;
        displayName = "[501st] EWHB-12 'Boomer'";
        author = "501st Aux Team";
        model = "SWLW_clones\machineguns\z7\z7.p3d";
        picture = "\Aux501\Weapons\Republic\EWHB12\data\UI\Aux501_launcher_staticweapons_boomer_ca.paa";
        mass = 450;
        class ACE_CSW
        {
            type = "mount";
            deployTime = 1;
            pickupTime = 1;
            deploy = "RD501_stat_reweb";
        };
    };
};

class ACE_CSW_Groups
{
    class Aux501_Weapons_Mags_EWHB12_MG_csw
    {
        Aux501_Weapons_Mags_EWHB12_MG = 1;
    };
    class Aux501_Weapons_Mags_EWHB12_GL_csw
    {
        Aux501_Weapons_Mags_EWHB12_GL = 1;
    };
};

class CfgMagazines
{
    class VehicleMagazine;
    class 40Rnd_20mm_G_belt;

    class Aux501_Weapons_Mags_EWHB12_MG: VehicleMagazine
    {
        scope = 1;
        scopeArsenal = 1;
        author = "501st Aux Team";
        displayName = "[501st] EWHB-12 EWEB Charge";
        displayNameShort = "500Rnd 30MW";
        descriptionShort = "Heavy Republic Railgun Cell";
        picture = "\Aux501\Weapons\Magazines\data\Aux501_icon_mag_staticeweb_ca.paa";
        count = 500;
        tracersEvery = 1;
        initSpeed = 750;
        maxLeadSpeed = 36.1111;
        nameSound = "mgun";
        ammo = "Aux501_Weapons_Ammo_30mw";
        model = "SWLW_merc_trando\machineguns\ls150\ls150_mag.p3d";
        weaponpoolavailable = 1;
        mass = 50;
    };
    class Aux501_Weapons_Mags_EWHB12_MG_csw: Aux501_Weapons_Mags_EWHB12_MG
    {
        scope = 2;
        scopeArsenal = 2;
        type = 256;
        ACE_isBelt = 1;
    };
    class Aux501_Weapons_Mags_EWHB12_GL: 40Rnd_20mm_G_belt
    {
        scope = 1;
        scopeArsenal = 1;
        author = "501st Aux Team";
        displayName= "[501st] EWHB-12 'Boomer' belt";
        displayNameShort = "40Rnd HE";
        picture = "\Aux501\Weapons\Magazines\data\Aux501_icon_mag_staticgl_ca.paa";
        model = "\A3\Structures_F_EPB\Items\Military\Ammobox_rounds_F.p3d";
        muzzleImpulseFactor[] = {0,0};
        count = 40;
        ammo = "Aux501_Weapons_Ammo_GL_HE";
        weaponpoolavailable = 1;
        mass = 50;	
    };
    class Aux501_Weapons_Mags_EWHB12_GL_csw: Aux501_Weapons_Mags_EWHB12_GL
    {
        scope = 2;
        scopeArsenal = 2;
        type = 256;
        ACE_isBelt = 1;
    };
};