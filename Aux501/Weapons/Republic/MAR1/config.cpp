class cfgPatches
{
    class Aux501_Patch_MAR1
    {
        addonRootClass = "Aux501_Patch_Weapons";
        requiredAddons[]=
        {
            "Aux501_Patch_Weapons",
            "A3_Weapons_F"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_Weaps_MAR1",
            "Aux501_Weaps_MAR1_carry"
        };
        magazines[] = 
        {
            "Aux501_Weapons_Mags_mar1",
            "Aux501_Weapons_Mags_mar1_csw"
        };
        ammo[] = 
        {
            "Aux501_Weapons_Ammo_mar1"
        };
    };
};

class cfgWeapons
{
    class MGun;
    class LMG_RCWS;
    class Launcher_Base_F;

    class Aux501_Weaps_MAR1: LMG_RCWS
    {
        author = "501st Aux Team";
        displayName = "MAR1 'Driver' Railgun";
        magazineReloadTime = 1;
        maxZeroing = 2000;
        class GunParticles{};
        showAimCursorInternal = 0;
        magazines[]=
        {
            "Aux501_Weapons_Mags_mar1"
        };
        class manual: MGun
        {
            reloadTime = 2;
            dispersion = 0.00001;
            sounds[] = {"StandardSound"};
            class BaseSoundModeType
            {
                weaponSoundEffect = "";
                begin1[] = {"\Aux501\Weapons\Republic\MAR1\data\sounds\MAR1_shot.wss",+5db,1,2200};
                closure1[] = {};
                closure2[] = {};
                soundClosure[] = {};
            };
            class StandardSound: BaseSoundModeType
            {
                weaponSoundEffect = "";
                begin1[] = {"\Aux501\Weapons\Republic\MAR1\data\sounds\MAR1_shot.wss",+5db,1,2200};
                soundBegin[] = {"begin1",1};
                closure1[] = {};
                closure2[] = {};
                soundClosure[] = {};
            };
        };
        class close: manual
        {
            aiBurstTerminable = 1;
            showToPlayer = 0;
            burst = 8;
            burstRangeMax = 16;
            aiRateOfFire = 0.5;
            aiRateOfFireDispersion = 1.5;
            aiRateOfFireDistance = 50;
            minRange = 0;
            minRangeProbab = 0.7;
            midRange = 100;
            midRangeProbab = 0.7;
            maxRange = 200;
            maxRangeProbab = 0.2;
        };
        class short: close
        {
            aiBurstTerminable = 1;
            showToPlayer = 0;
            burst = 6;
            burstRangeMax = 16;
            aiRateOfFire = 1;
            aiRateOfFireDispersion = 2;
            aiRateOfFireDistance = 150;
            minRange = 100;
            minRangeProbab = 0.7;
            midRange = 400;
            midRangeProbab = 0.75;
            maxRange = 800;
            maxRangeProbab = 0.2;
        };
        class medium: close
        {
            aiBurstTerminable = 1;
            showToPlayer = 0;
            burst = 4;
            burstRangeMax = 12;
            aiRateOfFire = 2;
            aiRateOfFireDispersion = 2;
            aiRateOfFireDistance = 400;
            minRange = 400;
            minRangeProbab = 0.75;
            midRange = 800;
            midRangeProbab = 0.7;
            maxRange = 1500;
            maxRangeProbab = 0.1;
        };
        class far: close
        {
            aiBurstTerminable = 1;
            showToPlayer = 0;
            burst = 3;
            burstRangeMax = 12;
            aiRateOfFire = 4;
            aiRateOfFireDispersion = 4;
            aiRateOfFireDistance = 800;
            minRange = 800;
            minRangeProbab = 0.6;
            midRange = 1500;
            midRangeProbab = 0.25;
            maxRange = 2000;
            maxRangeProbab = 0.05;
        };
    };
    class Aux501_Weaps_MAR1_carry: Launcher_Base_F
    {
        scope = 2;
        displayName = "[501st] MAR1 'Driver'";
        author = "501st Aux Team";
        model = "ls_weapons\tertiary\rps6\ls_weapon_rps6.p3d";
        picture = "\Aux501\Weapons\Republic\MAR1\data\UI\mar1_carry_ui_ca.paa";
        mass = 450;
        class ACE_CSW
        {
            type = "mount";
            deployTime = 15;
            pickupTime = 1;
            deploy = "RD501_stat_Railgun";
        };
    };
};

class ACE_CSW_Groups
{
    class Aux501_Weapons_Mags_mar1_csw
    {
        Aux501_Weapons_Mags_mar1 = 1;
    };
};

class CfgMagazines
{
    class VehicleMagazine;

    class Aux501_Weapons_Mags_mar1: VehicleMagazine
    {
        scope = 1;
        scopeArsenal = 1;
        author = "501st Aux Team";
        displayName = "[501st] 10rnd MAR1 'Driver' Slugs";
        displayNameShort = "10Rnd Slugs";
        descriptionShort = "Heavy Republic Railgun Cell";
        picture = "\Aux501\Weapons\Magazines\data\Aux501_icon_mag_staticrailgun_ca.paa";
        count = 10;
        tracersEvery = 1;
        initSpeed = 1500;
        maxLeadSpeed = 36.1111;
        nameSound = "mgun";
        ammo = "Aux501_Weapons_Ammo_mar1";
        model = "SWLW_clones\launchers\rps6\RPS6_mag.p3d";
        weaponpoolavailable = 1;
        mass = 95;
    };
    class Aux501_Weapons_Mags_mar1_csw: Aux501_Weapons_Mags_mar1
    {
        scope = 2;
        scopeArsenal = 2;
        type = 256;
        ACE_isBelt = 1;
    };
};

class CfgAmmo
{
    class Aux501_Weapons_Ammo_base_blue;

    class Aux501_Weapons_Ammo_mar1: Aux501_Weapons_Ammo_base_blue
    {
        effectFly = "Aux501_particle_effect_heavybolt_fly_blue";
        model = "\Aux501\Weapons\Ammo\data\Aux501_Weapons_Ammo_Tracers_Big_Blue.p3d";
        hit = 1000;
        thrust = 500;
        typicalSpeed = 3000;
        caliber = 50;
    };
};