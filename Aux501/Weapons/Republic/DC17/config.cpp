class cfgPatches
{
    class Aux501_Patch_DC17
    {
        addonRootClass = "Aux501_Patch_Weapons";
        requiredAddons[]=
        {
            "Aux501_Patch_Weapons",
            "A3_Weapons_F"
        };
        units[] = {};
        weapons[] = 
        {
           "Aux501_Weaps_DC17",
           "Aux501_Weaps_DC17_Shield",

           "Aux501_Weaps_DC17A",
           "Aux501_Weaps_DC17A_Shield",

           "Aux501_Weaps_DC17_Signal"
        };
        magazines[] = 
        {
            "Aux501_Weapons_Mags_20mwdp20",
            "Aux501_Weapons_Mags_20mw30",
            "Aux501_Weapons_Mags_20mwdp40_dual",

            "Aux501_Weapons_Mags_GL_smoke_white1",
            "Aux501_Weapons_Mags_GL_smoke_purple1",
            "Aux501_Weapons_Mags_GL_smoke_yellow1",
            "Aux501_Weapons_Mags_GL_smoke_red1",
            "Aux501_Weapons_Mags_GL_smoke_green1",
            "Aux501_Weapons_Mags_GL_smoke_blue1",
            "Aux501_Weapons_Mags_GL_smoke_orange1",

            "Aux501_Weapons_Mags_GL_flare_white1",
            "Aux501_Weapons_Mags_GL_flare_IR1",
            "Aux501_Weapons_Mags_GL_flare_Green1",
            "Aux501_Weapons_Mags_GL_flare_Red1",
            "Aux501_Weapons_Mags_GL_flare_Yellow1",
            "Aux501_Weapons_Mags_GL_flare_Blue1",
            "Aux501_Weapons_Mags_GL_flare_Cyan1",
            "Aux501_Weapons_Mags_GL_flare_Purple1"
        };
    };
};

class Mode_FullAuto;

class CowsSlot;

class cfgWeapons
{
    class Pistol_Base_F;
    class hgun_P07_F;
    class Aux501_stun_muzzle;
    class Aux501_pistol_base: hgun_P07_F
    {
        class Single;
        class WeaponSlotsInfo;
    };

    class Aux501_Weaps_DC17: Aux501_pistol_base
    {
        scope = 2;
        displayName = "[501st] DC-17";
        baseWeapon = "Aux501_Weaps_DC17";
        JLTS_canHaveShield = 1;
        JLTS_shieldedWeapon = "Aux501_Weaps_DC17_Shield";
        picture = "\MRC\JLTS\weapons\DC17SA\data\ui\DC17SA_ui_ca.paa";
        model = "\MRC\JLTS\weapons\DC17SA\DC17SA.p3d";
        magazines[] =
        {
            "Aux501_Weapons_Mags_20mwdp20"
        };
        minRange = 0;
        minRangeProbab = 0.1;
        midRange = 50;
        midRangeProbab = 0.3;
        maxRange = 180;
        maxRangeProbab = 0.04;
        aiRateOfFire = 0.5;
        aiRateOfFireDistance = 50;
        class Single: Single
        {
            sounds[] = {"StandardSound"};
            class BaseSoundModeType
            {
                weaponSoundEffect = "";
                closure1[] = {};
                closure2[] = {};
                soundClosure[] = {};
            };
            class StandardSound: BaseSoundModeType
            {
                weaponSoundEffect = "";
                begin1[] = {"\Aux501\Weapons\Republic\DC17\data\sounds\DC17Fire.wss",+3db,1,2200};
                soundBegin[] = {"begin1",1};
            };
            aiDispersionCoefY = 1.7;
            aiDispersionCoefX = 1.4;
            minRange = 1;
            minRangeProbab = 0.3;
            midRange = 30;
            midRangeProbab = 0.6;
            maxRange = 75;
            maxRangeProbab = 0.1;
            aiRateOfFire = 1;
            aiRateOfFireDistance = 30;
        };
        class WeaponSlotsInfo: WeaponSlotsInfo{};
    };
    class Aux501_Weaps_DC17_Shield: Aux501_Weaps_DC17
    {
        scope = 1;
        displayName = "[501st] DC-17 (Shield)";
        baseWeapon = "Aux501_Weaps_DC17_Shield";
        JLTS_isShielded = 1;
        JLTS_baseWeapon = "Aux501_Weaps_DC17";
        model = "\MRC\JLTS\weapons\DC17SA\DC17SA_shielded.p3d";
        handAnim[] = {"OFP2_ManSkeleton","\MRC\JLTS\weapons\DC17SA\anims\DC17SA_shielded_handanim.rtm"};
        inertia = 0.8;
        recoil = "recoil_pdw";
        reloadAction="";
        class WeaponSlotsInfo: WeaponSlotsInfo
        {
            mass=110;
            class UnderBarrelSlot
            {
                linkProxy="\A3\Data_F_Mark\Proxies\Weapon_Slots\UNDERBARREL";
                iconPicture="\A3\Weapons_F_Mark\Data\UI\attachment_under.paa";
                iconPinpoint="Bottom";
                compatibleItems[]=
                {
                    "JLTS_riot_shield_attachment",
                    "JLTS_riot_shield_212_attachment",
                    "JLTS_riot_shield_501_attachment",
                    "JLTS_riot_shield_101_attachment",
                    "JLTS_riot_shield_CG_attachment",
                    "JLTS_riot_shield_GD_attachment",
                    "JLTS_riot_shield_droid_attachment"
                };
            };
        };
    };
    
    class Aux501_Weaps_DC17A: Aux501_Weaps_DC17
    {
        displayName = "[501st] DC-17A";
        baseWeapon = "Aux501_Weaps_DC17A";
        JLTS_canHaveShield = 1;
        JLTS_shieldedWeapon = "Aux501_Weaps_DC17A_Shield";
        magazines[] =
        {
            "Aux501_Weapons_Mags_20mw30"
        };
        modes[] = {"FullAuto","Single"};
        recoil = "recoil_SMG_03";
        class FullAuto: Single
        {
            autoFire = 1;
            reloadTime = 0.1;
            displayName = "$STR_DN_MODE_FULLAUTO";
            textureType = "fullAuto";
            aiDispersionCoefY = 3;
            aiDispersionCoefX = 2;
            soundBurst = 0;
            sounds[] = {"StandardSound"};
            class BaseSoundModeType
            {
                weaponSoundEffect = "";
                closure1[] = {};
                closure2[] = {};
                soundClosure[] = {};
            };
            class StandardSound: BaseSoundModeType
            {
                weaponSoundEffect = "";
                begin1[] = {"\Aux501\Weapons\Republic\DC17\data\sounds\DC17Fire.wss",+3db,1,2200};
                soundBegin[] = {"begin1",1};
            };
        };
        class WeaponSlotsInfo: WeaponSlotsInfo{};
    };
    class Aux501_Weaps_DC17A_Shield: Aux501_Weaps_DC17A
    {
        scope = 1;
        displayName = "[501st] DC-17A (Shield)";
        baseWeapon = "Aux501_Weaps_DC17A_Shield";
        JLTS_isShielded = 1;
        JLTS_baseWeapon = "Aux501_Weaps_DC17A";
        model = "\MRC\JLTS\weapons\DC17SA\DC17SA_shielded.p3d";
        handAnim[] = {"OFP2_ManSkeleton","\MRC\JLTS\weapons\DC17SA\anims\DC17SA_shielded_handanim.rtm"};
        inertia = 0.8;
        recoil = "recoil_pdw";
        reloadAction="";
        class WeaponSlotsInfo: WeaponSlotsInfo
        {
            mass = 110;
            class UnderBarrelSlot
            {
                linkProxy="\A3\Data_F_Mark\Proxies\Weapon_Slots\UNDERBARREL";
                iconPicture="\A3\Weapons_F_Mark\Data\UI\attachment_under.paa";
                iconPinpoint="Bottom";
                compatibleItems[]=
                {
                    "JLTS_riot_shield_attachment",
                    "JLTS_riot_shield_212_attachment",
                    "JLTS_riot_shield_501_attachment",
                    "JLTS_riot_shield_101_attachment",
                    "JLTS_riot_shield_CG_attachment",
                    "JLTS_riot_shield_GD_attachment",
                    "JLTS_riot_shield_droid_attachment"
                };
            };
        };
    };
    
    class Aux501_Weaps_DC17_Signal: Aux501_Weaps_DC17
    {
        scope = 2;
        displayName = "[501st] DC-17 Signal Pistol";
        baseWeapon = "Aux501_Weaps_DC17_Signal";
        JLTS_canHaveShield = 0;
        magazines[] =
        {
            "Aux501_Weapons_Mags_GL_flare_White1",
            "Aux501_Weapons_Mags_GL_flare_IR1",
            "Aux501_Weapons_Mags_GL_flare_Green10",
            "Aux501_Weapons_Mags_GL_flare_Red1",
            "Aux501_Weapons_Mags_GL_flare_Yellow1",
            "Aux501_Weapons_Mags_GL_flare_Blue1",
            "Aux501_Weapons_Mags_GL_flare_Cyan1",
            "Aux501_Weapons_Mags_GL_flare_Purple1",
            "Aux501_Weapons_Mags_GL_smoke_white6",
            "Aux501_Weapons_Mags_GL_smoke_purple3",
            "Aux501_Weapons_Mags_GL_smoke_yellow3",
            "Aux501_Weapons_Mags_GL_smoke_red3",
            "Aux501_Weapons_Mags_GL_smoke_green3",
            "Aux501_Weapons_Mags_GL_smoke_blue3",
            "Aux501_Weapons_Mags_GL_smoke_orange3",
            "ACE_HuntIR_M203"
        };
        class WeaponSlotsInfo: WeaponSlotsInfo{};
    };
};

class CfgMagazines
{
    class Aux501_Weapons_Mags_20mw40;
    class Aux501_Weapons_Mags_20mwdp30;

    class Aux501_Weapons_Mags_GL_smoke_white6;
    class Aux501_Weapons_Mags_GL_smoke_purple3;
    class Aux501_Weapons_Mags_GL_smoke_yellow3;
    class Aux501_Weapons_Mags_GL_smoke_red3;
    class Aux501_Weapons_Mags_GL_smoke_green3;
    class Aux501_Weapons_Mags_GL_smoke_blue3;
    class Aux501_Weapons_Mags_GL_smoke_orange3;

    class Aux501_Weapons_Mags_GL_flare_white3;
    class Aux501_Weapons_Mags_GL_flare_IR3;
    class Aux501_Weapons_Mags_GL_flare_Green3;
    class Aux501_Weapons_Mags_GL_flare_Red3;
    class Aux501_Weapons_Mags_GL_flare_Yellow3;
    class Aux501_Weapons_Mags_GL_flare_Blue3;
    class Aux501_Weapons_Mags_GL_flare_Cyan3;
    class Aux501_Weapons_Mags_GL_flare_Purple3;
    
    //Pistol Magazines
    class Aux501_Weapons_Mags_20mwdp20: Aux501_Weapons_Mags_20mwdp30
    {
        displayName = "[501st] 20Rnd 20MW DP Cell";
        displayNameShort = "20Rnd 20MW DP";
        picture = "\Aux501\Weapons\Magazines\data\Aux501_icon_mag_dc17_ca.paa";
        count = 20;
        mass = 4;
        descriptionShort = "DC17 Pistol Magazine";
        model = "\MRC\JLTS\weapons\DC17SA\DC17SA_mag.p3d";
    };
    class Aux501_Weapons_Mags_20mw30: Aux501_Weapons_Mags_20mw40
    {
        displayName = "[501st] 30Rnd 20MW Cell";
        displayNameShort = "30Rnd 20MW";
        picture = "\Aux501\Weapons\Magazines\data\Aux501_icon_mag_dc17a_ca.paa";
        count = 30;
        mass = 4;
        model = "\MRC\JLTS\weapons\DC17SA\DC17SA_mag.p3d";
        descriptionShort = "DC17A magazine";
    };
    class Aux501_Weapons_Mags_20mwdp40_dual: Aux501_Weapons_Mags_20mwdp30
    {
        displayName = "[501st] 40Rnd 20MW DP Dual Cell";
        displayNameShort = "40Rnd 20MW DP";
        count = 40;
        mass = 4;
        descriptionShort = "Dual DC17 Pistol Magazine";
    };

    //Signal Pistol Magazines
    class Aux501_Weapons_Mags_GL_smoke_white1: Aux501_Weapons_Mags_GL_smoke_white6
    {
        displayName = "[501st] 1Rnd Smoke (White)";
        displayNameShort = "1Rnd White";
        descriptionShort = "White Signal Smoke Grenade";
        count = 1;
    };
    class Aux501_Weapons_Mags_GL_smoke_purple1: Aux501_Weapons_Mags_GL_smoke_purple3
    {
        displayName = "[501st] 1Rnd Smoke (Purple)";
        displayNameShort = "1Rnd Purple";
        descriptionShort = "Purple Signal Smoke Grenade";
        count = 1;
    };
    class Aux501_Weapons_Mags_GL_smoke_yellow1: Aux501_Weapons_Mags_GL_smoke_yellow3
    {
        displayName = "[501st] 1Rnd Smoke (Yellow)";
        displayNameShort = "1Rnd Yellow";
        descriptionShort = "Yellow Signal Smoke Grenade";
        count = 1;
    };
    class Aux501_Weapons_Mags_GL_smoke_red1: Aux501_Weapons_Mags_GL_smoke_red3
    {
        displayName = "[501st] 1Rnd Smoke (Red)";
        displayNameShort = "1Rnd Red";
        descriptionShort = "Red Signal Smoke Grenade";
        count = 1;
    };
    class Aux501_Weapons_Mags_GL_smoke_green1: Aux501_Weapons_Mags_GL_smoke_green3
    {
        displayName = "[501st] 1Rnd Smoke (Green)";
        displayNameShort = "1Rnd Green";
        descriptionShort = "Green Signal Smoke Grenade";
        count = 1;
    };
    class Aux501_Weapons_Mags_GL_smoke_blue1: Aux501_Weapons_Mags_GL_smoke_blue3
    {
        displayName = "[501st] 1Rnd Smoke (Blue)";
        displayNameShort = "1Rnd Blue";
        descriptionShort = "Blue Signal Smoke Grenade";
        count = 1;
    };
    class Aux501_Weapons_Mags_GL_smoke_orange1: Aux501_Weapons_Mags_GL_smoke_orange3
    {
        displayName = "[501st] 1Rnd Smoke (Orange)";
        displayNameShort = "1Rnd Orange";
        descriptionShort = "Orange Signal Smoke Grenade";
        count = 1;
    };
    
    //Flares
    class Aux501_Weapons_Mags_GL_flare_White1: Aux501_Weapons_Mags_GL_flare_white3
    {
        displayName = "[501st] 1Rnd Flare (White)";
        displayNameShort = "White Flare";
        descriptionShort = "Signal Pistol White Flare";
        mass = 1;
        count = 1;
    };
    class Aux501_Weapons_Mags_GL_flare_IR1: Aux501_Weapons_Mags_GL_flare_IR3
    {
        displayName = "[501st] 1Rnd Flare (IR)";
        displayNameShort = "IR Flare";
        descriptionShort = "Signal Pistol IR Flare";
        mass = 1;
        count = 1;
    };
    class Aux501_Weapons_Mags_GL_flare_Green1: Aux501_Weapons_Mags_GL_flare_Green3
    {
        displayName = "[501st] 1Rnd Flare (Green)";;
        displayNameShort = "Green Flare";
        descriptionShort = "Signal Pistol Green Flare";
        mass = 1;
        count = 1;
    };
    class Aux501_Weapons_Mags_GL_flare_Red1: Aux501_Weapons_Mags_GL_flare_Red3
    {
        displayName = "[501st] 1Rnd Flare (Red)";
        displayNameShort = "Red Flare";
        descriptionShort = "Signal Pistol Red Flare";
        mass = 1;
        count = 1;
    };
    class Aux501_Weapons_Mags_GL_flare_Yellow1: Aux501_Weapons_Mags_GL_flare_Yellow3
    {
        displayName = "[501st] 1Rnd Flare (Yellow)";
        displayNameShort = "Yellow Flare";
        descriptionShort = "Signal Pistol Yellow Flare";
        mass = 1;
        count = 1;
    };
    class Aux501_Weapons_Mags_GL_flare_Blue1: Aux501_Weapons_Mags_GL_flare_Blue3
    {
        displayName = "[501st] 1Rnd Flare (Blue)";;
        displayNameShort = "Blue Flare";
        descriptionShort = "Signal Pistol Blue Flare";
        mass = 1;
        count = 1;
    };
    class Aux501_Weapons_Mags_GL_flare_Cyan1: Aux501_Weapons_Mags_GL_flare_Cyan3
    {
        displayName = "[501st] 1Rnd Flare (Cyan)";
        displayNameShort = "Cyan Flare";
        descriptionShort = "Signal Pistol Cyan Flare";
        mass = 1;
        count = 1;
    };
    class Aux501_Weapons_Mags_GL_flare_Purple1: Aux501_Weapons_Mags_GL_flare_Purple3
    {
        displayName = "[501st] 1Rnd Flare (Purple)";
        displayNameShort = "Purple Flare";
        descriptionShort = "Signal Pistol Purple Flare";
        mass = 1;
        count = 1;
    };
};