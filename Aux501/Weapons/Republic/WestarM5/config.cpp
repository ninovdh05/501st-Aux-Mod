class cfgPatches
{
    class Aux501_Patch_WestarM5
    {
        addonRootClass = "Aux501_Patch_Weapons";
        requiredAddons[]=
        {
            "Aux501_Patch_Weapons",
            "A3_Weapons_F"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_Weaps_WestarM4",
            "Aux501_Weaps_WestarM5"
        };
        magazines[] = 
        {
            "Aux501_Weapons_Mags_10mwsp50",
            "Aux501_Weapons_Mags_20mwsp40"
        };
        ammo[] = 
        {
            "Aux501_Weapons_Ammo_10mwsp",
            "Aux501_Weapons_Ammo_20mwsp"
        };
    };
};

class CowsSlot;
class MuzzleSlot;
class PointerSlot;
class UnderBarrelSlot;

class Mode_SemiAuto;

class cfgWeapons
{
    class Aux501_stun_muzzle;
    class arifle_MX_Base_F;
    class Aux501_rifle_base: arifle_MX_Base_F
    {
        class Single;
        class FullAuto;
        class WeaponSlotsInfo;
    };
    class UGL_F;

    class Aux501_Weaps_WestarM4: Aux501_rifle_base
    {
        scope = 2;
        displayName = "[501st] Westar M4";
        baseWeapon = "Aux501_Weaps_WestarM4";
        picture = "\SWLW_clones\smgs\westar_m5\data\ui\WestarM5_ui.paa";
        model = "3AS\3AS_Weapons\WestarM5\3AS_Westar_M5_GL.p3d";
        handAnim[]=
        {
            "OFP2_ManSkeleton",
            "\3AS\3AS_Weapons\WestarM5\Data\Anim\WestarM5_handanim.rtm"
        };
        recoil = "recoil_SMG_03";
        muzzles[]=
        {
            "this",
            "Stun",
            "Aux501_WestarM4_UGL_F"
        };
        magazines[]=
        {
            "Aux501_Weapons_Mags_10mwsp50",
            "Aux501_Weapons_Mags_10mw50"
        };
        class stun: Aux501_stun_muzzle{};
        modes[] = {"FullAuto","Single","close","short","medium","far_optic1"};
        class FullAuto: FullAuto
        {
            class BaseSoundModeType;
            class StandardSound: BaseSoundModeType
            {
                soundSetShot[] = {"3AS_Westar_Shot_SoundSet"};
            };
            class SilencedSound
            {
                begin1[] = {"swlw_rework\sounds\e-series\E5C_shot.wss",1,1,2200};
                closure1[] = {};
                closure2[] = {};
                soundBegin[] = {"begin1",1};
                soundClosure[] = {};
                weaponSoundEffect = "";
            };	
            reloadTime = 0.1;
            minRange = 0;
            minRangeProbab = 1;
            midRange = 10;
            midRangeProbab = 1;
            maxRange = 20;
            maxRangeProbab = 1;
            aiRateOfFire = 0;
            aiRateOfFireDistance = 600;
        };
        class Single: Single
        {
            reloadTime = 0.1;
            class BaseSoundModeType;
            class StandardSound: BaseSoundModeType
            {
                soundSetShot[] = {"3AS_Westar_Shot_SoundSet"};
            };
            class SilencedSound
            {
                begin1[] = {"swlw_rework\sounds\e-series\E5C_shot.wss",1,1,2200};
                closure1[] = {};
                closure2[] = {};
                soundBegin[] = {"begin1",1};
                soundClosure[] = {};
                weaponSoundEffect = "";
            };	
        };
        class close: FullAuto
        {
            burst = 20;
            aiRateOfFire = 0.01;
            aiRateOfFireDistance = 50;
            minRange = 0;
            minRangeProbab = 0.05;
            midRange = 30;
            midRangeProbab = 0.7;
            maxRange = 50;
            maxRangeProbab = 0.04;
            showToPlayer = 0;
        };
        class short: close
        {
            burst = 15;
            aiRateOfFire = 0.2;
            aiRateOfFireDistance = 300;
            minRange = 50;
            minRangeProbab = 0.05;
            midRange = 150;
            midRangeProbab = 0.7;
            maxRange = 300;
            maxRangeProbab = 0.04;
        };
        class medium: close
        {
            burst = 15;
            aiRateOfFire = 0.25;
            aiRateOfFireDistance = 600;
            minRange = 200;
            minRangeProbab = 0.05;
            midRange = 400;
            midRangeProbab = 0.6;
            maxRange = 600;
            maxRangeProbab = 0.1;
        };
        class far_optic1: close
        {
            requiredOpticType = 1;
            showToPlayer = 0;
            burst = 3;
            aiRateOfFire = 9;
            aiRateOfFireDistance = 900;
            minRange = 350;
            minRangeProbab = 0.04;
            midRange = 550;
            midRangeProbab = 0.5;
            maxRange = 700;
            maxRangeProbab = 0.01;
        };
        class Aux501_WestarM4_UGL_F: UGL_F
        {
            displayName = "[501st] Underbarrel Grenade Launcher";
            descriptionShort = "Underbarrel GL Module for Westar";
            useModelOptics = 0;
            useExternalOptic = 0;
            magazines[] = 
            {
                "ACE_HuntIR_M203"
            };
            magazineWell[] = {};
            cameraDir = "OP_look";
            discreteDistance[] = {75,100,150,200,250,300,350,400};
            discreteDistanceCameraPoint[] = {"OP_eye_75","OP_eye_100","OP_eye_150","OP_eye_200","OP_eye_250","OP_eye_300","OP_eye_350","OP_eye_400"};
            discreteDistanceInitIndex = 1;
            reloadAction = "GestureReloadMXUGL";
            reloadMagazineSound[] = {"A3\Sounds_F\arsenal\weapons\Rifles\MX\Mx_UGL_reload",1,1,10};
            class Single: Mode_SemiAuto
            {
                sounds[] = {"StandardSound"};
                class BaseSoundModeType
                {
                    weaponSoundEffect = "";
                    closure1[] = {};
                    closure2[] = {};
                    soundClosure[] = {};
                };
                class StandardSound: BaseSoundModeType
                {
                    weaponSoundEffect = "";
                    begin1[] = {"SWLW_clones\rifles\gl\sounds\gl",1,1,1800};
                    begin2[] = {"SWLW_clones\rifles\gl\sounds\gl",1,1,1800};
                    begin3[] = {"SWLW_clones\rifles\gl\sounds\gl",1,1,1800};
                    soundBegin[] = {"begin1",0.33,"begin2",0.33,"begin3",0.33};
                };
            };
        };
        class WeaponSlotsInfo: WeaponSlotsInfo
        {
            class CowsSlot: CowsSlot
            {
                displayName = "Optics Slot";
                iconPicture = "\A3\Weapons_F\Data\UI\attachment_top.paa";
                iconPinpoint = "Bottom";
                iconPosition[] = {0.5,0.35};
                iconScale = 0.2;
                linkProxy = "\a3\data_f\proxies\weapon_slots\TOP";
                compatibleItems[] = 
                {
                    "Aux501_cows_RCO",
                    "Aux501_cows_RCO_2",
                    "Aux501_cows_RCO_3",
                    "Aux501_cows_Holosight",
                    "Aux501_cows_Holosight_2",
                    "Aux501_cows_Holosight_3",
                    "Aux501_cows_HoloScope",
                    "Aux501_cows_HoloScope_2",
                    "Aux501_cows_HoloScope_3",
                    "Aux501_cows_MRCO",
                    "Aux501_cows_MRCO_2",
                    "Aux501_cows_MRCO_3",
                    "Aux501_cows_DMS",
                    "Aux501_cows_DMS_2",
                    "Aux501_cows_DMS_3",
                    "Aux501_cows_DMS_4",
                    "Aux501_cows_reflex_optic",
                    "3AS_Optic_Scope_WestarM5"
                };
            };
            class MuzzleSlot: MuzzleSlot
            {
                linkProxy="\A3\data_f\proxies\weapon_slots\MUZZLE";
                displayName="$str_a3_cfgweapons_abr_base_f_weaponslotsinfo_muzzleslot0";
                iconPicture="\A3\Weapons_F\Data\UI\attachment_muzzle.paa";
                iconPinpoint="Center";
                compatibleItems[]=
                {
                    "Aux501_muzzle_flash",
                    "Aux501_muzzle_surpressor"
                };
            };
            class PointerSlot: PointerSlot
            {
                compatibleItems[] = {"acc_flashlight","acc_pointer_IR"};
                iconPicture="\A3\Weapons_F\Data\UI\attachment_muzzle.paa";
                iconPinpoint="Center";
                linkProxy = "\A3\data_f\proxies\weapon_slots\SIDE";
                displayName = "Pointer Slot"; 
            };
        };
    };
    class Aux501_Weaps_WestarM5: Aux501_Weaps_WestarM4
    {
        displayName = "[501st] Westar M5";
        baseWeapon = "Aux501_Weaps_WestarM5";
        muzzles[]=
        {
            "this",
            "Stun",
            "Aux501_WestarM5_UGL_F"
        };
        magazines[]=
        {
            "Aux501_Weapons_Mags_20mwsp40",
            "Aux501_Weapons_Mags_20mw40"
        };
        class Single: Single
        {
            reloadTime = 0.12;
            dispersion = 0.00116;
        };
        class FullAuto: FullAuto
        {
            reloadTime = 0.12;
            dispersion = 0.00073;
        };
        class Aux501_WestarM5_UGL_F: Aux501_WestarM4_UGL_F
        {
            magazines[] = 
            {
                "ACE_HuntIR_M203",
                "Aux501_Weapons_Mags_GL_HE1",
                "Aux501_Weapons_Mags_GL_smoke_white6",
                "Aux501_Weapons_Mags_GL_smoke_purple3",
                "Aux501_Weapons_Mags_GL_smoke_yellow3",
                "Aux501_Weapons_Mags_GL_smoke_red3",
                "Aux501_Weapons_Mags_GL_smoke_green3",
                "Aux501_Weapons_Mags_GL_smoke_blue3",
                "Aux501_Weapons_Mags_GL_smoke_orange3",
                "Aux501_Weapons_Mags_GL_flare_White3",
                "Aux501_Weapons_Mags_GL_flare_Green3",
                "Aux501_Weapons_Mags_GL_flare_Red3",
                "Aux501_Weapons_Mags_GL_flare_Yellow3",
                "Aux501_Weapons_Mags_GL_flare_Blue3",
                "Aux501_Weapons_Mags_GL_flare_Cyan3",
                "Aux501_Weapons_Mags_GL_flare_Purple3",
                "Aux501_Weapons_Mags_GL_flare_IR3"
            };
        };
    };
};

class CfgMagazines
{
    class Aux501_Weapons_Mags_10mw50;
    class Aux501_Weapons_Mags_10mwsp50: Aux501_Weapons_Mags_10mw50
    {
        displayName = "[501st] 50Rnd 10MW SP Cell";
        displayNameShort = "50Rnd 10MW SP";
        picture = "\Aux501\Weapons\Magazines\data\Aux501_icon_mag_10mw_sp_ca.paa";
        ammo = "Aux501_Weapons_Ammo_10mwsp";
        descriptionShort="Westar Silenced Plasma magazine";
    };
    class Aux501_Weapons_Mags_20mwsp40: Aux501_Weapons_Mags_10mwsp50
    {
        displayName = "[501st] 40Rnd 20MW SP Cell";
        displayNameShort = "40Rnd 20MW SP";
        picture = "\Aux501\Weapons\Magazines\data\Aux501_icon_mag_20mwsp_ca.paa";
        ammo = "Aux501_Weapons_Ammo_20mwsp";
        descriptionShort="Westar Silenced Plasma magazine";
    };
};

class CfgAmmo
{
    class Aux501_Weapons_Ammo_10mw;
    class Aux501_Weapons_Ammo_20mw;

    class Aux501_Weapons_Ammo_10mwsp: Aux501_Weapons_Ammo_10mw
    {
        visibleFire = 1;
        audibleFire = 5;
        visibleFireTime = 3;
    };
    class Aux501_Weapons_Ammo_20mwsp: Aux501_Weapons_Ammo_20mw
    {
        visibleFire = 1;
        audibleFire = 5;
        visibleFireTime = 3;
    };
};