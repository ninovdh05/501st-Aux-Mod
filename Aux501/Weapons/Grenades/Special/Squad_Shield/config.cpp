class cfgPatches
{
    class Aux501_Patch_grenades_special_squad_shield
    {
        addonRootClass = "Aux501_Patch_Weapons";
        requiredAddons[] =
        {
            "Aux501_Patch_Weapons",
            "A3_Weapons_F",
            "A3_Structures_F_Households_House_Small03"
        };
        units[] = {};
        weapons[] = {};
        magazines[] = 
        {
            "Aux501_Weapons_Mags_Grenades_Squad_Shield"
        };
    };
};

class CfgVehicles
{
    class Land_House_Small_03_V1_ruins_F;

    class Aux501_Weapons_Grenades_Special_Object_Squad_Shield: Land_House_Small_03_V1_ruins_F
    {
        author = "501st Aux Team";
        scope = 2;
        scopeCurator = 2;
        side = 3;
        mapSize = 21.1;
        armor = 999999;
        armorStructural = 999;
        explosionShielding = 999;
        class SimpleObject
        {
            eden = 0;
            animate[] = {};
            hide[] = {};
            verticalOffset = -0.023;
            verticalOffsetWorld = 0;
            init = "''";
        };
        editorPreview = "\A3\EditorPreviews_F\Data\CfgVehicles\Land_House_Small_03_V1_ruins_F.jpg";
        simulation = "house";
        displayName = "Squad Shield";
        model = "\Aux501\Weapons\Grenades\Special\Squad_Shield\Squad_Shield.p3d";
        icon = "iconObject_1x1";
        Aux501_fired_deployable_loopSound = "Aux501_Sound_Grenade_Shield_Loop";
        Aux501_fired_deployable_loopDuration = 14;
        Aux501_fired_deployable_endSound = "Aux501_Sound_Grenade_Shield_end";
        Aux501_fired_deployable_endDuration = 1;
        Aux501_fired_deployable_soundDistance = 300;
        Aux501_fired_deployable_ignoreDamage = 1;
        hiddenSelections[] = {"camo1"};
        hiddenSelectionsTextures[] = {"\Aux501\Weapons\Grenades\Special\data\textures\shield_texture.paa"};
        vehicleClass = "RD501_Vehicle_Class_statics";
        editorCategory = "RD501_Editor_Category_statics";
        editorSubcategory = "RD501_Editor_Category_static_msc";
    };
};

class CfgMagazines
{
    class HandGrenade;
    
    class Aux501_Weapons_Mags_Grenades_Squad_Shield: HandGrenade
    {
        scope = 2;
        author = "501st Aux Team";
        displayName = "[501st] Squad Shield";
        displayNameShort = "Squad Shield";
        ammo = "Aux501_Weapons_Ammo_Grenades_Squad_Shield";
        descriptionShort = "Squad Shield";
        model = "3as\3as_shield\SquadShield_Throwable.p3d";
        picture = "\Aux501\Weapons\Magazines\data\Aux501_icon_mag_imploder_ca.paa";
        mass = 8;
        value = 1;
        type = 256;
        initSpeed = 18;
        nameSound = "handgrenade";
        maxLeadSpeed = 7;
    };
};

class CfgAmmo
{
    class grenade;

    class Aux501_Weapons_Ammo_Grenades_Squad_Shield: grenade
    {
        hit = 0;
        indirectHit = 0;
        indirectHitRange = 0;
        typicalspeed = 18;
        visibleFire = 0.5;
        audibleFire = 0.05;
        visibleFireTime = 1;
        soundFly[] = {"",1.5,1,90};
        fuseDistance = 0;
        model = "3as\3as_shield\SquadShield_Throwable.p3d";
        Aux501_fired_deployable_enabled = 1;
        Aux501_fired_deployable_object = "Aux501_Weapons_Grenades_Special_Object_Squad_Shield";
        Aux501_fired_deployable_timeToLive = 60;
        simulation = "shotShell";
    };
};

class CfgSounds
{
    class Aux501_Sound_Grenade_Shield_Loop
    {
        // how the sound is referred to in the editor (e.g. trigger effects)
        name = "shield_loop";

        // filename, volume, pitch, distance (optional)
        sound[] = { "\Aux501\Weapons\Grenades\Special\data\sounds\shield_loop.wss", 4, 1, 300 };

        // subtitle delay in seconds, subtitle text
        titles[] = { 1, "" };
    };
    class Aux501_Sound_Grenade_Shield_end
    {
        // how the sound is referred to in the editor (e.g. trigger effects)
        name = "shield_end";

        // filename, volume, pitch, distance (optional)
        sound[] = { "\Aux501\Weapons\Grenades\Special\data\sounds\shield_end.wss", 4, 1, 300 };

        // subtitle delay in seconds, subtitle text
        titles[] = { 1, "" };
    };
};