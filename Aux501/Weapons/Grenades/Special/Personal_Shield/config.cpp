class cfgPatches
{
    class Aux501_Patch_grenades_special_personal_shield
    {
        addonRootClass = "Aux501_Patch_Weapons";
        requiredAddons[] =
        {
            "Aux501_Patch_Weapons",
            "A3_Weapons_F",
            "A3_Structures_F_Households_House_Small03"
        };
        units[] = {};
        weapons[] = {};
        magazines[] = 
        {
            "Aux501_Weapons_Mags_Grenades_Shield_Personal"
        };
    };
};

class CfgVehicles
{
    class Land_House_Small_03_V1_ruins_F;

    class Aux501_Weapons_Grenades_Special_Object_Personal_Shield: Land_House_Small_03_V1_ruins_F
    {       
        author = "501st Aux Team";
        scope = 2;
        scopeCurator = 2;
        displayName = "Personal Shield";
        mapSize = 21.1;
        class SimpleObject
        {
            eden = 0;
            animate[] = {};
            hide[] = {};
            verticalOffset = -0.023;
            verticalOffsetWorld = 0;
            init = "''";
        };
        editorPreview = "\A3\EditorPreviews_F\Data\CfgVehicles\Land_House_Small_03_V1_ruins_F.jpg";
        armor = 999999;
        armorStructural = 999;
        sound = "Shield";
        simulation = "Fountain";
        model = "\Aux501\Weapons\Grenades\Special\Personal_Shield\Personal_Shield.p3d";
        icon = "iconObject_1x1";
        //vehicleClass = "RD501_Vehicle_Class_statics";
        //editorCategory = "RD501_Editor_Category_statics";
        //editorSubcategory = "RD501_Editor_Category_static_msc";
    };
};

class CfgMagazines
{
    class Aux501_Weapons_Mags_Grenades_Squad_Shield;
    
    class Aux501_Weapons_Mags_Grenades_Shield_Personal: Aux501_Weapons_Mags_Grenades_Squad_Shield
    {
        displayName = "[501st] Personal Shield";
        displayNameShort = "Personal Shield";
        descriptionShort = "Personal Shield";
        model = "";
        ammo = "Aux501_Weapons_Ammo_Grenades_Personal_Shield";
    };
};

class CfgAmmo
{
    class Aux501_Weapons_Ammo_Grenades_Squad_Shield;

    class Aux501_Weapons_Ammo_Grenades_Personal_Shield: Aux501_Weapons_Ammo_Grenades_Squad_Shield
    {
        Aux501_fired_deployable_object = "Aux501_Weapons_Grenades_Special_Object_Personal_Shield";
        Aux501_fired_deployable_timeToLive = 120;
        model = "";
        Aux501_fired_deployable_attach = 1;
        Aux501_fired_deployable_attach_offset[] = {0.5, 0.7, -0.2};
        Aux501_fired_deployable_attach_bone = "weapon";
        Aux501_fired_deployable_attach_rotate = 1;
    };
};