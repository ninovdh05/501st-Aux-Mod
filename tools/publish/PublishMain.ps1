$publisherCmd = Join-Path (Get-ItemProperty "HKCU:\Software\Bohemia Interactive\Arma 3 Tools").path "Publisher\PublisherCmd.exe"

& $publisherCmd update /id:2173635424 /changenote:"See: https://gitlab.com/501st-legion-starsim/aux-mod-team/501st-Aux-Mod" /path:"$((Resolve-Path "..\..\@501st Community Aux Mod").Path)"
