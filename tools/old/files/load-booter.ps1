# This file calls the Create-Virtual-Instance script. Its
# intent is to make it simpler than trying to write all the
# parameters in the command line.

param(
    [int]$Loaders = 4
)

Function Get-FolderName($initialDirectory) {
    [System.Reflection.Assembly]::LoadWithPartialName("System.windows.forms") | Out-Null
    $OpenFolderDialog = New-Object System.Windows.Forms.FolderBrowserDialog
    $Topmost = New-Object System.Windows.Forms.Form
    $Topmost.TopMost = $True
    $Topmost.MinimizeBox = $True
    $OpenFolderDialog.ShowDialog($Topmost) | Out-Null
    return $OpenFolderDialog.SelectedPath
}

Function Prompt-YesNo($caption, $message) {
    [int]$defaultChoice = 1
    $yes = New-Object System.Management.Automation.Host.ChoiceDescription "&Yes", "I understand, flash over it."
    $no = New-Object System.Management.Automation.Host.ChoiceDescription "&No"
    $options = [System.Management.Automation.Host.ChoiceDescription[]]($yes, $no)
    $choiceRTN = $host.UI.PromptForChoice($caption, $message, $options, $defaultChoice)

    return ($choiceRTN -ne 1)
}

$cacheFolder = Join-Path $PSScriptRoot "..\build_cache"
$cacheFile = Join-Path $cacheFolder "cache.json"
$cache = $null
if (Test-Path $cacheFile) {
    $cache = Get-Content $cacheFile | Out-String | ConvertFrom-Json
}

Write-Output "Looking for ArmA 3"
$arma = $(Join-Path $(Get-ItemProperty 'HKCU:\SOFTWARE\Valve\Steam').SteamPath "/steamapps/common/Arma 3")
if (-Not $(Test-Path $arma) -And ($null -ne $cache)) { $arma = $cache.arma }
if($arma -eq "" -Or -Not $(Test-Path $arma)) {
    Prompt-YesNo("NOTICE", "Please select your ArmA 3 folder in the next menu. Press any button to continue.")

    $arma = Get-FolderName
}

if(-Not $(Test-Path $(Join-Path $arma "arma3.exe"))) {
    Write-Output "Arma not found, exiting."
    Exit 1
}

if(Prompt-YesNo("ArmA 3 Folder", "Confirm your arma 3 folder: $arma")) {
    Write-Output "Found ArmA 3"
} else {
    if (Test-Path $cacheFolder) {
        Remove-Item -Path $cacheFile -ErrorAction SilentlyContinue
    }
    Write-Error "Arma 3 folder not confirmed."
    Exit 1
}

if ($null -ne $cache) {
    $cache.arma = $arma
} else {
    $cache = @"
{
    "arma": "$($arma.Replace("\", "\\"))"
}
"@ | ConvertFrom-Json
}

if (-Not (Test-Path $cacheFolder)) {
    New-Item -Path $cacheFolder -ItemType Directory
}

$cache | ConvertTo-Json | Out-File $cacheFile

Write-Output "Saved to cache $cacheFile"

$local = Join-Path $PSScriptRoot "\..\dummy_mods"

if (-Not (Test-Path $local)) {
    New-Item -Path $local -ItemType Directory
}

$mods = @(
    "@3AS (Beta Test)",
    "@ace",
    "@CBA_A3",
    "@Legion Base - Stable",
    "@Just Like The Simulations - The Great War",
    "@Kobra Mod Pack - Main",
    "@Operation TREBUCHET",
    "@WebKnight Droids",
    "@DBA CIS",
    "@DBA Core",
    "@Black Legion Imperial Assets Mod",
    "@StarForge Armory",
    "@GARC Asset Pack"
)

$route = "http://localhost:9127"
$output = "P:"

$noMods = $False
$extensions = @()
$whitelist = @(
    "config.bin"
)
$Preload = $True
$initRunners = $Loaders
$randomOutput = $False

$params = @{
    "Route"=$route;
    "Arma"=$Arma;
    "Mods" = $Mods;
    "NoMods" = $NoMods;
    "Output" = $output;
    "Local" = $Local;
    "Extensions" = $Extensions;
    "Whitelist" = $Whitelist;
    "Preload" = $Preload;
    "Runners" = $initRunners;
    "RandomOutput" = $randomOutput;
    "Log"=$true;
    "EnvVarName"="VPD_VIRTUAL_INSTANCE_ID";
    "CreateIfNotCreated"=$True;
}

Write-Output $params

Expand-Archive -Force (Resolve-Path "$PSScriptRoot\..\dummy-mods.zip").Path (Resolve-Path "$PSScriptRoot\..\dummy_mods").Path

$createInstancePath = (Resolve-Path "$PSScriptRoot\..\shared\create-instance.ps1").Path
& $createInstancePath @params
