//ARC
	#define NEW_501_ARC_Helm(NAME)\
		class macro_new_helmet(arc,NAME) : macro_new_helmet(arc,base_jlts)\
		{\
			scope = 2;\
			displayName = Clonetrooper helmet (501st '##NAME##');\
			hiddenSelectionsTextures[] = {macro_custom_helmet_textures##\arc\##NAME##.paa};\
		};	

//Lumanated Visor
	#define NEW_501_Lumanated_Visor_Helm(NAME)\
		class macro_new_helmet(infantry_lumanated,NAME) : macro_new_helmet(infantry,lum_base)\
		{\
			scope = 2;\
			displayName = Clonetrooper helmet (501st '##NAME##');\
			hiddenSelectionsTextures[] = {macro_custom_helmet_textures##\infantry\##NAME##.paa};\
		};
		
//Normal Infantry ones JLTS
	#define NEW_501_Inf_Helm_JLTS(NAME)\
		class macro_new_helmet(infantry,NAME) : macro_new_helmet(infantry,jlts_trooper)\
		{\
			scope = 2;\
			displayName = [501st] INF HELM ('##NAME##');\
			hiddenSelectionsTextures[] = {macro_custom_helmet_textures##\infantry\##NAME##.paa};\
		};

//ARC JLTS
	#define NEW_501_ARC_Helm_JLTS(NAME)\
		class macro_new_helmet(arc,NAME) : macro_new_helmet(arc,base_jlts)\
		{\
			scope = 2;\
			displayName = [501st] ARC HELM ('##NAME##');\
			hiddenSelectionsTextures[] = {macro_custom_helmet_textures##\arc\##NAME##.paa};\
		};

//BARC JLTS
	#define NEW_501_BARC_Helm_JLTS(NAME)\
		class macro_new_helmet(arc,NAME) : macro_new_helmet(barc,base_jlts)\
		{\
			scope = 2;\
			displayName = [501st] INF MED HELM 06 ('##NAME##');\
			hiddenSelectionsTextures[] = {macro_custom_helmet_textures##\Medic\##NAME##.paa};\
		};

//Mynock 3AS
	#define NEW_501_MYN_Helm_3AS(NAME)\
		class macro_new_helmet(mynock,NAME) : macro_new_helmet(mynock,3as_base)\
		{\
			scope = 2;\
			displayName = [501st] MYN HELM ('##NAME##');\
			hiddenSelectionsTextures[] = {macro_custom_helmet_textures##\Mynock\##NAME##.paa};\
		};

//Lumanated Visor JLTS
	#define NEW_501_Lumanated_Visor_Helm_JLTS(NAME)\
		class macro_new_helmet(infantry_lumanated,NAME) : macro_new_helmet(infantry,jlts_lum_base)\
		{\
			scope = 2;\
			displayName = [501st] INF HELM ('##NAME##');\
			hiddenSelectionsTextures[] = {macro_custom_helmet_textures##\infantry\##NAME##.paa};\
		};
//LS RTO
	#define NEW_501_RTO_Helm_LS(NAME)\
		class macro_new_helmet(infantry,NAME) : macro_new_helmet(infantry,ls_base)\
		{\
			scope = 2;\
			displayName = [501st] INF RTO HELM ('##NAME##');\
			hiddenSelectionsTextures[] = {macro_custom_helmet_textures##\RTO\##NAME##.paa};\
		};
//3AS helms
	#define NEW_501_Inf_Helm_3AS(NAME)\
		class macro_new_helmet(infantry,NAME) : macro_new_helmet(infantry,3AS_base)\
		{\
			scope = 2;\
			displayName = Clonetrooper helmet (501st '##NAME##');\
			hiddenSelectionsTextures[] = {macro_custom_helmet_textures##\3AS\##NAME##.paa};\
		};
//JLTSv2
	#define NEW_501_Inf_Helm_JLTS2(NAME)\
		class macro_new_helmet(infantry,NAME) : macro_new_helmet(infantry,jlts_recruit)\
		{\
			scope = 2;\
			displayName = [501st] INF MED HELM 03 ('##NAME##');\
			hiddenSelectionsTextures[] = {macro_custom_helmet_textures##\jlts\inf\##NAME##.paa};\
		};
		
////////////////AIRBORNE//////////////////////
#define TEXTURE \RD501_Helmets\_textures\airborne

#define NEW_501_AB_HELM(NAME)\
	class macro_new_helmet(airborne,NAME) : macro_new_helmet(airborne,base)\
	{\
		scope = 2;\
		displayName = Clone airborne helmet ('##NAME##');\
		hiddenSelectionsTextures[] = {TEXTURE##\##NAME##.paa};\
	};
	
#define NEW_501_AB_HELM_JLTS(NAME)\
	class macro_new_helmet(airborne,NAME) : macro_new_helmet(airborne,jlts_base)\
	{\
		scope = 2;\
		displayName = MACRO_QUOTE([501st] AB HELM ('##NAME##'));\
		hiddenSelectionsTextures[] = {TEXTURE##\##NAME##.paa};\
	};
///////////////////AVIATION///////////////////
	#define NEW_501_Pilot_HELM(classname,displayname,texture)\
	class macro_new_helmet(pilot,classname) : macro_new_helmet(pilot,base)\
	{\
		scope = 2;\
		author = "RD501";\
		displayName = [501st] AVI HELM ('##displayname##');\
		hiddenSelectionsTextures[] = {macro_custom_helmet_textures##\aviation\##texture};\
		subItems[] = {"G_B_Diving"};\
	};

#define LS_WARDEN_HELM(NAME)\
	class macro_new_helmet(ls_warden,NAME) : macro_new_helmet(ls_warden,501st)\
	{\
		scope = 2;\
		author = "RD501";\
		displayName = [501st] WRDN HELM ('##NAME##');\
		hiddenSelections[] = {"camo1","illum","visor"};\
		hiddenSelectionsTextures[] = {macro_custom_helmet_textures##\LS_Warden\##NAME,"ls_armor_bluefor\helmet\gar\engineer\data\light_co.paa","ls_armor_bluefor\helmet\gar\engineer\data\visor_co.paa"};\
		subItems[] = {"G_B_Diving"};\
	};

#define AUX501_WARDEN_HELM(NAME)\
	class Aux501_Customs_Warden_Helmet_##NAME: Aux501_Units_Republic_501_Warden_Helmet\
	{\
		displayName = [501st] WRDN HELM ('##NAME##');\
		hiddenSelections[] = {"camo","visor"};\
		hiddenSelectionsTextures[] = {macro_custom_helmet_textures##\aviation\warden\##NAME,"Aux501\Units\Republic\501st\Warden\Helmets\Phase2\data\textures\warden_visor_CO.paa"};\
	};

#define NEW_3AS_Pilot_HELM(name)\
	class macro_new_helmet(3as_pilot,name) : macro_new_helmet(3as_pilot,base_2)\
	{\
		scope = 2;\
		author = "RD501";\
		displayName = [501st] AVI HELM ('##name##');\
		hiddenSelectionsTextures[] = {macro_custom_helmet_textures##\aviation\##name,"","3AS\3AS_Characters\Clones\Headgear\Textures\PilotP2\Phase_2_Pilot_Tubes_co.paa","",macro_custom_helmet_textures##\aviation\box\##name};\
		subItems[] = {"G_B_Diving"};\
		hiddenSelections[] = {"Camo","Camo2","Camo3","Camo4","Camo5"};\
	};