class CfgPatches
{
	class rd501_patch_name_magna_units
	{
		requiredAddons[]=
		{
			RD501_patch_units
		};
		requiredVersion=0.1;
		units[]=
		{
			"RD501_opfor_unit_magnaguard_droid_standard"
			
		};
		weapons[]=
		{
			
		};
	};
};
class CfgVehicles
{
    class lsd_cis_bxdroid_specops;
	class RD501_opfor_unit_magnaguard_droid_base: lsd_cis_bxdroid_specops
    {
		scope = 0;
		scopeCurator = 0;
		displayName = "IG-100 MagnaGuard";
		faction="RD501_CIS_Faction";
		editorSubcategory="RD501_Editor_Category_CIS_SpecOps";
		vehicleClass ="RD501_Editor_Category_CIS_SpecOps";
		genericNames = "ls_droid_b1";
		canBleed = 0;
		impactEffectsBlood = "ImpactMetal";
		impactEffectsNoBlood = "ImpactPlastic";
		items[] = {};
		respawnItems[] = {};
		class HitPoints
		{
			class HitFace
			{
				armor = 1;
				material = -1;
				name = "face_hub";
				passThrough = 0.1;
				radius = 0.08;
				explosionShielding = 0.1;
				minimalHit = 0.01;
			};
			class HitNeck: HitFace
			{
				armor = 5;
				material = -1;
				name = "neck";
				passThrough = 0.1;
				radius = 0.1;
				explosionShielding = 0.5;
				minimalHit = 0.01;
			};
			class HitHead: HitNeck
			{
				armor = 5;
				material = -1;
				name = "head";
				passThrough = 0.1;
				radius = 0.2;
				explosionShielding = 0.5;
				minimalHit = 0.01;
				depends = "HitFace max HitNeck";
			};
			class HitPelvis: HitHead
			{
				armor = 15;
				material = -1;
				name = "pelvis";
				passThrough = 0.1;
				radius = 0.24;
				explosionShielding = 3;
				visual = "injury_body";
				minimalHit = 0.01;
				depends = "";
			};
			class HitAbdomen: HitPelvis
			{
				armor = 15;
				material = -1;
				name = "spine1";
				passThrough = 0.1;
				radius = 0.16;
				explosionShielding = 3;
				visual = "injury_body";
				minimalHit = 0.01;
			};
			class HitDiaphragm: HitAbdomen
			{
				armor = 15;
				material = -1;
				name = "spine2";
				passThrough = 1;
				radius = 0.18;
				explosionShielding = 6;
				visual = "injury_body";
				minimalHit = 0.01;
			};
			class HitChest: HitDiaphragm
			{
				armor = 15;
				material = -1;
				name = "spine3";
				passThrough = 0.1;
				radius = 0.18;
				explosionShielding = 6;
				visual = "injury_body";
				minimalHit = 0.01;
			};
			class HitBody: HitChest
			{
				armor = 1000;
				material = -1;
				name = "body";
				passThrough = 0.1;
				radius = 0;
				explosionShielding = 6;
				visual = "injury_body";
				minimalHit = 0.01;
				depends = "HitPelvis max HitAbdomen max HitDiaphragm max HitChest";
			};
			class HitArms: HitBody
			{
				armor = 15;
				material = -1;
				name = "arms";
				passThrough = 0.5;
				radius = 0.1;
				explosionShielding = 3;
				visual = "injury_hands";
				minimalHit = 0.01;
				depends = "0";
			};
			class HitHands: HitArms
			{
				armor = 15;
				material = -1;
				name = "hands";
				passThrough = 0.5;
				radius = 0.1;
				explosionShielding = 1;
				visual = "injury_hands";
				minimalHit = 0.01;
				depends = "HitArms";
			};
			class HitLegs: HitHands
			{
				armor = 15;
				material = -1;
				name = "legs";
				passThrough = 0.5;
				radius = 0.14;
				explosionShielding = 3;
				visual = "injury_legs";
				minimalHit = 0.01;
				depends = "0";
			};
			class Incapacitated: HitLegs
			{
				armor = 1000;
				material = -1;
				name = "body";
				passThrough = 1;
				radius = 0;
				explosionShielding = 3;
				visual = "";
				minimalHit = 0;
				depends = "(((Total - 0.25) max 0) + ((HitHead - 0.25) max 0) + ((HitBody - 0.25) max 0)) * 2";
			};
			class HitLeftArm
			{
				armor = 6;
				material = -1;
				name = "hand_l";
				passThrough = 0.1;
				radius = 0.08;
				explosionShielding = 3;
				visual = "injury_hands";
				minimalHit = 0.01;
			};
			class HitRightArm: HitLeftArm
			{
				name = "hand_r";
			};
			class HitLeftLeg
			{
				armor = 6;
				material = -1;
				name = "leg_l";
				passThrough = 0.1;
				radius = 0.1;
				explosionShielding = 3;
				visual = "injury_legs";
				minimalHit = 0.01;
			};
			class HitRightLeg: HitLeftLeg
			{
				name = "leg_r";
			};
		};
		armor = 50;
		armorStructural = 2;
		explosionShielding = 0.4;
		minTotalDamageThreshold = 0.001;
		impactDamageMultiplier = 0.5;
		model = "";
		hiddenSelections[] = {"camo1"};
		hiddenSelectionsTextures[]=
		{
            "RD501_Droids\data\Magnaguard\standard.paa"
		};
		class SoundEnvironExt
		{		
			generic[] = {
             {"walk", {"\WebKnightsRobotics\sounds\b2_step_1.ogg", 2, 1, 30}}, 
             {"walk", {"\WebKnightsRobotics\sounds\b2_step_2.ogg", 2, 1, 30}},
             {"walk", {"\WebKnightsRobotics\sounds\b2_step_3.ogg", 2, 1, 30}},
			 {"run", {"\WebKnightsRobotics\sounds\b2_step_1.ogg", 2, 1, 30}}, 
             {"run", {"\WebKnightsRobotics\sounds\b2_step_2.ogg", 2, 1, 30}},
             {"run", {"\WebKnightsRobotics\sounds\b2_step_3.ogg", 2, 1, 30}},
			 {"sprint", {"\WebKnightsRobotics\sounds\b2_step_1.ogg", 2, 1, 30}}, 
             {"sprint", {"\WebKnightsRobotics\sounds\b2_step_2.ogg", 2, 1, 30}},
             {"sprint", {"\WebKnightsRobotics\sounds\b2_step_3.ogg", 2, 1, 30}}
			};
		};
		class SoundEquipment 
		{	
		    soldier[] = {
             {"walk", {"\WebKnightsRobotics\sounds\b2_step_1.ogg", 2, 1, 30}}, 
             {"walk", {"\WebKnightsRobotics\sounds\b2_step_2.ogg", 2, 1, 30}},
             {"walk", {"\WebKnightsRobotics\sounds\b2_step_3.ogg", 2, 1, 30}},
			 {"run", {"\WebKnightsRobotics\sounds\b2_step_1.ogg", 2, 1, 30}}, 
             {"run", {"\WebKnightsRobotics\sounds\b2_step_2.ogg", 2, 1, 30}},
             {"run", {"\WebKnightsRobotics\sounds\b2_step_3.ogg", 2, 1, 30}},
			 {"sprint", {"\WebKnightsRobotics\sounds\b2_step_1.ogg", 2, 1, 30}}, 
             {"sprint", {"\WebKnightsRobotics\sounds\b2_step_2.ogg", 2, 1, 30}},
             {"sprint", {"\WebKnightsRobotics\sounds\b2_step_3.ogg", 2, 1, 30}}
            };
		};
		class SoundBreath
		{
			breath[] = {};
		};
		class SoundDrown
		{
			breath[] = {};
		};
		class SoundInjured
		{
			breath[] = {};
		};
		class SoundBleeding
		{
			breath[] = {};
		};
		class SoundBurning
		{
			breath[] = {};
		};
		class SoundChoke
		{
			breath[] = {};
		};
		class SoundRecovered
		{
			breath[] = {};
		};
	};
	class RD501_opfor_unit_magnaguard_droid_standard : RD501_opfor_unit_magnaguard_droid_base
	{
		scope = 0;
		scopeCurator = 0;
        JLTS_isDroid = 1; 
		JLTS_hasEMPProtection = 0; 
		displayName = "IG-100 MagnaGuard";
		uniformClass = "RD501_opfor_uniform_IG100_armor";
		genericNames = "ls_droid_bx";
		backpack = "";
		identityTypes[] = {"lsd_voice_b1Droid"};
		linkedItems[]=
		{
			"ItemMap",
			"ItemCompass",
			"ItemWatch",
			"JLTS_droid_comlink",
			"JLTS_NVG_droid_chip_1"
		};
		respawnLinkedItems[] = 
		{
			"ItemMap",
			"ItemCompass",
			"ItemWatch",
			"JLTS_droid_comlink",
			"JLTS_NVG_droid_chip_1"
		};
		weapons[] = 
        {
            "Aux501_Weaps_E5_Special",
            "Throw",
            "Put"
        };
		respawnWeapons[] = 
        {
            "Aux501_Weaps_E5_Special",
            "Throw",
            "Put"
        };
		magazines[] = 
        {
            "Aux501_Weapons_Mags_E5100",
			"Aux501_Weapons_Mags_E5100",
			"Aux501_Weapons_Mags_E5100",
			"Aux501_Weapons_Mags_E5100",
			"Aux501_Weapons_Mags_E5100",
			"Aux501_Weapons_Mags_E5100",
			"Aux501_Weapons_Mags_E5100",
			"Aux501_Weapons_Mags_CISDetonator",
			"Aux501_Weapons_Mags_CISDetonator",
			"RD501_dioxis_x1_mag"
        };
		respawnMagazines[] = {
            "Aux501_Weapons_Mags_E5100",
			"Aux501_Weapons_Mags_E5100",
			"Aux501_Weapons_Mags_E5100",
			"Aux501_Weapons_Mags_E5100",
			"Aux501_Weapons_Mags_E5100",
			"Aux501_Weapons_Mags_E5100",
			"Aux501_Weapons_Mags_E5100",
			"Aux501_Weapons_Mags_CISDetonator",
			"Aux501_Weapons_Mags_CISDetonator",
			"RD501_dioxis_x1_mag"
        };
	};
};
class CfgWeapons
{
	//Uniform
	class U_I_CombatUniform;
	class UniformItem;
	class RD501_opfor_uniform_IG100_armor : U_I_CombatUniform
	{
		scope = 0;
		scopeCurator = 0;
		displayName = "[CIS] IG-100 Chassis";
		picture = "\RD501_Droids\Magna\ig100_armor_ui.paa";
		model = "";
		hiddenSelections[] = {"camo1"};
		hiddenSelectionsTextures[]=
		{
            "RD501_Droids\data\Magnaguard\standard.paa"
		};
		armor = 15;
		class ItemInfo:UniformItem
		{
			uniformModel = "";
			uniformClass = "RD501_opfor_unit_magnaguard_droid_standard";
			containerClass = "Supply150";
			mass = 40;
		};
	};
};


	
