#include "../../../RD501_Main/config_macros.hpp"

class CfgPatches
{
    class RD501_patch_opfor_human
    {
        addonRootClass=MACRO_QUOTE(RD501_patch_units);
        requiredAddons[] = {
            MACRO_QUOTE(RD501_patch_units)
        };
        requiredVersion = 0.1;
        units[] = 
		{
			"RD501_opfor_unit_human_heavy",
			"RD501_opfor_unit_human_rifleman",
			"RD501_opfor_unit_human_marksman",
			"RD501_opfor_unit_human_shotgun",
			"RD501_opfor_unit_human_AT"
		};
    };
};

class CfgVehicles
{
	class B_T_Recon_TL_F;
	class RD501_opfor_unit_human_heavy: B_T_Recon_TL_F
	{
		displayName = "Loyalist Heavy";
		author = "RD501";
		scope = 2;
		side = 0;
		backpack = "JLTS_Clone_belt_bag";
		faction = "RD501_CIS_Faction";
		editorSubcategory = "RD501_Editor_Category_CIS_Humans";
		cost = 4;
		weapons[] = {"Aux501_Weaps_E5C","Throw","Put"};
		magazines[] = 
		{
			"Aux501_Weapons_Mags_E5C150",
			"Aux501_Weapons_Mags_E5C150",
			"Aux501_Weapons_Mags_E5C150",
			"Aux501_Weapons_Mags_E5C150",
			"Aux501_Weapons_Mags_CISDetonator",
			"Aux501_Weapons_Mags_CISDetonator"
		};
		respawnWeapons[] = {"Aux501_Weaps_E5C","Throw","Put"};
		respawnMagazines[] = 
		{
			"Aux501_Weapons_Mags_E5C150",
			"Aux501_Weapons_Mags_E5C150",
			"Aux501_Weapons_Mags_E5C150",
			"Aux501_Weapons_Mags_E5C150",
			"Aux501_Weapons_Mags_CISDetonator",
			"Aux501_Weapons_Mags_CISDetonator"
		};
		linkeditems[] = 
		{
			"OPTRE_UNSC_M52A_Armor_Sniper_WDL",
			"k_Scout_woodland_Helmet",
			"442_clone_arf_glasses",
			"ItemGPS",
			"ItemMap",
			"ItemCompass",
			"ItemWatch",
			"JLTS_droid_comlink"
		};
		respawnlinkeditems[] = 
		{
			"OPTRE_UNSC_M52A_Armor_Sniper_WDL",
			"k_Scout_woodland_Helmet",
			"442_clone_arf_glasses",
			"ItemGPS",
			"ItemMap",
			"ItemCompass",
			"ItemWatch",
			"JLTS_droid_comlink"
		};
		items[] = {"ACE_elasticBandage","ACE_elasticBandage","ACE_packingBandage","ACE_packingBandage","ACE_tourniquet","ACE_splint","RD501_Painkiller"};
		respawnItems[] = {"ACE_elasticBandage","ACE_elasticBandage","ACE_packingBandage","ACE_packingBandage","ACE_tourniquet","ACE_splint","RD501_Painkiller"};
	};
	class RD501_opfor_unit_human_rifleman: RD501_opfor_unit_human_heavy
	{
		displayName = "Loyalist Rifleman";
		weapons[] = {"Aux501_Weaps_E5","Throw","Put"};
		magazines[] = 
		{
			"Aux501_Weapons_Mags_E5100",
			"Aux501_Weapons_Mags_E5100",
			"Aux501_Weapons_Mags_E5100",
			"Aux501_Weapons_Mags_E5100",
			"Aux501_Weapons_Mags_CISDetonator",
			"Aux501_Weapons_Mags_CISDetonator"
		};
		respawnWeapons[] = {"Aux501_Weaps_E5","Throw","Put"};
		respawnMagazines[] = 
		{
			"Aux501_Weapons_Mags_E5100",
			"Aux501_Weapons_Mags_E5100",
			"Aux501_Weapons_Mags_E5100",
			"Aux501_Weapons_Mags_E5100",
			"Aux501_Weapons_Mags_CISDetonator",
			"Aux501_Weapons_Mags_CISDetonator"
		};
	};
	class RD501_opfor_unit_human_marksman: RD501_opfor_unit_human_heavy
	{
		displayName = "Loyalist Marksman";
		weapons[]=
		{
			"Aux501_Weaps_E5S",
			"Throw",
			"Put"
		};
		respawnWeapons[]=
		{
			"Aux501_Weaps_E5S",
			"Throw",
			"Put"
		};
		magazines[]=
		{
			"Aux501_Weapons_Mags_E5S15",
			"Aux501_Weapons_Mags_E5S15",
			"Aux501_Weapons_Mags_E5S15",
			"Aux501_Weapons_Mags_E5S15",
			"Aux501_Weapons_Mags_E5S15",
			"Aux501_Weapons_Mags_E5S15",
			"Aux501_Weapons_Mags_E5S15",
			"Aux501_Weapons_Mags_E5S15",
			"Aux501_Weapons_Mags_E5S15",
			"Aux501_Weapons_Mags_E5S15",
			"Aux501_Weapons_Mags_E5S15",
			"Aux501_Weapons_Mags_CISDetonator",
			"Aux501_Weapons_Mags_CISDetonator"
		};
		respawnMagazines[]=
		{
			"Aux501_Weapons_Mags_E5S15",
			"Aux501_Weapons_Mags_E5S15",
			"Aux501_Weapons_Mags_E5S15",
			"Aux501_Weapons_Mags_E5S15",
			"Aux501_Weapons_Mags_E5S15",
			"Aux501_Weapons_Mags_E5S15",
			"Aux501_Weapons_Mags_E5S15",
			"Aux501_Weapons_Mags_E5S15",
			"Aux501_Weapons_Mags_E5S15",
			"Aux501_Weapons_Mags_E5S15",
			"Aux501_Weapons_Mags_E5S15",
			"Aux501_Weapons_Mags_CISDetonator",
			"Aux501_Weapons_Mags_CISDetonator"
		};
	};
	class RD501_opfor_unit_human_shotgun: RD501_opfor_unit_human_heavy
	{
		displayName = "Loyalist Breacher";
		weapons[] = {"Aux501_Weaps_SBB3","Throw","Put"};
		magazines[] = 
		{
			"Aux501_Weapons_Mags_SBB3_shotgun25",
			"Aux501_Weapons_Mags_SBB3_shotgun25",
			"Aux501_Weapons_Mags_SBB3_shotgun25",
			"Aux501_Weapons_Mags_SBB3_shotgun25",
			"Aux501_Weapons_Mags_CISDetonator",
			"Aux501_Weapons_Mags_CISDetonator"
		};
		respawnWeapons[] = {"Aux501_Weaps_SBB3","Throw","Put"};
		respawnMagazines[] = 
		{
			"Aux501_Weapons_Mags_SBB3_shotgun25",
			"Aux501_Weapons_Mags_SBB3_shotgun25",
			"Aux501_Weapons_Mags_SBB3_shotgun25",
			"Aux501_Weapons_Mags_SBB3_shotgun25",
			"Aux501_Weapons_Mags_CISDetonator",
			"Aux501_Weapons_Mags_CISDetonator"
		};
	};
	class RD501_opfor_unit_human_AT: RD501_opfor_unit_human_heavy
	{
		displayName = "Loyalist AT Rifleman";
		weapons[] = {"Aux501_Weaps_E5","Aux501_Weaps_e60r_at","Throw","Put"};
		magazines[] = 
		{
			"Aux501_Weapons_Mags_E5100",
			"Aux501_Weapons_Mags_E5100",
			"Aux501_Weapons_Mags_E5100",
			"Aux501_Weapons_Mags_E5100",
			"Aux501_Weapons_Mags_e60r_at",
			"Aux501_Weapons_Mags_e60r_at",
			"Aux501_Weapons_Mags_e60r_at",
			"Aux501_Weapons_Mags_CISDetonator",
			"Aux501_Weapons_Mags_CISDetonator"
		};
		respawnWeapons[] = {"Aux501_Weaps_E5","Aux501_Weaps_e60r_at","Throw","Put"};
		respawnMagazines[] = 
		{
			"Aux501_Weapons_Mags_E5100",
			"Aux501_Weapons_Mags_E5100",
			"Aux501_Weapons_Mags_E5100",
			"Aux501_Weapons_Mags_E5100",
			"Aux501_Weapons_Mags_e60r_at",
			"Aux501_Weapons_Mags_e60r_at",
			"Aux501_Weapons_Mags_e60r_at",
			"Aux501_Weapons_Mags_CISDetonator",
			"Aux501_Weapons_Mags_CISDetonator"
		};
	};
};