#define macro_isJumppack RD501_jumppack_is_jumppack
#define macro_spamDelay  RD501_jumppack_spam_delay
#define macro_energyCap RD501_jumppack_energy_capacity
#define macro_rechargeRate RD501_jumppack_recharge
#define macro_effectScript RD501_jumppack_jump_effect_script
#define macro_effectPoints RD501_jumppack_effect_points[]

#define macro_igniteSound RD501_jumppack_sound_ignite[]
#define macro_landSound RD501_jumppack_sound_land[]
#define macro_idleSound RD501_jumppack_sound_idle[]

#define macro_textureCatagory RD501_jumppack_skin_group[]

#define macro_types_of_jumps RD501_jumppack_jump_types[]//update script

#define macro_default_texture_group "default"

#define macro_jumppackClass(group,name) RD501_jumppack_##group##_##name




