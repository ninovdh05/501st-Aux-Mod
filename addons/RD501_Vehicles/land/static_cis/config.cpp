//get generlized macros
#include "../../../RD501_main/config_macros.hpp"


class CfgPatches
{
	class RD501_patch_static_cis
	{
		addonRootClass=MACRO_QUOTE(RD501_patch_vehicles);

		requiredAddons[]=
		{
			RD501_patch_vehicles
		};
		requiredVersion=0.1;
		units[]=
		{
			macro_new_vehicle(field_cannon,cis),
			macro_new_vehicle(field_repeater,cis),
			macro_new_vehicle(mortar,cis)
		};
		weapons[]=
		{
			
		};
	};
};


class CfgVehicles
{
	class Land;
	class LandVehicle: Land
	{
		class ViewPilot;
		class ViewGunner;
		class NewTurret;
	};
	class StaticWeapon: LandVehicle
	{
		class Turrets
		{
			class MainTurret;
		};
		class AnimationSources;
	};
	class StaticMGWeapon: StaticWeapon
	{
		class Turrets: Turrets
		{
			class MainTurret: MainTurret{};
		};
	};
	class 3AS_FieldCannon_Base: StaticMGWeapon
	{
		class Turrets: Turrets
		{
			class MainTurret: MainTurret{};
		};
	};
	class macro_new_vehicle(field_cannon,cis):3AS_FieldCannon_Base
	{
		author = "RD501";
		scope = 2;
		side = 0;
		displayname = "Proton Cannon";
		scopeCurator = 2;
		crew=MACRO_QUOTE(macro_new_unit_class(opfor,B1));
		faction = MACRO_QUOTE(macro_faction(CIS));
		editorSubcategory = MACRO_QUOTE(macro_editor_cat(turrets));
		vehicleClass = MACRO_QUOTE(macro_editor_vehicle_type(turrets));
		ace_dragging_canCarry = 0;
		ace_dragging_canDrag = 0;
		class Turrets: Turrets
		{
			class MainTurret: MainTurret
			{
				weapons[] = 
				{
					"RD501_proton_cannon_weapon"
				};
				magazines[] = 
				{
					"RD501_CIS_Proton_Mag_ap_12",
					"RD501_CIS_Proton_Mag_ap_12",
					"RD501_CIS_Proton_Mag_ap_12",
					"RD501_CIS_Proton_Mag_ap_12"
				};
			};
		};
		destrType = "DestructBuilding";
		explosionEffect="FuelExplosion";
		class DestructionEffects
		{
			class Dust
			{
				intensity = 0.1;
				interval = 1;
				lifeTime = 0.01;
				position = "destructionEffect2";
				simulation = "particles";
				type = "HousePartDust";
			};
			class Light1
			{
				enabled = "distToWater";
				intensity = 0.1;
				interval = 1;
				lifeTime = 3;
				position = "destructionEffect";
				simulation = "light";
				type = "ObjectDestructionLightSmall";
			};
			class Fire1
			{
				intensity = 0.15;
				interval = 1;
				lifeTime = 3;
				position = "destructionEffect";
				simulation = "particles";
				type = "ObjectDestructionFire1Small";
			};
			class Refract1
			{
				intensity = 1;
				interval = 1;
				lifeTime = 3;
				position = "destructionEffect";
				simulation = "particles";
				type = "SmallFireFRefract";
			};
			class Sound
			{
				intensity = 1;
				interval = 1;
				lifeTime = 1;
				position = "destructionEffect";
				simulation = "sound";
				type = "Fire";
			};
			class sparks1
			{
				intensity = 0.5;
				interval = 1;
				lifeTime = 0;
				position = "destructionEffect2";
				simulation = "particles";
				type = "ObjectDestructionSparks";
			};
			class Smoke1
			{
				simulation="particles";
				type="BarelDestructionSmoke";
				position[]={0,0,0};
				intensity=0.2;
				interval=1;
				lifeTime=1;
			};
			class HouseDestr
			{
				intensity=1;
				interval=1;
				lifeTime=5;
				position="";
				simulation="destroy";
				type="DelayedDestruction";
			};
		};
	};
	class 3as_ParticleCannon_Base: StaticMGWeapon
	{
		class Turrets: Turrets
		{
			class MainTurret: MainTurret
			{
			};
		};
	};
	class macro_new_vehicle(field_repeater,cis):3as_ParticleCannon_Base
	{
		author = "RD501";
		scope = 2;
		side = 0;
		displayname = "Field Repeater";
		scopeCurator = 2;
		crew = MACRO_QUOTE(macro_new_unit_class(opfor,B1));
		faction = MACRO_QUOTE(macro_faction(CIS));
		editorSubcategory = MACRO_QUOTE(macro_editor_cat(turrets));
		vehicleClass = MACRO_QUOTE(macro_editor_vehicle_type(turrets));
		class Turrets: Turrets
		{
			class MainTurret: MainTurret
			{
				weapons[]=
				{
					"RD501_repeater_weapon"
				};
				magazines[]=
				{
					"RD501_CIS_Repeater_Mag_24",
					"RD501_CIS_Repeater_Mag_24",
					"RD501_CIS_Repeater_Mag_24"
				};
			};
		};
		destrType = "DestructBuilding";
		explosionEffect="FuelExplosion";
		class DestructionEffects
		{
			class Dust
			{
				intensity = 0.1;
				interval = 1;
				lifeTime = 0.01;
				position = "destructionEffect2";
				simulation = "particles";
				type = "HousePartDust";
			};
			class Light1
			{
				enabled = "distToWater";
				intensity = 0.1;
				interval = 1;
				lifeTime = 3;
				position = "destructionEffect";
				simulation = "light";
				type = "ObjectDestructionLightSmall";
			};
			class Fire1
			{
				intensity = 0.15;
				interval = 1;
				lifeTime = 3;
				position = "destructionEffect";
				simulation = "particles";
				type = "ObjectDestructionFire1Small";
			};
			class Refract1
			{
				intensity = 1;
				interval = 1;
				lifeTime = 3;
				position = "destructionEffect";
				simulation = "particles";
				type = "SmallFireFRefract";
			};
			class Sound
			{
				intensity = 1;
				interval = 1;
				lifeTime = 1;
				position = "destructionEffect";
				simulation = "sound";
				type = "Fire";
			};
			class sparks1
			{
				intensity = 0.5;
				interval = 1;
				lifeTime = 0;
				position = "destructionEffect2";
				simulation = "particles";
				type = "ObjectDestructionSparks";
			};
			class Smoke1
			{
				simulation="particles";
				type="BarelDestructionSmoke";
				position[]={0,0,0};
				intensity=0.2;
				interval=1;
				lifeTime=1;
			};
			class HouseDestr
			{
				intensity=1;
				interval=1;
				lifeTime=5;
				position="";
				simulation="destroy";
				type="DelayedDestruction";
			};
		};
	};
	/*class StaticMortar: StaticWeapon
	{
		class Turrets: Turrets
		{
			class MainTurret
			{};
		};
	};
	class Mortar_01_base_F: StaticMortar
	{
		class Turrets: Turrets
		{
			class MainTurret: MainTurret
			{};
		};
		class assembleInfo;
	};
	class B_Mortar_01_F: Mortar_01_base_F
	{
		class Turrets: Turrets
		{
			class MainTurret: MainTurret
			{};
		};
	};
	class 3AS_Republic_Mortar: B_Mortar_01_F
	{
		class Turrets: Turrets
		{
			class MainTurret: MainTurret
			{};
		};
	};*/
	class 3AS_CIS_Mortar;
	class macro_new_vehicle(mortar,cis):3AS_CIS_Mortar
	{
		author = "RD501";
		scope = 2;
		side = 0;
		displayname = "CIS Field Mortar";
		scopeCurator = 2;
		crew = MACRO_QUOTE(macro_new_unit_class(opfor,B1_crew));
		faction = MACRO_QUOTE(macro_faction(CIS));
		editorSubcategory = MACRO_QUOTE(macro_editor_cat(arty));
		vehicleClass = MACRO_QUOTE(macro_editor_vehicle_type(arty));
	};

	//EWEB

	class OPTRE_Static_M247H_Shielded_Tripod:StaticMGWeapon
	{
		class Turrets: Turrets
		{
			class MainTurret: MainTurret
			{
			};
		};
	};
	class macro_new_vehicle(eweb_base,cis): OPTRE_Static_M247H_Shielded_Tripod
	{
		_generalMacro = "HMG_01_base_F";
		scope = 0;
		displayName = "EWEB Heavy Turret";
		armor = 300;
		icon = "3AS\3as_static\HeavyRepeater\Data\ui\HeavyRepeater_top_ca.paa";
		hiddenSelectionsTextures[] = 
		{
			"\OPTRE_Weapons\static\M247H\data\m247_co.paa",
			"\OPTRE_Weapons\static\M247H\data\heavyattach_co.paa",
			"\OPTRE_Weapons\static\M247H\data\Mag_co.paa",
			"\OPTRE_Weapons\static\M247H\data\Mount_co.paa",
			"\OPTRE_Weapons\static\M247H\data\Shield_CO.paa",
			"\OPTRE_Weapons\static\M247H\data\TurretLegs_co.paa",
			""
		};
		editorSubcategory = MACRO_QUOTE(macro_editor_cat(turrets));
		ace_dragging_canCarry = 0;
		ace_dragging_canDrag = 0;
		class Turrets: Turrets
		{
			class MainTurret: MainTurret
			{
				memoryPointGunnerOptics = "gunnerview";
				memoryPointsGetInGunner = "Pos Gunner";
				memoryPointsGetInGunnerDir = "Pos Gunner dir";
				minElev = -20;
				maxElev = 15;
				minTurn = -360;
				maxTurn = 360;
				weapons[] = 
				{
					"RD501_eweb_blaster"
				};
				magazines[] = 
				{
					"Aux501_Weapons_Mags_CIS_eweb",
					"Aux501_Weapons_Mags_CIS_eweb",
					"Aux501_Weapons_Mags_CIS_eweb",
					"Aux501_Weapons_Mags_CIS_eweb",
					"Aux501_Weapons_Mags_CIS_eweb"
				};
				gunnerOpticsModel = "\A3\weapons_f\reticle\Optics_Gunner_AAA_01_w_F";
				gunnerAction = "OPTRE_Gunner_M247H";
				gunnergetInAction = "";
				gunnergetOutAction = "";
				displayName = "";
				ejectDeadGunner = 1;
				gunnerForceOptics = 0;
				gunnerOutOpticsShowCursor = 1;
				gunnerOpticsShowCursor = 1;
				class OpticsIn
				{
					class Wide
					{
						opticsDisplayName = "W";
						initAngleX = 0;
						minAngleX = -15;
						maxAngleX = 20;
						initAngleY = 0;
						minAngleY = -100;
						maxAngleY = 100;
						initFov = 0.466;
						minFov = 0.466;
						maxFov = 0.466;
						cameraDir = "";
						visionMode[] = {"Normal","NVG","Ti"};
						thermalMode[] = {0,1};
						gunnerOpticsModel = "\A3\weapons_f\reticle\Optics_Gunner_AAA_01_w_F";
					};
					class Medium: Wide
					{
						opticsDisplayName = "M";
						initFov = 0.093;
						minFov = 0.093;
						maxFov = 0.093;
						gunnerOpticsModel = "\A3\weapons_f\reticle\Optics_Gunner_AAA_01_m_F";
					};
					class Narrow: Wide
					{
						opticsDisplayName = "N";
						gunnerOpticsModel = "\A3\weapons_f\reticle\Optics_Gunner_AAA_01_n_F";
						initFov = 0.029;
						minFov = 0.029;
						maxFov = 0.029;
					};
				};
			};
		};
		class AnimationSources: AnimationSources
		{
			class muzzle_source
			{
				source = "reload";
				weapon = "";
			};
			class muzzle_source_rot
			{
				source = "ammorandom";
				weapon = "RD501_eweb_blaster";
			};
			class ReloadAnim
			{
				source = "reload";
				weapon = "RD501_eweb_blaster";
			};
			class ReloadMagazine
			{
				source = "reloadmagazine";
				weapon = "RD501_eweb_blaster";
			};
			class Revolving
			{
				source = "revolving";
				weapon = "RD501_eweb_blaster";
			};
		};
	};
	class macro_new_vehicle(eweb,cis): macro_new_vehicle(eweb_base,cis)
	{
		editorPreview = "3as\3as_static\images\3AS_StationaryTurret.jpg";
		_generalMacro = "B_HMG_01_F";
		scope = 2;
		side = 0;
		faction = MACRO_QUOTE(macro_faction(CIS));
		crew = MACRO_QUOTE(macro_new_unit_class(opfor,B1));
		destrType = "DestructBuilding";
		explosionEffect="FuelExplosion";
		class DestructionEffects
		{
			class Dust
			{
				intensity = 0.1;
				interval = 1;
				lifeTime = 0.01;
				position = "destructionEffect2";
				simulation = "particles";
				type = "HousePartDust";
			};
			class Light1
			{
				enabled = "distToWater";
				intensity = 0.1;
				interval = 1;
				lifeTime = 3;
				position = "destructionEffect";
				simulation = "light";
				type = "ObjectDestructionLightSmall";
			};
			class Fire1
			{
				intensity = 0.15;
				interval = 1;
				lifeTime = 3;
				position = "destructionEffect";
				simulation = "particles";
				type = "ObjectDestructionFire1Small";
			};
			class Refract1
			{
				intensity = 1;
				interval = 1;
				lifeTime = 3;
				position = "destructionEffect";
				simulation = "particles";
				type = "SmallFireFRefract";
			};
			class Sound
			{
				intensity = 1;
				interval = 1;
				lifeTime = 1;
				position = "destructionEffect";
				simulation = "sound";
				type = "Fire";
			};
			class sparks1
			{
				intensity = 0.5;
				interval = 1;
				lifeTime = 0;
				position = "destructionEffect2";
				simulation = "particles";
				type = "ObjectDestructionSparks";
			};
			class Smoke1
			{
				simulation="particles";
				type="BarelDestructionSmoke";
				position[]={0,0,0};
				intensity=0.2;
				interval=1;
				lifeTime=1;
			};
			class HouseDestr
			{
				intensity=1;
				interval=1;
				lifeTime=5;
				position="";
				simulation="destroy";
				type="DelayedDestruction";
			};
		};
	};
};