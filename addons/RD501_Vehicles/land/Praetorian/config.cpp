//Get this addons macro

//get the macro for the air RD501_patch_vehicles

//get generlized macros
#include "../../../RD501_main/config_macros.hpp"

//General name of the vehicle
#define vehicle_addon Praetorian_Variant
#define patch_name MODNAME##vehicle_addon##_Patches
#define vehicle_classname MODNAME##_##vehicle_addon

#define macro_new_prea_class(name) vehicle_classname##_##name


class CfgPatches
{
	class RD501_patch_Praetorian_Variants
	{
		addonRootClass=MACRO_QUOTE(RD501_patch_vehicles);

		requiredAddons[]=
		{
			RD501_patch_vehicles
		};
		requiredVersion=0.1;
		units[]=
		{
			"RD501_CIS_SHORAD",
			"RD501_Republic_SHORAD"
		};
		weapons[]=
		{
			
		};
	};
};
class SensorTemplatePassiveRadar;
class SensorTemplateAntiRadiation;
class SensorTemplateActiveRadar;
class SensorTemplateIR;
class SensorTemplateVisual;
class SensorTemplateMan;
class SensorTemplateLaser;
class SensorTemplateNV;
class SensorTemplateDataLink;
class DefaultVehicleSystemsDisplayManagerLeft
{
	class components;
};
class DefaultVehicleSystemsDisplayManagerRight
{
	class components;
};
class VehicleSystemsTemplateLeftPilot: DefaultVehicleSystemsDisplayManagerLeft
{
	class components;
};
class VehicleSystemsTemplateRightPilot: DefaultVehicleSystemsDisplayManagerRight
{
	class components;
};
class CfgDestructPos
{
	scope = 1;
	class DelayedDestruction;
	class DelayedDestructionAmmo: DelayedDestruction
	{
		timeBeforeHiding = "21";
		hideDuration = "10";
	};
};
class Optics_Armored;
class Optics_Gunner_APC_01: Optics_Armored
{
	class Wide;
	class Medium;
	class Narrow;
};
class WeaponCloudsMGun;
class DefaultEventhandlers;
class CfgVehicles
{	
	class All;
	class Strategic;
	class Land;
	class LandVehicle: Land
	{
		class ViewPilot;
		class ViewGunner;
		class NewTurret;
	};
	class StaticWeapon: LandVehicle
	{
		class Turrets
		{
			class MainTurret;
		};
	};
	class StaticMGWeapon: StaticWeapon
	{
		class Turrets: Turrets
		{
			class MainTurret: MainTurret
			{
				class ViewOptics;
			};
		};
		class Components;
	};
	class AAA_System_01_base_F:StaticMGWeapon
	{
		class Turrets: Turrets
		{
			class MainTurret:MainTurret{};
		};
	};
	class B_AAA_System_01_F:AAA_System_01_base_F
	{
		class Turrets: Turrets
		{
			class MainTurret:MainTurret{};
		};
	};
	
	class RD501_CIS_SHORAD: B_AAA_System_01_F
	{
		scope = 2;
		forceInGarage=1;
		allowTabLock = 1;
		canUseScanner = 1;
		driverCanSee = 31;
		gunnerCanSee = 31;
		commanderCanSee = 31; 
		canLock = 2; 
		cmImmunity = 1; 
		lockAcquire = 0; 
		weaponLockDelay = 0;
		radarType = 2; 
		irScanToEyeFactor = 1;
		irScanRangeMin = 0;
		irScanRangeMax = 100000;
		irScanGround = 1;
		weaponLockSystem = "0"; 

		receiveRemoteTargets = true;
		reportRemoteTargets = false;
		reportOwnPosition = true;
		scopecurator = 2;
		displayName = "CIS SHORAD";
		side = 0;
		editorPreview = "";
		author = "RD501";

		visualTarget = 0; 
		visualTargetSize = 0;
		radarTarget = 1;
		radarTargetSize = 1;
		nvTarget = 0;
		laserTarget = 0;
		irTarget = 1;
		irTargetSize = 1;
		countermeasureActivationRadius = 2000;

		faction = MACRO_QUOTE(macro_faction(CIS));
		editorSubcategory = MACRO_QUOTE(macro_editor_cat(AA));
		vehicleClass = MACRO_QUOTE(macro_editor_vehicle_type(AA));
		class Turrets: Turrets
		{
			class MainTurret: MainTurret
			{
				canUseScanner = 1; 
				weapons[] = {
					"RD501_CIS_Laser_AA"
				};
				magazines[] = {
					"RD501_CIS_Laster_AA_Mag",
					"RD501_CIS_Laster_AA_Mag",
					"RD501_CIS_Laster_AA_Mag",
					"RD501_CIS_Laster_AA_Mag",
					"RD501_CIS_Laster_AA_Mag",
				};
			};
		};
		class components
		{
			class SensorsManagerComponent
			{
				class components
				{
					class VisualSensorComponent: SensorTemplateVisual
					{
						typeRecognitionDistance = 16000;
						angleRangeHorizontal 	= 360;
						angleRangeVertical 		= 360;
						groundNoiseDistanceCoef = -1;
						maxGroundNoiseDistance 	= -1;
						minSpeedThreshold 		= 0;
						maxSpeedThreshold 		= 1000;
						maxTrackableSpeed 		= 500;
						class AirTarget
						{
							minRange = 0; 
							maxRange = 3000;                                              
							objectDistanceLimitCoef = -1; 
							viewDistanceLimitCoef = -1;             						
						};
						class GroundTarget
						{
							minRange = 0; 
							maxRange = 3000;                                              
							objectDistanceLimitCoef = -1; 
							viewDistanceLimitCoef = -1;             
						};					
					};
					class DataLinkSensorComponent: SensorTemplateDataLink
					{
						typeRecognitionDistance = 16000;
						angleRangeHorizontal 	= 360;
						angleRangeVertical 		= 360;
						groundNoiseDistanceCoef = -1;
						maxGroundNoiseDistance 	= 1600;
						minSpeedThreshold 		= 0;
						maxSpeedThreshold 		= 2000;
						class AirTarget
						{
							minRange = 0; 
							maxRange = 16000;                                              
							objectDistanceLimitCoef = -1; 
							viewDistanceLimitCoef = -1;             						
						};
						class GroundTarget
						{
							minRange = 0; 
							maxRange = 16000;                                              
							objectDistanceLimitCoef = -1; 
							viewDistanceLimitCoef = -1;             
						};					
					};
				};
			};
		};	

		class EventHandlers :DefaultEventhandlers{};
	};

	class RD501_Republic_SHORAD : RD501_CIS_SHORAD
	{
		side = 1;
		faction = MACRO_QUOTE(macro_faction(republic));
		displayName = "Republic SHORAD";
	};

};

class Extended_Init_EventHandlers
{
	class RD501_CIS_SHORAD
	{
        class RD501_CSHORAD_Radar_On
        {
            init = "(_this#0) setVehicleRadar 1;";
        };
	};
	class RD501_Republic_SHORAD
	{
        class RD501_RSHORAD_Radar_On
        {
            init = "(_this#0) setVehicleRadar 1;";
        };
	};
};