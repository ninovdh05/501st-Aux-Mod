class cfgPatches
{
    class Aux501_Patch_Vehicles_Republic_Gunships_LAAT_C
    {
        addonRootClass = "RD501_patch_vehicles";
        requiredAddons[] = 
        {
            "A3_data_F",
            "A3_anims_F",
            "A3_weapons_F",
            "A3_characters_F",
            "lsd_vehicles_heli"
        };
        units[] = 
        {
            "Aux501_Vehicles_Republic_Gunships_LAAT_C"
        };
        weapons[] = {};
    };
};

class CfgVehicles
{
    class Helicopter_Base_H;

    class lsd_laatc_base: Helicopter_Base_H
    {
        class ACE_SelfActions;
    };

    class Aux501_Vehicles_Republic_Gunships_LAAT_C: lsd_laatc_base
    {
        scope = 2;
        scopecurator = 2;
        author = "501st Aux Team";
        faction = "RD501_republic_Faction";
        editorSubcategory = "RD501_Editor_Category_Air_Republic_heli";
        vehicleClass = "RD501_Air_Vehicle_Class_Republic";
        displayName = "LAAT/C";
        tas_can_impulse = 0;
        ls_impulsor_fuelDrain_1 = 1.5e-05;
        ls_impulsor_fuelDrain_2 = 3.5e-05;
        ls_impulsor_boostSpeed_1 = 400;
        ls_impulsor_boostSpeed_2 = 600;
        ls_hasImpulse = 1;
        RD501_magclamp_small_1[] = {-8.0,-2.0,-5.0};
        RD501_magclamp_large[] = {0.0,-2.0,-5.0};
        RD501_magclamp_small_2[] = {8.0,-2.0,-5.0};
        RD501_magclamp_small_forbidden = 1;
        RD501_magclamp_large_offset[] = {0.0,1.0,-4.5};
        class ACE_SelfActions: ACE_SelfActions
        {
            class ACE_Passengers
            {
                condition = "alive _target";
                displayName = "Passengers";
                insertChildren = "_this call ace_interaction_fnc_addPassengersActions";
                statement = "";
            };
            class RD501_HUD_Changer
            {
                displayName = "Change HUD Color :)";
                exceptions[] = {"isNotInside","isNotSwimming","isNotSitting"};
                condition = "!(isNull objectParent player) && (driver (vehicle player)==player)";
                showDisabled = 0;
                priority = 2.5;
                icon = "RD501_Main\textures\interaction_Icons\colorWheel.paa";
                class RD501_Red_HUD
                {
                    displayName = "Red HUD Color";
                    exceptions[] = {"isNotInside","isNotSwimming","isNotSitting"};
                    condition = "!(isNull objectParent player)";
                    statement = "[1,0,0,1,vehicle player] spawn RD501_fnc_change_hud_color";
                    showDisabled = 0;
                    runOnHover = 1;
                    priority = 2.5;
                    icon = "RD501_Main\textures\interaction_Icons\red.paa";
                };
                class RD501_Orange_HUD: RD501_Red_HUD
                {
                    displayName = "Orange HUD Color";
                    statement = "[1,.05,0,1,vehicle player] spawn RD501_fnc_change_hud_color";
                    icon = "RD501_Main\textures\interaction_Icons\orange.paa";
                };
                class RD501_Yellow_HUD: RD501_Red_HUD
                {
                    displayName = "Yellow HUD Color";
                    statement = "[1,1,0,1,vehicle player] spawn RD501_fnc_change_hud_color";
                    icon = "RD501_Main\textures\interaction_Icons\yellow.paa";
                };
                class RD501_Green_HUD: RD501_Red_HUD
                {
                    displayName = "Green HUD Color";
                    statement = "[0,1,0,1,vehicle player] spawn RD501_fnc_change_hud_color";
                    icon = "RD501_Main\textures\interaction_Icons\green.paa";
                };
                class RD501_Cyan_HUD: RD501_Red_HUD
                {
                    displayName = "Cyan HUD Color";
                    statement = "[0,1,1,1,vehicle player] spawn RD501_fnc_change_hud_color";
                    icon = "RD501_Main\textures\interaction_Icons\cyan.paa";
                };
                class RD501_Blue_HUD: RD501_Red_HUD
                {
                    displayName = "Blue HUD Color";
                    statement = "[0,0,1,1,vehicle player] spawn RD501_fnc_change_hud_color";
                    icon = "RD501_Main\textures\interaction_Icons\blue.paa";
                };
                class RD501_Purple_HUD: RD501_Red_HUD
                {
                    displayName = "Purple HUD Color";
                    statement = "[.5,0,.5,1,vehicle player] spawn RD501_fnc_change_hud_color";
                    icon = "RD501_Main\textures\interaction_Icons\purple.paa";
                };
                class RD501_White_HUD: RD501_Red_HUD
                {
                    displayName = "White HUD Color";
                    statement = "[1,1,1,1,vehicle player] spawn RD501_fnc_change_hud_color";
                    icon = "RD501_Main\textures\interaction_Icons\white.paa";
                };
                class RD501_Black_HUD: RD501_Red_HUD
                {
                    displayName = "Black HUD Color";
                    statement = "[0,0,0,1,vehicle player] spawn RD501_fnc_change_hud_color";
                    icon = "RD501_Main\textures\interaction_Icons\black.paa";
                };
                class RD501_Clear_HUD: RD501_Red_HUD
                {
                    displayName = "No HUD Color";
                    statement = "[1,1,1,0,vehicle player] spawn RD501_fnc_change_hud_color";
                    icon = "RD501_Main\textures\interaction_Icons\noHud.paa";
                };
            };
        };
    };
};