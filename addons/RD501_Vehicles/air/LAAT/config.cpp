//Get this addons macro

//get the macro for the air subaddon

//get generlized macros
#include "../../../RD501_main/config_macros.hpp"

//General name of the vehicle
#define vehicle_addon LAAT
#define patch_name MODNAME##vehicle_addon##_Patches
#define vehicle_classname MODNAME##_##vehicle_addon

#define macro_new_laat(name) CONCAT_3(vehicle_classname,_,name)

class CfgPatches
{
	class RD501_patch_laat
	{
		addonRootClass = "RD501_patch_vehicles";
		requiredAddons[] = 
		{
			"RD501_patch_vehicles",
			"3AS_LAAT"
		};
		requiredVersion = 0.1;
		units[]=
		{
			"RD501_LAAT_MKIII_Balls",
			"RD501_Laat_LE",
			"RD501_Laat_LE_Unarmed",
			"RD501_LAAT_R"
		};
		weapons[]=
		{
			
		};
	};
};

class Extended_init_EventHandlers
{
	class RD501_Laat_Base
	{
		class laat_init_eh
		{
			init = "(_this) spawn ls_vehicle_fnc_ImpulsorMonitor; [_this select 0] execVM 'RD501_Main\functions\autocrate\autocrate.sqf';";
		};
	};

	class RD501_LAAT_MKIII_Balls
	{
		class laat_init_eh
		{
			init = "(_this) spawn ls_vehicle_fnc_ImpulsorMonitor; [_this select 0] execVM 'RD501_Main\functions\autocrate\autocrate.sqf';";
		};
	};

	class RD501_Laat_LE
	{
		class laat_init_eh
		{
			init = "(_this) spawn ls_vehicle_fnc_ImpulsorMonitor; [_this select 0] execVM 'RD501_Main\functions\autocrate\autocrate.sqf'; [_this select 0] execVM 'RD501_Main\functions\autocrate\autocrate_small.sqf';";
		};
	};

	class RD501_Laat_C
	{
		class laat_init_eh
		{
			init = "(_this) spawn ls_vehicle_fnc_ImpulsorMonitor;";
		};
	};
};

#include "../../common/sensor_templates.hpp"
class DefaultEventhandlers; 
class CfgVehicles
{

	#include "inheritance.hpp"
	class RD501_Laat_Base: 3as_LAAT_Mk1
	{
		tas_can_impulse = 0;
		ls_impulsor_fuelDrain_1 = 0.000075;
		ls_impulsor_fuelDrain_2 = 0.0002;
		ls_impulsor_boostSpeed_1 = 400;
		ls_impulsor_boostSpeed_2 = 600;
		ls_hasImpulse = 1;

		ace_fastroping_enabled = 1;
		ace_fastroping_ropeOrigins[] = {
			{1.5, 1, -0.2},
			{1.5, 2.5, -0.2},
			{1.5, -1, -0.2},
			{-1.5, 1, -0.2},
			{-1.5, 2.5, -0.2},
			{-1.5, -1, -0.2}
		};

		class ACE_SelfActions: ACE_SelfActions
		{
			class ACE_Passengers
			{
				condition = "alive _target";
				displayName = "Passengers";
				insertChildren = "_this call ace_interaction_fnc_addPassengersActions";
				statement = "";
			};
			#include "../../common/universal_hud_color_changer.hpp"
		};
		soundSetSonicBoom[] = {"Plane_Fighter_SonicBoom_SoundSet"};
		scope=1;
		scopeCurator=1;
		author="RD501";
		forceInGarage = 0;

		irTarget = 1;
		irTargetSize = 1;
		radarTarget = 1;
		radarTargetSize = 1;
		receiveRemoteTargets = true;
		reportRemoteTargets = true;
		reportOwnPosition = true;
		threat[] = {.4, .7, .9};
		cost= 10000;

		faction = MACRO_QUOTE(macro_faction(republic));
		editorSubcategory = MACRO_QUOTE(macro_editor_cat_air(Republic_heli));
		vehicleClass = MACRO_QUOTE(macro_editor_vehicle_type_air(Republic));

		RD501_magclamp_small_forbidden=1;
		RD501_magclamp_small_1[] = {0.0,1.0,-1.0};
		RD501_magclamp_large_offset[]={0.0,1.6,-7.3};

		///Flight model
			liftForceCoef = 2;
			bodyFrictionCoef = 5;//25.5;//0.6777;
			cyclicAsideForceCoef = 4*1.2;
			cyclicForwardForceCoef = 2*1.2;
			maxSpeed=200;
			fuelCapacity = 700;
			fuelConsumptionRate = 0.2;
			altFullForce = 16000;
			altNoForce = 19000;
		///dmg properties
			armor=500;
			crewCrashProtection=0;
			epeImpulseDamageCoef=0;
		///
		weapons[] = {
			"Laserdesignator_pilotCamera",
			"RD501_CMFlareLauncher",
			"SmokeLauncherMK2",
			"RD501_Republic_Aircraft_Laser_AA",
			"RD501_Aircraft_Laser_Cannon",
			MACRO_QUOTE(macro_new_weapon_nofam(guided_resupply_pod_launcher))
		};
		magazines[] = {
			"Laserbatteries",
			"RD501_CMFlare_Magazine_200",
			"RD501_CMFlare_Magazine_200",
			"RD501_Republic_Aircraft_Laser_AA_Mag_600",
			"RD501_Republic_Aircraft_Laser_Cannon_Mag_100",
			"SmokeLauncherMagMK2",
			"SmokeLauncherMagMK2",
			"SmokeLauncherMagMK2",
			"SmokeLauncherMagMK2",
			"RD501_Guided_Resupply_Ammo_Resupply_Magazine", 
            "RD501_Guided_Resupply_Medical_Resupply_Magazine"
		};
		smokeLauncherGrenadeCount=24;
		smokeLauncherAngle=360;
		smokeLauncherVelocity=14;

		#include "../../common/common_pilotCamera.hpp"
		class ViewPilot: ViewPilot
		{
			initAngleX = 0;
		};
		class Components : Components
		{
			class VehicleSystemsDisplayManagerComponentLeft: VehicleSystemsTemplateLeftPilot
			{
				class Components: components
				{
					class EmptyDisplay
					{
						componentType = "EmptyDisplayComponent";
					};
					class MinimapDisplay
					{
						componentType = "MinimapDisplayComponent";
						resource = "RscCustomInfoAirborneMiniMap";
					};
					class CrewDisplay
					{
						componentType = "CrewDisplayComponent";
						resource = "RscCustomInfoCrew";
					};
					class UAVDisplay
					{
						componentType = "UAVFeedDisplayComponent";
					};
					class VehiclePrimaryGunnerDisplay
					{
						componentType = "TransportFeedDisplayComponent";
						source = "PrimaryGunner";
					};
					class VehicleMissileDisplay
					{
						componentType = "TransportFeedDisplayComponent";
						source = "Missile";
					};
					class SensorDisplay
					{
						componentType = "SensorsDisplayComponent";
						range[] = {4000,8000,16000,300,2000};
						resource = "RscCustomInfoSensors";
					};
				};
			};
			class VehicleSystemsDisplayManagerComponentRight: VehicleSystemsTemplateRightPilot
			{
				defaultDisplay = "SensorDisplay";
				class Components: components
				{
					class EmptyDisplay
					{
						componentType = "EmptyDisplayComponent";
					};
					class MinimapDisplay
					{
						componentType = "MinimapDisplayComponent";
						resource = "RscCustomInfoAirborneMiniMap";
					};
					class CrewDisplay
					{
						componentType = "CrewDisplayComponent";
						resource = "RscCustomInfoCrew";
					};
					class UAVDisplay
					{
						componentType = "UAVFeedDisplayComponent";
					};
					class VehiclePrimaryGunnerDisplay
					{
						componentType = "TransportFeedDisplayComponent";
						source = "PrimaryGunner";
					};
					class VehicleMissileDisplay
					{
						componentType = "TransportFeedDisplayComponent";
						source = "Missile";
					};
					class SensorDisplay
					{
						componentType = "SensorsDisplayComponent";
						range[] = {4000,8000,16000,300,2000};
						resource = "RscCustomInfoSensors";
					};
				};
			};
			
			class SensorsManagerComponent
			{
				class Components
				{
					class IRSensorComponent: SensorTemplateIR
					{
						class AirTarget
						{
							minRange=0;
							maxRange=3500;
							objectDistanceLimitCoef=-1;
							viewDistanceLimitCoef=1;
						};
						class GroundTarget
						{
							minRange=0;
							maxRange=3500;
							objectDistanceLimitCoef=-1;
							viewDistanceLimitCoef=1;
						};
						componentType = "IRSensorComponent";

						typeRecognitionDistance = 3000;
						angleRangeHorizontal = 120;
						angleRangeVertical = 40;
						maxFogSeeThrough = 0.85;

						groundNoiseDistanceCoef = -1;
						maxGroundNoiseDistance = 0;

						minSpeedThreshold = 0;
						maxSpeedThreshold = 1000;

						minTrackableSpeed = -1e10;
						maxTrackableSpeed = 1e10;

						minTrackableATL = -1e10;
						maxTrackableATL = 1e10;

						allowsMarking = 1;
						aimDown = 10;
						animDirection="";
						color[]={1,0,0,1};
					};
					
					class ActiveRadarSensorComponent: SensorTemplateActiveRadar
					{
						class AirTarget
						{
							minRange=0;
							maxRange=5000;
							objectDistanceLimitCoef=-1;
							viewDistanceLimitCoef=-1;
						};
						class GroundTarget
						{
							minRange=0;
							maxRange=5000;
							objectDistanceLimitCoef=-1;
							viewDistanceLimitCoef=-1;
						};
						componentType = "ActiveRadarSensorComponent";

						typeRecognitionDistance = 4000;
						angleRangeHorizontal = 90;
						angleRangeVertical = 90;
						maxFogSeeThrough = -1;

						groundNoiseDistanceCoef = -1;
						maxGroundNoiseDistance = 0;

						minSpeedThreshold = 0;
						maxSpeedThreshold = 1000;

						minTrackableSpeed = -1e10;
						maxTrackableSpeed = 1e10;

						minTrackableATL = -1e10;
						maxTrackableATL = 1e10;

						allowsMarking = 1;
						aimDown = 0;
						animDirection="";
						color[]={0,1,1,1};
					};

					class VisualRadarSensorComponent: SensorTemplateVisual
					{
						class AirTarget
						{
							minRange	= 0;
							maxRange	= 1000;
							objectDistanceLimitCoef	= -1;
							viewDistanceLimitCoef	= -1;
						};
						class GroundTarget
						{
							minRange = 0;
							maxRange = 1000;
							objectDistanceLimitCoef = -1;
							viewDistanceLimitCoef 	= -1;
						};
						componentType = "VisualSensorComponent";

						typeRecognitionDistance = 750;
						angleRangeHorizontal = 90;
						angleRangeVertical = 90;
						maxFogSeeThrough = -1;

						groundNoiseDistanceCoef = -1;
						maxGroundNoiseDistance = 0;

						minSpeedThreshold = 0;
						maxSpeedThreshold = 1000;

						minTrackableSpeed = -1e10;
						maxTrackableSpeed = 1e10;

						minTrackableATL = -1e10;
						maxTrackableATL = 1e10;

						allowsMarking = 1;
						aimDown = 0;
						color[]={1,1,0.5,0.80000001};
					};

					class ManRadarSensorComponent: SensorTemplateMan
					{
						class AirTarget
						{
							minRange	= 0;
							maxRange	= 300;
							objectDistanceLimitCoef	= -1;
							viewDistanceLimitCoef	= -1;
						};
						class GroundTarget
						{
							minRange = 0;
							maxRange = 300;
							objectDistanceLimitCoef = -1;
							viewDistanceLimitCoef 	= -1;
						};
						componentType = "ManSensorComponent";

						typeRecognitionDistance = 300;
						angleRangeHorizontal = 1800;
						angleRangeVertical = 180;
						maxFogSeeThrough = 0.7;

						groundNoiseDistanceCoef = -1;
						maxGroundNoiseDistance = 0;

						minSpeedThreshold = 0;
						maxSpeedThreshold = 1000;

						minTrackableSpeed = -1e10;
						maxTrackableSpeed = 1e10;

						minTrackableATL = -1e10;
						maxTrackableATL = 1e10;

						allowsMarking = 1;
						aimDown = 0;
					};
						
					class LaserSensorComponent: SensorTemplateLaser
					{
						class AirTarget
						{
							minRange	= 0;
							maxRange	= 16000;
							objectDistanceLimitCoef	= -1;
							viewDistanceLimitCoef	= -1;
						};
						class GroundTarget
						{
							minRange = 0;
							maxRange = 16000;
							objectDistanceLimitCoef = -1;
							viewDistanceLimitCoef 	= -1;
						};
						componentType="LaserSensorComponent";
						angleRangeHorizontal=360;
						angleRangeVertical=180;
						aimDown=0;
						maxFogSeeThrough = 0.85;
					};

					class NVSensorComponent: SensorTemplateNV
					{
						class AirTarget
						{
							minRange	= 0;
							maxRange	= 16000;
							objectDistanceLimitCoef	= -1;
							viewDistanceLimitCoef	= -1;
						};
						class GroundTarget
						{
							minRange = 0;
							maxRange = 16000;
							objectDistanceLimitCoef = -1;
							viewDistanceLimitCoef 	= -1;
						};
						componentType="NVSensorComponent";
						angleRangeHorizontal=360;
						angleRangeVertical=180;
						aimDown=0;
						maxFogSeeThrough = 0.85;
					};
				
					class DataLinkSensorComponent: SensorTemplateDataLink
					{
						class AirTarget
						{
							minRange	= 0;
							maxRange	= 16000;
							objectDistanceLimitCoef	= -1;
							viewDistanceLimitCoef	= -1;
						};
						class GroundTarget
						{
							minRange = 0;
							maxRange = 16000;
							objectDistanceLimitCoef = -1;
							viewDistanceLimitCoef 	= -1;
						};
						componentType="DataLinkSensorComponent";
						allowsMarking=1;
						typeRecognitionDistance=0;
						color[]={1,1,1,0};
					};

				};
			};
			
			class TransportPylonsComponent
			{
				uiPicture = "\A3\Air_F_Gamma\Plane_Fighter_03\Data\UI\Plane_A143_3DEN_CA.paa";

				class Pylons
				{
					class pylons1
					{
						hardpoints[]=
						{
							"RD501_Republic_Aircraft_Missile_AA_IR_Pylon",
							"RD501_Republic_Aircraft_Missile_AT_IR_Pylon",
							"RD501_Republic_Aircraft_Missile_AT_Wire_Pylon",
							"RD501_Republic_Aircraft_Missile_AP_Pylon"
						};
						attachment = "empty";
						priority=6;
						maxweight=75;
						UIposition[]={0.15,0.45};
					};
					class pylons2: pylons1
					{
						priority=4;
						UIposition[]={0.2,0.35};
					};
					class pylons3: pylons1
					{
						priority=2;
						UIposition[]={0.25,0.25};
					};
					class pylons4: pylons1
					{
						priority=1;
						UIposition[]={0.6,0.25};
						mirroredMissilePos=3;
					};
					class pylons5: pylons1
					{
						priority=3;
						UIposition[]={0.65,0.35};
						mirroredMissilePos=2;
					};
					class pylons6: pylons1
					{
						priority=5;
						UIposition[]={0.7,0.45};
						mirroredMissilePos=1;
					};
				};

				class Presets
				{
					class Empty
					{
						displayName = "empty";
						attachment[]={};
					};

					class Standard
					{
						displayName = "Standard";
						attachment[]=
						{
							"RD501_Republic_Aircraft_Missile_AA_IR_Pylon",
							"RD501_Republic_Aircraft_Missile_AT_IR_Pylon",
							"RD501_Republic_Aircraft_Missile_AT_Wire_Pylon",
							"RD501_Republic_Aircraft_Missile_AT_Wire_Pylon",
							"RD501_Republic_Aircraft_Missile_AT_IR_Pylon",
							"RD501_Republic_Aircraft_Missile_AA_IR_Pylon"
						};
					};
				};
			};
		};
	
		//transport
		class TransportWeapons
		{
			#include "../../common/common_weap.hpp"
		};
		class TransportMagazines
		{
			#include "../../common/common_ammo_mag.hpp"
		};
		class TransportItems
		{
			#include "../../common/common_items_medical.hpp"
			#include "../../common/common_items.hpp"
		};
	};

	class RD501_LAAT_MKIII_Balls: RD501_Laat_Base
	{
		displayName="LAAT/I MK.III";
		laserScanner = 1;
		scope = 2;
		scopeCurator = 2;

		class UserActions: UserActions
		{
			delete impulseOn;
			
			delete impulseOff;

			class rampOpen: rampOpen
			{
				condition="(this animationphase 'ramp' == 0) AND (alive this) AND (player in [gunner this, driver this])";
			};

			class rampClose: rampClose
			{
				condition="(this animationphase 'ramp' == 1) AND (alive this) AND (player in [gunner this, driver this])";
			};
		};

		hiddenSelectionsTextures[]=
        {
            "RD501_Vehicles\textures\LAAT\hull_tmp_co.paa",
            "3AS\3as_Laat\LAATI\data\wings_501_CO.paa",
			"3AS\3as_Laat\LAATI\data\weapons_CO.paa",
			"3AS\3as_Laat\LAATI\data\weapon_Details_CO.paa",
			"3AS\3as_Laat\LAATI\data\interior_CO.paa"
        };

		class Turrets: Turrets
		{
			class Gunner: Copilot
			{
				minelev=-60;
				minturn=-240;
				maxelev=40;
				maxturn=-120;
				#include "../../common/common_optics.hpp"
				weapons[]=
					{
						macro_new_weapon(generic,republic_aircraft_cannon),
						"Laserdesignator_pilotCamera"
					};
				magazines[]=
					{
						"Laserbatteries",
						macro_new_mag(generic_aircraft_gun_green,1000),
						macro_new_mag(generic_aircraft_gun_green,1000),
						macro_new_mag(generic_aircraft_gun_green,1000)
					};
				memorypointgunneroptics="FIXME";

			};
			class LeftDoorgun: LeftDoorgun
			{
				weapons[]=
				{
					macro_new_weapon(turret,laat_ball_beam_l),
					"Laserdesignator_pilotCamera"
				};
				magazines[]=
				{
					"Laserbatteries",
					"RD501_Republic_Aircraft_Laser_Beam_Mag_500",
					"RD501_Republic_Aircraft_Laser_Beam_Mag_500",
					"RD501_Republic_Aircraft_Laser_Beam_Mag_500"
				};
				#include "../../common/common_optics.hpp"
				memorypointgunneroptics="FIXME";
				stabilizedInAxes = 3;
			};
			class RightDoorGun: RightDoorGun
			{
				weapons[]=
				{
					macro_new_weapon(turret,laat_ball_beam_r),
					"Laserdesignator_pilotCamera"
				};
				magazines[]=
				{
					"Laserbatteries",
					"RD501_Republic_Aircraft_Laser_Beam_Mag_500",
					"RD501_Republic_Aircraft_Laser_Beam_Mag_500",
					"RD501_Republic_Aircraft_Laser_Beam_Mag_500"
				};
				#include "../../common/common_optics.hpp"
				memorypointgunneroptics="FIXME";
				stabilizedInAxes = 3;
			};
			class CargoTurret_01: CargoTurret_01{};
			class CargoTurret_02: CargoTurret_02{};
			class CargoTurret_03: CargoTurret_03{};
			class CargoTurret_04: CargoTurret_04{};
			class CargoTurret_05: CargoTurret_05{};
			class CargoTurret_06: CargoTurret_06{};
		};

		class rd501_jumppackDispenser {
            jumppack="RD501_JLTS_Clone_droppack";
        };

		#include "LAATi_texture_selector.hpp"

	};
	class RD501_Laat_LE: 3AS_Patrol_LAAT_Republic
	{
		tas_can_impulse = 0;
		displayName="Republic LAAT/LE";
		TFAR_hasIntercom = 1; //maybe fix intercomm maybe do nothing? maybe break all of CBA monki dont know?
		ls_impulsor_fuelDrain_1 = 0.0001;
		ls_impulsor_fuelDrain_2 = 0.0002;
		ls_impulsor_boostSpeed_1 = 450;
		ls_impulsor_boostSpeed_2 = 700;
		ls_hasImpulse = 1;
		maxSpeed=600;
		class TransportWeapons
		{
			#include "../../common/common_weap.hpp"
		};
		class TransportMagazines
		{
			#include "../../common/common_ammo_mag.hpp"
		};
		class TransportItems
		{
			#include "../../common/common_items_medical.hpp"
			#include "../../common/common_items.hpp"
		};
		scope=2;
		author="RD501";
		forceInGarage = 1;
		receiveRemoteTargets = false;
		reportRemoteTargets = true;
		reportOwnPosition = true;
		laserScanner = 1;
		irTarget = 1;
		irTargetSize = 1;
		radarTarget = 1;
		radarTargetSize = 0.9;
		threat[] = {.5, .5, .4};
		cost= 9500;

		ace_cargo_space = 12;
        ace_cargo_hasCargo = 1;
		
		fuelCapacity = 450;
		fuelConsumptionRate = 0.2;

		altFullForce = 16000;
		altNoForce = 19000;

		memoryPointGun[] = {"L_Muzzle","R_muzzle"};
		gunBeg[] = {"L_Muzzle","R_Muzzle"};
		gunEnd[] = {"L_Chamber","R_Chamber"};

		RD501_magclamp_large_offset[] = {0.0, 0.0, -5.0};
		RD501_magclamp_small_offset[] = {0.0, 0.0, -5.0};

		faction = MACRO_QUOTE(macro_faction(republic));
		editorSubcategory = MACRO_QUOTE(macro_editor_cat_air(Republic_heli));
		vehicleClass = MACRO_QUOTE(macro_editor_vehicle_type_air(Republic));
		transportSoldier=14;
		class UserActions
		{
			delete impulseOn;
			
			delete impulseOff;
		};
		class ACE_SelfActions
		{
			class ACE_Passengers
			{
				condition = "alive _target";
				displayName = "Passengers";
				insertChildren = "_this call ace_interaction_fnc_addPassengersActions";
				statement = "";
			};
			#include "../../common/universal_hud_color_changer.hpp"
		};
		RD501_magclamp_small_1[] = {0.0,1.0,-1.0};
		weapons[]=
		{
			"Laserdesignator_pilotCamera",
			"RD501_CMFlareLauncher",
			"RD501_Republic_Aircraft_Laser_Blaster",
			"RD501_Republic_Aircraft_Missile_AP"				
		};
		magazines[]=
		{
			"Laserbatteries",
			"RD501_CMFlare_Magazine_200",
			"RD501_CMFlare_Magazine_200",
			"RD501_Republic_Aircraft_Laser_Blaster_Mag_75",
			"RD501_Republic_Aircraft_Laser_Blaster_Mag_75",
			"RD501_Republic_Aircraft_Missile_AP_Mag_8"
		};
		class Turrets: Turrets
		{

			class Gunner: Copilot
			{
				outGunnerMayFire=1;
				commanding=-1;
				primaryGunner=1;
				weapons[]=
					{
						"RD501_Republic_Aircraft_Laser_Repeater",
						"Laserdesignator_pilotCamera"
					};
				magazines[]=
					{
						"RD501_Republic_Aircraft_Laser_Repeater_Mag_1000"
					};
				#include "../../common/common_optics.hpp"
			};
		};

		class rd501_jumppackDispenser {
            jumppack="RD501_JLTS_Clone_droppack";
        };

		class Components : Components
		{
			class VehicleSystemsDisplayManagerComponentLeft: VehicleSystemsTemplateLeftPilot
			{
				class Components: components
				{
					class EmptyDisplay
					{
						componentType = "EmptyDisplayComponent";
					};
					class MinimapDisplay
					{
						componentType = "MinimapDisplayComponent";
						resource = "RscCustomInfoAirborneMiniMap";
					};
					class CrewDisplay
					{
						componentType = "CrewDisplayComponent";
						resource = "RscCustomInfoCrew";
					};
					class UAVDisplay
					{
						componentType = "UAVFeedDisplayComponent";
					};
					class VehiclePrimaryGunnerDisplay
					{
						componentType = "TransportFeedDisplayComponent";
						source = "PrimaryGunner";
					};
					class VehicleMissileDisplay
					{
						componentType = "TransportFeedDisplayComponent";
						source = "Missile";
					};
					class SensorDisplay
					{
						componentType = "SensorsDisplayComponent";
						range[] = {2000,500};
						resource = "RscCustomInfoSensors";
					};
				};
			};
			class VehicleSystemsDisplayManagerComponentRight: VehicleSystemsTemplateRightPilot
			{
				defaultDisplay = "SensorDisplay";
				class Components: components
				{
					class EmptyDisplay
					{
						componentType = "EmptyDisplayComponent";
					};
					class MinimapDisplay
					{
						componentType = "MinimapDisplayComponent";
						resource = "RscCustomInfoAirborneMiniMap";
					};
					class CrewDisplay
					{
						componentType = "CrewDisplayComponent";
						resource = "RscCustomInfoCrew";
					};
					class UAVDisplay
					{
						componentType = "UAVFeedDisplayComponent";
					};
					class VehiclePrimaryGunnerDisplay
					{
						componentType = "TransportFeedDisplayComponent";
						source = "PrimaryGunner";
					};
					class VehicleMissileDisplay
					{
						componentType = "TransportFeedDisplayComponent";
						source = "Missile";
					};
					class SensorDisplay
					{
						componentType = "SensorsDisplayComponent";
						range[] = {2000,500};
						resource = "RscCustomInfoSensors";
					};
				};
			};
			
			class SensorsManagerComponent
			{
				class Components
				{
					
					class VisualRadarSensorComponent: SensorTemplateVisual
					{
						class AirTarget
						{
							minRange = 0;
							maxRange = 1500;
							objectDistanceLimitCoef	= -1;
							viewDistanceLimitCoef	= -1;
						};
						class GroundTarget
						{
							minRange = 0;
							maxRange = 1500;
							objectDistanceLimitCoef = -1;
							viewDistanceLimitCoef 	= -1;
						};
						componentType = "VisualSensorComponent";

						typeRecognitionDistance = 11000;
						angleRangeHorizontal = 180;
						angleRangeVertical = 90;
						maxFogSeeThrough = 0.7;

						groundNoiseDistanceCoef = -1;
						maxGroundNoiseDistance = 0;

						minSpeedThreshold = 0;
						maxSpeedThreshold = 1000;

						minTrackableSpeed = -1e10;
						maxTrackableSpeed = 1e10;

						minTrackableATL = -1e10;
						maxTrackableATL = 1e10;

						allowsMarking = 1;
						aimDown = 0;
						color[]={1,1,0.5,0.80000001};
					};

					class ManRadarSensorComponent: SensorTemplateMan
					{
						class AirTarget
						{
							minRange	= 0;
							maxRange	= 1000;
							objectDistanceLimitCoef	= -1;
							viewDistanceLimitCoef	= -1;
						};
						class GroundTarget
						{
							minRange = 0;
							maxRange = 1000;
							objectDistanceLimitCoef = -1;
							viewDistanceLimitCoef 	= -1;
						};
						componentType = "ManSensorComponent";

						typeRecognitionDistance = 1000;
						angleRangeHorizontal = 180;
						angleRangeVertical = 180;
						maxFogSeeThrough = 0.7;

						groundNoiseDistanceCoef = -1;
						maxGroundNoiseDistance = 0;

						minSpeedThreshold = 0;
						maxSpeedThreshold = 1000;

						minTrackableSpeed = -1e10;
						maxTrackableSpeed = 1e10;

						minTrackableATL = -1e10;
						maxTrackableATL = 1e10;

						allowsMarking = 1;
						aimDown = 0;
					};
						
					class LaserSensorComponent: SensorTemplateLaser
					{
						class AirTarget
						{
							minRange	= 0;
							maxRange	= 16000;
							objectDistanceLimitCoef	= -1;
							viewDistanceLimitCoef	= -1;
						};
						class GroundTarget
						{
							minRange = 0;
							maxRange = 16000;
							objectDistanceLimitCoef = -1;
							viewDistanceLimitCoef 	= -1;
						};
						componentType="LaserSensorComponent";
						angleRangeHorizontal=360;
						angleRangeVertical=180;
						aimDown=0;
						maxFogSeeThrough = 0.85;
					};

					class NVSensorComponent: SensorTemplateNV
					{
						class AirTarget
						{
							minRange	= 0;
							maxRange	= 16000;
							objectDistanceLimitCoef	= -1;
							viewDistanceLimitCoef	= -1;
						};
						class GroundTarget
						{
							minRange = 0;
							maxRange = 16000;
							objectDistanceLimitCoef = -1;
							viewDistanceLimitCoef 	= -1;
						};
						componentType="NVSensorComponent";
						angleRangeHorizontal=360;
						angleRangeVertical=180;
						aimDown=0;
						maxFogSeeThrough = 0.85;
					};
				
					class DataLinkSensorComponent: SensorTemplateDataLink
					{

					};
						class AirTarget
						{
							minRange	= 0;
							maxRange	= 16000;
							objectDistanceLimitCoef	= -1;
							viewDistanceLimitCoef	= -1;
						};
						class GroundTarget
						{
							minRange = 0;
							maxRange = 16000;
							objectDistanceLimitCoef = -1;
							viewDistanceLimitCoef 	= -1;
						};
						componentType="DataLinkSensorComponent";
						allowsMarking=1;
						typeRecognitionDistance=0;
						color[]={1,1,1,0};
				};
			};
		
		};
	};
	class RD501_Laat_LE_Unarmed: RD501_Laat_LE
	{
		displayName="Republic LAAT/LE (Unarmed)";
		weapons[]=
		{
			"Laserdesignator_pilotCamera",
			"RD501_CMFlareLauncher"
		};
		magazines[]=
		{
			"Laserbatteries",
			"RD501_CMFlare_Magazine_200",
			"RD501_CMFlare_Magazine_200"
		};
		class Turrets: Turrets
		{
			delete Gunner;
		};
	};

	class RD501_LAAT_R: RD501_LAAT_MKIII_Balls
	{
		displayName="LAAT/R 'SkyCrab'";
		laserScanner = 1;
		scope = 2;
		scopeCurator = 2;
		transportSoldier = 0;

		weapons[] = {
			"Laserdesignator_pilotCamera",
			"RD501_CMFlareLauncher",
			"SmokeLauncherMK2",
			"RD501_Republic_Aircraft_Laser_AA",
			"RD501_Republic_Aircraft_Missile_AA_IR"
		};
		magazines[] = {
			"Laserbatteries",
			"RD501_CMFlare_Magazine_200",
			"RD501_CMFlare_Magazine_200",
			"RD501_Republic_Aircraft_Laser_AA_Mag_600",
			"SmokeLauncherMagMK2",
			"SmokeLauncherMagMK2",
			"SmokeLauncherMagMK2",
			"SmokeLauncherMagMK2",
			"RD501_Republic_Aircraft_Missile_AA_IR_Mag_2",
			"RD501_Republic_Aircraft_Missile_AA_IR_Mag_2"
		};
		class Turrets: Turrets
		{
			class LeftDoorgun: LeftDoorgun
			{
				weapons[]=
				{
					macro_new_weapon(turret,laat_ball_beam_l),
					"Laserdesignator_pilotCamera"
				};
				magazines[]=
				{
					"Laserbatteries",
					"RD501_Republic_Aircraft_Laser_Beam_Mag_500",
					"RD501_Republic_Aircraft_Laser_Beam_Mag_500",
					"RD501_Republic_Aircraft_Laser_Beam_Mag_500"
				};
				#include "../../common/common_optics.hpp"
				memorypointgunneroptics="FIXME";
				stabilizedInAxes = 3;
			};
			class RightDoorGun: RightDoorGun
			{
				weapons[]=
				{
					macro_new_weapon(turret,laat_ball_beam_r),
					"Laserdesignator_pilotCamera"
				};
				magazines[]=
				{
					"Laserbatteries",
					"RD501_Republic_Aircraft_Laser_Beam_Mag_500",
					"RD501_Republic_Aircraft_Laser_Beam_Mag_500",
					"RD501_Republic_Aircraft_Laser_Beam_Mag_500"
				};
				#include "../../common/common_optics.hpp"
				memorypointgunneroptics="FIXME";
				stabilizedInAxes = 3;
			};
			delete CargoTurret_06;
			delete CargoTurret_05;
			delete CargoTurret_04;
			delete CargoTurret_03;
			delete CargoTurret_02;
			delete CargoTurret_01;
		};

		class TransportWeapons {};
		class TransportMagazines {};
		class TransportItems {};
		class Components : Components
		{
			class TransportPylonsComponent {};
			class VehicleSystemsDisplayManagerComponentLeft: VehicleSystemsTemplateLeftPilot
			{
				class Components: components
				{
					class EmptyDisplay
					{
						componentType = "EmptyDisplayComponent";
					};
					class MinimapDisplay
					{
						componentType = "MinimapDisplayComponent";
						resource = "RscCustomInfoAirborneMiniMap";
					};
					class CrewDisplay
					{
						componentType = "CrewDisplayComponent";
						resource = "RscCustomInfoCrew";
					};
					class UAVDisplay
					{
						componentType = "UAVFeedDisplayComponent";
					};
					class VehiclePrimaryGunnerDisplay
					{
						componentType = "TransportFeedDisplayComponent";
						source = "PrimaryGunner";
					};
					class VehicleMissileDisplay
					{
						componentType = "TransportFeedDisplayComponent";
						source = "Missile";
					};
					class SensorDisplay
					{
						componentType = "SensorsDisplayComponent";
						range[] = {4000,8000,16000,32000};
						resource = "RscCustomInfoSensors";
					};
				};
			};
			class VehicleSystemsDisplayManagerComponentRight: VehicleSystemsTemplateRightPilot
			{
				defaultDisplay = "SensorDisplay";
				class Components: components
				{
					class EmptyDisplay
					{
						componentType = "EmptyDisplayComponent";
					};
					class MinimapDisplay
					{
						componentType = "MinimapDisplayComponent";
						resource = "RscCustomInfoAirborneMiniMap";
					};
					class CrewDisplay
					{
						componentType = "CrewDisplayComponent";
						resource = "RscCustomInfoCrew";
					};
					class UAVDisplay
					{
						componentType = "UAVFeedDisplayComponent";
					};
					class VehiclePrimaryGunnerDisplay
					{
						componentType = "TransportFeedDisplayComponent";
						source = "PrimaryGunner";
					};
					class VehicleMissileDisplay
					{
						componentType = "TransportFeedDisplayComponent";
						source = "Missile";
					};
					class SensorDisplay
					{
						componentType = "SensorsDisplayComponent";
						range[] = {4000,8000,16000,32000};
						resource = "RscCustomInfoSensors";
					};
				};
			};
			class SensorsManagerComponent
			{
				class Components
				{
					class IRSensorComponent: SensorTemplateIR
					{
						class AirTarget
						{
							minRange=0;
							maxRange=20000;
							objectDistanceLimitCoef=-1;
							viewDistanceLimitCoef=1;
						};
						class GroundTarget
						{
							minRange=0;
							maxRange=20000;
							objectDistanceLimitCoef=-1;
							viewDistanceLimitCoef=1;
						};
						componentType = "IRSensorComponent";

						typeRecognitionDistance = 16000;
						angleRangeHorizontal = 360;
						angleRangeVertical = 180;
						maxFogSeeThrough = 0.7;

						groundNoiseDistanceCoef = -1;
						maxGroundNoiseDistance = 0;

						minSpeedThreshold = 0;
						maxSpeedThreshold = 1000;

						minTrackableSpeed = -1e10;
						maxTrackableSpeed = 1e10;

						minTrackableATL = -1e10;
						maxTrackableATL = 1e10;

						allowsMarking = 1;
						aimDown = 0;

						color[]={1,0,0,1};
					};
					
					class ActiveRadarSensorComponent: SensorTemplateActiveRadar
					{
						class AirTarget
						{
							minRange=0;
							maxRange=20000;
							objectDistanceLimitCoef=-1;
							viewDistanceLimitCoef=-1;
						};
						class GroundTarget
						{
							minRange=0;
							maxRange=20000;
							objectDistanceLimitCoef=-1;
							viewDistanceLimitCoef=-1;
						};
						componentType = "ActiveRadarSensorComponent";

						typeRecognitionDistance = 16000;
						angleRangeHorizontal = 360;
						angleRangeVertical = 180;
						maxFogSeeThrough = -1;

						groundNoiseDistanceCoef = -1;
						maxGroundNoiseDistance = 0;

						minSpeedThreshold = 0;
						maxSpeedThreshold = 1000;

						minTrackableSpeed = -1e10;
						maxTrackableSpeed = 1e10;

						minTrackableATL = -1e10;
						maxTrackableATL = 1e10;

						allowsMarking = 1;
						aimDown = 0;
						color[]={0,1,1,1};
					};

					class PassiveRadarSensorComponen: SensorTemplatePassiveRadar
					{
						class AirTarget
						{
							minRange = 0; 
							maxRange = 20000;                                              
							objectDistanceLimitCoef = -1; 
							viewDistanceLimitCoef = -1;             						
						};
						class GroundTarget
						{
							minRange = 0; 
							maxRange = 20000;                                              
							objectDistanceLimitCoef = -1; 
							viewDistanceLimitCoef = -1;             
						};	
						componentType = "PassiveRadarSensorComponent";
						typeRecognitionDistance = 16000;
						angleRangeHorizontal = 360;
						angleRangeVertical = 180;
						maxFogSeeThrough = -1;

						groundNoiseDistanceCoef = -1;
						maxGroundNoiseDistance = 0;

						minSpeedThreshold = 0;
						maxSpeedThreshold = 1000;

						minTrackableSpeed = -1e10;
						maxTrackableSpeed = 1e10;

						minTrackableATL = -1e10;
						maxTrackableATL = 1e10;
						allowsMarking = 1;
					};

					class VisualRadarSensorComponent: SensorTemplateVisual
					{
						class AirTarget
						{
							minRange	= 0;
							maxRange	= 9000;
							objectDistanceLimitCoef	= -1;
							viewDistanceLimitCoef	= -1;
						};
						class GroundTarget
						{
							minRange = 0;
							maxRange = 9000;
							objectDistanceLimitCoef = -1;
							viewDistanceLimitCoef 	= -1;
						};
						componentType = "VisualSensorComponent";
						typeRecognitionDistance = 6000;
						angleRangeHorizontal = 360;
						angleRangeVertical = 180;
						maxFogSeeThrough = -1;

						groundNoiseDistanceCoef = -1;
						maxGroundNoiseDistance = 0;

						minSpeedThreshold = 0;
						maxSpeedThreshold = 1000;

						minTrackableSpeed = -1e10;
						maxTrackableSpeed = 1e10;

						minTrackableATL = -1e10;
						maxTrackableATL = 1e10;

						allowsMarking = 1;
						aimDown = 0;
						color[]={1,1,0.5,0.80000001};
					};

					class LaserSensorComponent: SensorTemplateLaser
					{
						class AirTarget
						{
							minRange	= 0;
							maxRange	= 16000;
							objectDistanceLimitCoef	= -1;
							viewDistanceLimitCoef	= -1;
						};
						class GroundTarget
						{
							minRange = 0;
							maxRange = 16000;
							objectDistanceLimitCoef = -1;
							viewDistanceLimitCoef 	= -1;
						};
						componentType="LaserSensorComponent";
						angleRangeHorizontal=360;
						angleRangeVertical=180;
						aimDown=0;
						maxFogSeeThrough = 0.85;
					};

					class NVSensorComponent: SensorTemplateNV
					{
						class AirTarget
						{
							minRange	= 0;
							maxRange	= 16000;
							objectDistanceLimitCoef	= -1;
							viewDistanceLimitCoef	= -1;
						};
						class GroundTarget
						{
							minRange = 0;
							maxRange = 16000;
							objectDistanceLimitCoef = -1;
							viewDistanceLimitCoef 	= -1;
						};
						componentType="NVSensorComponent";
						angleRangeHorizontal=360;
						angleRangeVertical=180;
						aimDown=0;
						maxFogSeeThrough = 0.85;
					};
				
					class DataLinkSensorComponent: SensorTemplateDataLink
					{
						class AirTarget
						{
							minRange	= 0;
							maxRange	= 16000;
							objectDistanceLimitCoef	= -1;
							viewDistanceLimitCoef	= -1;
						};
						class GroundTarget
						{
							minRange = 0;
							maxRange = 16000;
							objectDistanceLimitCoef = -1;
							viewDistanceLimitCoef 	= -1;
						};
						componentType="DataLinkSensorComponent";
						allowsMarking=1;
						typeRecognitionDistance=0;
						color[]={1,1,1,0};
					};
				};
			};
		};
	};
};