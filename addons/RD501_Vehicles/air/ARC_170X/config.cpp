#include "../../../RD501_main/config_macros.hpp"
class CfgPatches
{
    class RD501_patch_ARC_170X
    {
        addonRootClass = "RD501_patch_vehicles";
        requiredAddons[]=
        {
            "RD501_patch_vehicles",
            "3as_arc_170"
        };
        requiredVersion=0.1;
        units[]=
        {
            "RD501_arc_170_blue",
            "RD501_arc_170_razor",
            "RD501_arc_170_red"
        };
        weapons[]={};
    };
};
class SensorTemplatePassiveRadar;
class SensorTemplateAntiRadiation;
class SensorTemplateActiveRadar;
class SensorTemplateIR;
class SensorTemplateVisual;
class SensorTemplateMan;
class SensorTemplateLaser;
class SensorTemplateNV;
class SensorTemplateDataLink;
class DefaultVehicleSystemsDisplayManagerLeft
{
    class components;
};
class DefaultVehicleSystemsDisplayManagerRight
{
    class components;
};
class VehicleSystemsTemplateLeftPilot: DefaultVehicleSystemsDisplayManagerLeft
{
    class components;
};
class VehicleSystemsTemplateRightPilot: DefaultVehicleSystemsDisplayManagerRight
{
    class components;
};

class CfgVehicles
{
	class 3AS_ARC_170_Base;
	class 3as_arc_170_blue: 3AS_ARC_170_Base
	{
		class Components;
		class ViewPilot;
		class Turrets;
	};
	class RD501_arc_170_blue: 3as_arc_170_blue
	{
		displayName = "ARC-170 (Blue)";
		hiddenselections[] = {"camo1","camo2","guns"};
		hiddenselectionstextures[] =
		{
			"3as\3AS_ARC170\Data\units\501st_Main_Frame_CO.paa",
			"3as\3AS_ARC170\Data\units\501st_Wings_Engines_CO.paa",
			"3as\3AS_ARC170\Data\units\501st_Guns_CO.paa"
		};
	
		scope = 2;
		scopeCurator = 2;
		scopeArsenal = 2;
		stallSpeed=5;
		allowTabLock = 1;
		irTarget = 1;
		irTargetSize = 1;
		radarTarget = 1;
		radarTargetSize = 1;
		draconicTorqueYCoef[]={0,1.5,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
		draconicForceYCoef=6;
		maxSpeed=1000;
		altFullForce = 16000;
		altNoForce = 19000;
		thrustCoef[] = {2.0,2.0,2.2,2.4,2.7,2.7,3,3.2,3.5,3.2,3.1,2.5,2.3,2.0,1.7,1.5,1.5};
		envelope[] = {0.1,0.2,0.3,1.5,2,2.69,3.87,5.27,6.89,8.72,9.7,9.6,9.2,8.5,8.2,8};
		faction = "RD501_Republic_Faction";
		editorSubcategory = "RD501_Editor_Category_Air_Republic_vtol";
		soundSetSonicBoom[] = {"Plane_Fighter_SonicBoom_SoundSet"};
		threat[] = {.1, .6, .7};
		cost= 10000;
		weapons[]=
		{
			"RD501_CMFlareLauncher",
			"RD501_Republic_Aircraft_Laser_Turbo",
			"RD501_Republic_Aircraft_Laser_AA"
		};
		magazines[]=
		{
			"RD501_CMFlare_Magazine_200",
			"RD501_CMFlare_Magazine_200",
			"RD501_Republic_Aircraft_Laser_Turbo_Mag_15",
			"RD501_Republic_Aircraft_Laser_AA_Mag_600"
		};

		RD501_magclamp_large_offset[]={0.0,0.0,-3.0};
		RD501_magclamp_small_offset[]={0.0,0.0,-0.5};
		class pilotCamera
		{
			class OpticsIn
			{
				class Wide
				{
					opticsDisplayName="WFOV";
					initAngleX=0;
					minAngleX=-10;
					maxAngleX=90;
					initAngleY=0;
					minAngleY=-90;
					maxAngleY=90;
					initFov=0.425;//"(30 / 120)";
					minFov=0.425;//"(30 / 120)";
					maxFov=0.425;//"(30 / 120)";
					thermalMode[] = {0,1,2,3,4,5};
					visionMode[]=
					{
						"Normal",
						"NVG",
						"Ti"
					};
					gunnerOpticsModel="\A3\Drones_F\Weapons_F_Gamma\Reticle\UAV_Optics_Gunner_wide_F.p3d";
					opticsPPEffects[]=
					{
						"OpticsCHAbera2",
						"OpticsBlur2"
					};
				};

				class zoomx4: Wide
				{
					opticsDisplayName="NFOV";
					initFov="(0.425/4)";//"(3.75 / 120)";
					minFov="(0.425/4)";//"(3.75 / 120)";
					maxFov="(0.425/4)";//"(3.75 / 120)";
					gunnerOpticsModel="\A3\Drones_F\Weapons_F_Gamma\Reticle\UAV_Optics_Gunner_narrow_F.p3d";
				};

				class zoomX8: Wide
				{
					opticsDisplayName="NFOV";
					initFov="(0.42/8)";//"(.375 / 120)";
					minFov="(0.42/8)";//"(.375 / 120)";
					maxFov="(0.42/8)";//"(.375 / 120)";
					gunnerOpticsModel="\A3\Drones_F\Weapons_F_Gamma\Reticle\UAV_Optics_Gunner_narrow_F.p3d";
				};
				class zoomX20: Wide
				{
					opticsDisplayName="NFOV";
					initFov="(0.42/20)";//"(.375 / 120)";
					minFov="(0.42/20)";//"(.375 / 120)";
					maxFov="(0.42/20)";//"(.375 / 120)";
					gunnerOpticsModel="\A3\Drones_F\Weapons_F_Gamma\Reticle\UAV_Optics_Gunner_narrow_F.p3d";
				};
				class zoomX50: Wide
				{
					opticsDisplayName="NFOV";
					initFov="(0.42/50)";//"(.375 / 120)";
					minFov="(0.42/50)";//"(.375 / 120)";
					maxFov="(0.42/50)";//"(.375 / 120)";
					gunnerOpticsModel="\A3\Drones_F\Weapons_F_Gamma\Reticle\UAV_Optics_Gunner_narrow_F.p3d";
				};

				class zoomX70: Wide
				{
					opticsDisplayName="NFOV";
					initFov="(0.42/70)";//"(.375 / 120)";
					minFov="(0.42/70)";//"(.375 / 120)";
					maxFov="(0.42/70)";//"(.375 / 120)";
					gunnerOpticsModel="\A3\Drones_F\Weapons_F_Gamma\Reticle\UAV_Optics_Gunner_narrow_F.p3d";
				};

				showMiniMapInOptics=1;
				showUAVViewInOptics=0;
				showSlingLoadManagerInOptics=1;
			};

			minTurn=-180;
			maxTurn=180;
			initTurn=0;

			minElev=-10;
			maxElev=90;
			initElev=-10;

			maxXRotSpeed=0.30000001;
			maxYRotSpeed=0.30000001;
			pilotOpticsShowCursor=1;
			controllable=1;
		};
		class Turrets: Turrets
		{
			delete LaserPilot;
			delete Reargun;
		};
		class ACE_SelfActions
		{
			class ACE_Passengers
			{
				condition = "alive _target";
				displayName = "Passengers";
				insertChildren = "_this call ace_interaction_fnc_addPassengersActions";
				statement = "";
			};
			#include "../../common/universal_hud_color_changer.hpp"
		};
		class ViewPilot: ViewPilot
		{
			initAngleX = 0;
		};
		class Components: Components
		{
			class VehicleSystemsDisplayManagerComponentLeft: VehicleSystemsTemplateLeftPilot
			{
				class Components: components
				{
					class EmptyDisplay
					{
						componentType = "EmptyDisplayComponent";
					};
					class MinimapDisplay
					{
						componentType = "MinimapDisplayComponent";
						resource = "RscCustomInfoAirborneMiniMap";
					};
					class CrewDisplay
					{
						componentType = "CrewDisplayComponent";
						resource = "RscCustomInfoCrew";
					};
					class UAVDisplay
					{
						componentType = "UAVFeedDisplayComponent";
					};
					class VehiclePrimaryGunnerDisplay
					{
						componentType = "TransportFeedDisplayComponent";
						source = "PrimaryGunner";
					};
					class VehicleMissileDisplay
					{
						componentType = "TransportFeedDisplayComponent";
						source = "Missile";
					};
					class SensorDisplay
					{
						componentType = "SensorsDisplayComponent";
						range[] = {4000,8000,16000,32000};
						resource = "RscCustomInfoSensors";
					};
				};
			};
			class VehicleSystemsDisplayManagerComponentRight: VehicleSystemsTemplateRightPilot
			{
				defaultDisplay = "SensorDisplay";
				class Components: components
				{
					class EmptyDisplay
					{
						componentType = "EmptyDisplayComponent";
					};
					class MinimapDisplay
					{
						componentType = "MinimapDisplayComponent";
						resource = "RscCustomInfoAirborneMiniMap";
					};
					class CrewDisplay
					{
						componentType = "CrewDisplayComponent";
						resource = "RscCustomInfoCrew";
					};
					class UAVDisplay
					{
						componentType = "UAVFeedDisplayComponent";
					};
					class VehiclePrimaryGunnerDisplay
					{
						componentType = "TransportFeedDisplayComponent";
						source = "PrimaryGunner";
					};
					class VehicleMissileDisplay
					{
						componentType = "TransportFeedDisplayComponent";
						source = "Missile";
					};
					class SensorDisplay
					{
						componentType = "SensorsDisplayComponent";
						range[] = {4000,8000,16000,32000};
						resource = "RscCustomInfoSensors";
					};
				};
			};

			class SensorsManagerComponent
			{
				class Components
				{
					class IRSensorComponent: SensorTemplateIR
					{
						class AirTarget
						{
							minRange=0;
							maxRange=12000;
							objectDistanceLimitCoef=-1;
							viewDistanceLimitCoef=1;
						};
						class GroundTarget
						{
							minRange=0;
							maxRange=12000;
							objectDistanceLimitCoef=-1;
							viewDistanceLimitCoef=1;
						};
						componentType = "IRSensorComponent";

						typeRecognitionDistance = 9000;
						angleRangeHorizontal = 30;
						angleRangeVertical = 30;
						maxFogSeeThrough = 0.85;

						groundNoiseDistanceCoef = -1;
						maxGroundNoiseDistance = 0;

						minSpeedThreshold = 0;
						maxSpeedThreshold = 1000;

						minTrackableSpeed = -1e10;
						maxTrackableSpeed = 1e10;

						minTrackableATL = -1e10;
						maxTrackableATL = 1e10;

						allowsMarking = 1;
						aimDown = 0;
						color[]={1,0,0,1};
					};
					
					class ActiveRadarSensorComponent: SensorTemplateActiveRadar
					{
						class AirTarget
						{
							minRange=0;
							maxRange=8000;
							objectDistanceLimitCoef=-1;
							viewDistanceLimitCoef=-1;
						};
						class GroundTarget
						{
							minRange=0;
							maxRange=8000;
							objectDistanceLimitCoef=-1;
							viewDistanceLimitCoef=-1;
						};
						componentType = "ActiveRadarSensorComponent";

						typeRecognitionDistance = 6000;
						angleRangeHorizontal = 90;
						angleRangeVertical = 135;
						maxFogSeeThrough = -1;

						groundNoiseDistanceCoef = -1;
						maxGroundNoiseDistance = 0;

						minSpeedThreshold = 0;
						maxSpeedThreshold = 1000;

						minTrackableSpeed = -1e10;
						maxTrackableSpeed = 1e10;

						minTrackableATL = -1e10;
						maxTrackableATL = 1e10;

						allowsMarking = 1;
						aimDown = 20;
						animDirection = "";
						color[]={0,1,1,1};
					};

					class AntiRadiationSensorComponent: SensorTemplateAntiRadiation
					{
						componentType="PassiveRadarSensorComponent";
						class AirTarget
						{
							maxRange = 12000;
							minRange = 12000;
							objectDistanceLimitCoef = -1;
							viewDistanceLimitCoef = -1;
						};
						class GroundTarget
						{
							maxRange = 12000;
							minRange = 12000;
							objectDistanceLimitCoef = -1;
							viewDistanceLimitCoef = -1;
						};
						typeRecognitionDistance = 20000;
						angleRangeHorizontal = 10;
						angleRangeVertical = 120;
						maxFogSeeThrough = 0.85;

						groundNoiseDistanceCoef = -1;
						maxGroundNoiseDistance = 0;

						minSpeedThreshold = 0;
						maxSpeedThreshold = 1000;

						minTrackableSpeed = -1e10;
						maxTrackableSpeed = 1e10;

						minTrackableATL = -1e10;
						maxTrackableATL = 1e10;

						allowsMarking = 1;
						aimDown = 45;
						animDirection = "";
						color[] = {0,1,1,1};
					};

					class VisualRadarSensorComponent: SensorTemplateVisual
					{
						class AirTarget
						{
							minRange	= 0;
							maxRange	= 1000;
							objectDistanceLimitCoef	= -1;
							viewDistanceLimitCoef	= -1;
						};
						class GroundTarget
						{
							minRange = 0;
							maxRange = 1000;
							objectDistanceLimitCoef = -1;
							viewDistanceLimitCoef 	= -1;
						};
						componentType = "VisualSensorComponent";

						typeRecognitionDistance = 750;
						angleRangeHorizontal = 90;
						angleRangeVertical = 90;
						maxFogSeeThrough = -1;

						groundNoiseDistanceCoef = -1;
						maxGroundNoiseDistance = 0;

						minSpeedThreshold = 0;
						maxSpeedThreshold = 1000;

						minTrackableSpeed = -1e10;
						maxTrackableSpeed = 1e10;

						minTrackableATL = -1e10;
						maxTrackableATL = 1e10;

						allowsMarking = 1;
						aimDown = 0;
						color[]={1,1,0.5,0.80000001};
					};

					class LaserSensorComponent: SensorTemplateLaser
					{
						class AirTarget
						{
							minRange	= 0;
							maxRange	= 16000;
							objectDistanceLimitCoef	= -1;
							viewDistanceLimitCoef	= -1;
						};
						class GroundTarget
						{
							minRange = 0;
							maxRange = 16000;
							objectDistanceLimitCoef = -1;
							viewDistanceLimitCoef 	= -1;
						};
						componentType="LaserSensorComponent";
						angleRangeHorizontal=360;
						angleRangeVertical=180;
						aimDown=0;
						maxFogSeeThrough = 0.85;
					};

					class NVSensorComponent: SensorTemplateNV
					{
						class AirTarget
						{
							minRange	= 0;
							maxRange	= 16000;
							objectDistanceLimitCoef	= -1;
							viewDistanceLimitCoef	= -1;
						};
						class GroundTarget
						{
							minRange = 0;
							maxRange = 16000;
							objectDistanceLimitCoef = -1;
							viewDistanceLimitCoef 	= -1;
						};
						componentType="NVSensorComponent";
						angleRangeHorizontal=360;
						angleRangeVertical=180;
						aimDown=0;
						maxFogSeeThrough = 0.85;
					};
				
					class DataLinkSensorComponent: SensorTemplateDataLink
					{
						class AirTarget
						{
							minRange	= 0;
							maxRange	= 16000;
							objectDistanceLimitCoef	= -1;
							viewDistanceLimitCoef	= -1;
						};
						class GroundTarget
						{
							minRange = 0;
							maxRange = 16000;
							objectDistanceLimitCoef = -1;
							viewDistanceLimitCoef 	= -1;
						};
						componentType="DataLinkSensorComponent";
						allowsMarking=1;
						typeRecognitionDistance=0;
						color[]={1,1,1,0};
					};
				};
			};

			class TransportPylonsComponent
			{
				UIPicture = "3as\3as_arc170\data\plane_arc_pylon_ca.paa";
				class pylons
				{
					class pylons1
					{
						hardpoints[]=
						{
							"RD501_Republic_Aircraft_Bomb_Laser_Glide_Pylon",
							"RD501_Republic_Aircraft_Missile_AA_IR_Pylon",
							"RD501_Republic_Aircraft_Missile_AT_IR_Pylon",
							"RD501_Republic_Aircraft_Missile_AT_Wire_Pylon",
							"RD501_Republic_Aircraft_Missile_AP_Pylon",
							"RD501_Republic_Aircraft_Laser_Repeater_Pylon",
							"RD501_Republic_Aircraft_Missile_AR_LR_Pylon",
							"RD501_Republic_Aircraft_Missile_ARM_SR_Pylon"
						};
						attachment = "empty";
						priority=1;
						maxweight=75;
						UIposition[] = {0.15,0.45};
					};
					class pylons2: pylons1
					{
						priority = 1;
						UIposition[] = {0.2,0.35};
					};
					class pylons3: pylons1
					{
						hardpoints[]=
						{
							"RD501_Republic_Aircraft_Bomb_Laser_Glide_Pylon",
							"RD501_Republic_Aircraft_Missile_AA_IR_Pylon",
							"RD501_Republic_Aircraft_Missile_AT_IR_Pylon",
							"RD501_Republic_Aircraft_Missile_AT_Wire_Pylon",
							"RD501_Republic_Aircraft_Missile_AP_Pylon",
							"RD501_Republic_Aircraft_Laser_Repeater_Pylon",
							"RD501_Republic_Aircraft_Missile_ARM_LR_Pylon",
							"RD501_Republic_Aircraft_Missile_AR_LR_Pylon",
							"RD501_Republic_Aircraft_Missile_ARM_SR_Pylon"
						};
						priority=1;
						maxweight=75;
						UIposition[] = {0.25,0.25};
					};
					class pylons4: pylons3
					{
						priority = 1;
						UIposition[] = {0.6,0.25};
						mirroredMissilePos = 3;
					};
					class pylons5: pylons1
					{
						
						priority = 1;
						UIposition[] = {0.65,0.35};
						mirroredMissilePos = 2;
					};
					class pylons6: pylons1
					{
						priority = 1;
						UIposition[] = {0.7,0.45};
						mirroredMissilePos = 1;
					};
				};
				class presets
				{
					class empty
					{
						displayName = "$STR_empty";
						attachment[] = {};
					};
					class CAPHARM
					{
						displayName = "CAP/HARM";
						attachment[] =
						{
							"RD501_Republic_Aircraft_Missile_AR_LR_Pylon",
							"RD501_Republic_Aircraft_Missile_AR_LR_Pylon",
							"RD501_Republic_Aircraft_Missile_ARM_LR_Pylon",
							"RD501_Republic_Aircraft_Missile_ARM_LR_Pylon",
							"RD501_Republic_Aircraft_Missile_AR_LR_Pylon",
							"RD501_Republic_Aircraft_Missile_AR_LR_Pylon"
						};
					};
				};
			};
		};

		receiveRemoteTargets = true;
		reportRemoteTargets = true;
		reportOwnPosition = true;
	};
	class RD501_arc_170_razor: RD501_arc_170_blue
	{
		displayName = "ARC-170 (Razor)";
	};
	class RD501_arc_170_red: RD501_arc_170_blue
	{
		displayName = "ARC-170 (Red)";
		hiddenSelectionsTextures[] =
		{
			"\3as\3as_arc170\Data\Main_Frame_co.paa",
			"\3as\3as_arc170\Data\Wings_Engines_co.paa",
			"\3as\3as_arc170\Data\Guns_co.paa"
		};
	};
};
