#include "../../../RD501_main/config_macros.hpp"
class CfgPatches
{
    class RD501_patch_FuelCan
    {
        addonRootClass=MACRO_QUOTE(RD501_patch_vehicles);

        requiredAddons[]=
        {
            RD501_patch_vehicles
        };
        requiredVersion=0.1;
        units[]=
        {
        };
        weapons[]=
        {
        };
    };
};

class Extended_InitPost_EventHandlers 
{
    class RD501_FuelCan 
    {
        class init 
        {
            init = [(_this select 0), 140] call ace_refuel_fnc_makeJerryCan;
        };
    };
};
class CfgVehicles
{
    class Misc_thing;
    class RD501_FuelCan : Misc_thing
    {
        displayName = "Fuel Can";
        editorPreview = "";
        editorCategory =  MACRO_QUOTE(macro_editor_cat(statics));
		editorSubcategory = MACRO_QUOTE(macro_editor_cat(static_msc));
        icon = "iconObject_5x2";
        model = "RD501_Vehicles\static\FuelCan\FuelCan.p3d";
        scope = 2;
        scopeCurator = 2;
        vehicleClass = "Misc";

        ace_cargo_size = 2;
        ace_cargo_canLoad = 1;
        ace_cargo_noRename = 1;
    };
};