//---Basic Definitions---
	//I learned all of this from this weeb mod lol https://steamcommunity.com/sharedfiles/filedetails/?id=1620525773
	#define StabilizedInAxesNone 0
	#define StabilizedInAxisX 1
	#define StabilizedInAxisY 2
	#define StabilizedInAxesBoth 3

	#define MACRO_AUTHOR "RD501"
	#define MACRO_AUTHOR_PPL "Namenai"
	#define MODNAME RD501

//---Basic Functions---
	#define CONCAT(a,b) a##b
	#define CONCAT_3(a,b,c) CONCAT(a,CONCAT(b,c))
	#define CONCAT_4(a,b,c,d) CONCAT(a,CONCAT_3(b,c,d))
	#define CONCAT_5(a,b,c,d,e) CONCAT(a,CONCAT_4(b,c,d,e))
	#define CONCAT_6(a,b,c,d,e,f) CONCAT(a,CONCAT_5(b,c,d,e,f))
	#define CONCAT_7(a,b,c,d,e,f,g) CONCAT(a,CONCAT_6(b,c,d,e,f,g))
	
	#define UNDERSCORE_CONCAT(a,b) CONCAT_3(a,_,b)
	#define ADDON UNDERSCORE_CONCAT(MODNAME,COMPONENT)

	#define ADDON_NICE Research & Development 501
	#define DANKAUTHORS RD501

	//Adds the mod prefix to watever, used for classnames and such
	#define ADD_PREFIX(NAME)UNDERSCORE_CONCAT(MODNAME,NAME)

	#define concat_function(var) UNDERSCORE_CONCAT(ADDON,var)

	// #define quote_this(thing) #thing
	#define macro_single_quote(thing) '##thing##'
	#define MACRO_QUOTE(thing) #thing

	#define macro_new_config_prop(name) CONCAT_3(MODNAME,_,name)

	//Faction names,classes,and catagories
	#define macro_republic_faction CONCAT(MODNAME,_Republic_Faction)
	#define macro_rebel_faction CONCAT(MODNAME,_Rebel_Faction)
	#define macro_resistance_faction CONCAT(MODNAME,_Resistance_Faction)

	#define macro_cis_faction CONCAT(MODNAME,_CIS_Faction)
	#define macro_empire_faction CONCAT(MODNAME,_Empire_Faction)
	#define macro_faction(name) CONCAT_4(MODNAME,_,name,_Faction)

	#define macro_first_order_faction CONCAT(MODNAME,_First_Order)
	#define macro_ind_faction CONCAT(MODNAME,_Independent_Faction)
	#define macro_civ_faction CONCAT(MODNAME,_Civ_Faction)

	#define macro_editor_cat(name) CONCAT_3(MODNAME,_Editor_Category_,name)
	#define macro_editor_cat_air(name) CONCAT_3(MODNAME,_Editor_Category_Air_,name)

	#define macro_editor_vehicle_type(name) CONCAT_3(MODNAME,_Vehicle_Class_,name)
	#define macro_editor_vehicle_type_air(name) CONCAT_3(MODNAME,_Air_Vehicle_Class_,name)

//---Functions---
	#define macro_mod_script_path RD501_Main\functions
	#define post_init_fnc(name)\
				class name\
				{\
					postInit = 1;\
					file = function_path(name)	\
				};

	#define normal_fnc(name)\
				class name\
				{\
					file = function_path(name)	\
				};

	#define function_path(scriptname) MACRO_QUOTE(CONCAT(ADDON,\scripts\scriptname.sqf))

	#define macro_hud_changer_interaction_icons RD501_Main\textures\interaction_Icons

	#define macro_fnc_name(title) CONCAT_3(MODNAME,_fnc_,title)
	#define macro_grp_fnc_name(grp,title) CONCAT_5(MODNAME,_,grp,_fnc_,title)

	#define macro_preprocess_fnc(file)\
	call compileFinal preprocessFileLineNumbers 'COMPONENT##\##file'
	


//---Patches---
	#define macro_patch_name(component) CONCAT_3(MODNAME,_patch_,component)
	#define macro_A3_patch MACRO_QUOTE(A3_Data_F_Tank_Loadorder)
	#define macro_root_req MACRO_QUOTE(RD501_patch_main)
	#define macro_lvl2_req MACRO_QUOTE(RD501_patch_weapons), MACRO_QUOTE(RD501_patch_jumppack), MACRO_QUOTE(RD501_patch_helmets), MACRO_QUOTE(RD501_patch_vehicle_weapons)
	#define macro_lvl3_req MACRO_QUOTE(RD501_patch_units), MACRO_QUOTE(RD501_patch_vehicles), MACRO_QUOTE(RD501_patch_zeus)
	#define macro_lvl1_req MACRO_QUOTE(RD501_patch_particle_effects), MACRO_QUOTE(RD501_patch_emp)
	#define macro_lvl4_req MACRO_QUOTE(RD501_patch_droids_config), MACRO_QUOTE(macro_patch_name(laat_variants))
	#define macro_lvl5_req MACRO_QUOTE(macro_patch_name(legacy_classnames))
	#define macro_end_patches macro_lvl5_req

	

//---Weapons, Mags, and Ammo
	
	#define macro_scope_magnification(zoom) 0.25/zoom

	#define macro_new_scope(scopename) CONCAT_3(MODNAME,_weapon_scope_,scopename)

	#define macro_new_weapon(family,name) CONCAT_5(MODNAME,_,family,_,name)

	#define macro_new_weapon_nofam(name) CONCAT_3(MODNAME,_,name)

	#define macro_new_ammo(name) CONCAT_4(MODNAME,_,name,_ammo)

	#define macro_new_mag(name,count) CONCAT_6(MODNAME,_,name,_x,count,_mag)

	#define macro_new_magwell(name) CONCAT_4(MODNAME,_,name,_magwell)

	#define macro_new_magwell_array(name) CONCAT_4(MODNAME,_,name,_magwell[])

	#define macro_no_muzzle_impulse muzzleImpulseFactor[] = {0.00, 0.00};

	#define macro_new_recoil(name) CONCAT_4(MODNAME,_,name,_recoil)



//---Vehicles---

	#define macro_basic_air_weapons "Laserdesignator_pilotCamera","CMFlareLauncher"
	#define macro_basic_air_mags "Laserbatteries","300Rnd_CMFlare_Chaff_Magazine","300Rnd_CMFlare_Chaff_Magazine"

	#define macro_new_vehicle(group,name) CONCAT_5(MODNAME,_,group,_,name)

//----uniforms,units,vests
	//wat we see in arsenal,weapon
	#define macro_new_uniform_class(side,name) CONCAT_5(MODNAME,_,side,_uniform_,name)
	//vests
	#define macro_new_vest_class(side,name) CONCAT_5(MODNAME,_,side,_vest_,name)
	//actualyt skin of uniforms,vehicle
	#define macro_new_uniform_skin_class(side,name) CONCAT_5(MODNAME,_,side,_uniform_skin_,name)
	//wat we see in zeus,vehicle
	#define macro_new_unit_class(side,name) CONCAT_5(MODNAME,_,side,_unit_,name)
	//backpak
	#define macro_new_backpack_class(side,name) CONCAT_6(MODNAME,_,side,_,name,_backpack)
	//compositions
	#define macro_new_composition(side,name) CONCAT_5(MODNAME,_,side,_composition_,name)

	//backpack presets
	#define macro_b1_at_backpack_inv \
	class _xx_MAAWS_HE \
	{\
		magazine = "MRAWS_HE_F";\
		count = 3;\
	};\
	class _xx_MAAWS_HEAT\
	{\
		magazine = "MRAWS_HEAT_F";\
		count = 2;\
	};


	#define macro_b1_aa_backpack_inv \
			class _xx_Titan_AA\
			{\
				magazine = "Titan_AA";\
				count = 6;\
			};


//---Ordanance---
	#define macro_new_ordnance(name) CONCAT_3(MODNAME,_Ordnance_,name)
	#define macro_new_ordnance_ammo(name) CONCAT_3(MODNAME,_Ordnance_Ammo_,name)

//---helmets
	#define macro_custom_helmet_textures \RD501_Helmets\_textures
	#define macro_new_helmet(group,name) CONCAT_5(MODNAME,_,group,_helmet_,name)
	#define macro_new_helmet_jlts(group,name) CONCAT_5(MODNAME,_,group,_helmet_,name)

	#define macro_clone_helmet_hitpoints\
	class HitpointsProtectionInfo\
	{\
		class Head\
		{\
			hitpointName = "HitHead";\
			armor = 50;\
			passThrough = 0.6;\
		};\
	};

	#define macro_clone_other_armor_values\
		explosionShielding = 2.2;\
		minimalHit = 0.01;\
		passThrough = 0.01;

	#define macro_ace_hearing_helmet_setting\
	ace_hearing_protection = 0.85; \
	ace_hearing_lowerVolume = 0.6; 


	#define macro_rebreather_armor_stuff\
		containerClass = "Supply100";\
		mass = 20;\
		vestType = "Rebreather";

	#define macro_clone_armor_hitpoints \
		class HitpointsProtectionInfo\
		{\
			class Neck\
			{\
				hitpointName = "HitNeck";\
				armor = 8;\
				passThrough = 0.6;\
			};\
			class Arms\
			{\
				hitpointName = "HitArms";\
				armor = 16;\
				passThrough = 0.6;\
			};\
			class Chest\
			{\
				hitpointName = "HitChest";\
				armor = 32;\
				passThrough = 0.6;\
			};\
			class Diaphragm\
			{\
				hitpointName = "HitDiaphragm";\
				armor = 32;\
				passThrough = 0.6;\
			};\
			class Abdomen\
			{\
				hitpointName = "HitAbdomen";\
				armor = 16;\
				passThrough = 0.6;\
			};\
			class Pelvis\
			{\
				hitpointName = "HitPelvis";\
				armor = 16;\
				passThrough = 0.6;\
			};\
			class Body\
			{\
				hitpointName = "HitBody";\
				passThrough = 0.6;\
			};\
		};

//---textures
	#define macro_unit_skin_textures RD501_Units\textures
	#define macro_vehicle_textures RD501_Vehicles\textures
	#define macro_b1_textures MRC\JLTS\characters\DroidArmor\data


//---XEH prep---
	#define macro_prep_xeh(SCRIPT,name)\
	[MACRO_QUOTE(macro_mod_script_path\SCRIPT),MACRO_QUOTE(macro_fnc_name(name))] call CBA_fnc_compileFunction;

//---particle efffects---
	#define macro_new_cloudlet(name) CONCAT_3(MODNAME,_cloudlet_,name)
	#define macro_new_complex_effect(name) CONCAT_3(MODNAME,_particle_effect_,name)
	#define macro_new_light(name) CONCAT_3(MODNAME,_light_,name)

//---NVG----
	#define macro_thermal_nvg_default \
	thermalMode[] = {8};
