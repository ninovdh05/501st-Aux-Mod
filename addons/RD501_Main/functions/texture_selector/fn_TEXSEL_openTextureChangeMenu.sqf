#include "..\loglevel.hh"

params ["_vic"];

// Local UI so this needs to run where player is not null.

if (isDedicated || isNull player) exitWith {
	["Open Texture Change Menu can not be called on a server/when the player is null.", LOG_WARN, "TEXSEL"] call RD501_fnc_logMessage;
};

// Load config data
private _vicType = (configFile >> "CfgVehicles" >> typeOf _vic);
if (_vicType isEqualTo "") exitWith {
	[["No config was detected for", _vic, "- Unable to open menu."] joinString " ", LOG_ERROR, "TEXSEL"] call RD501_fnc_logMessage;
};

private _texSelCfg = _vicType >> "RD501_Texture_Selector";
if (not isClass _texSelCfg) exitWith {
	[["No RD501 Texture Selector config was detected for", _vic, "- aborting."] joinString " ", LOG_WARN, "TEXSEL"] call RD501_fnc_logMessage;
};

private _options = "true" configClasses _texSelCfg;

private _eligibleConfigs = createHashMap;
{
	if (isText (_x >> "title") && isArray (_x >> "decals")) then {
		private _title = getText (_x >> "title");
		private _decals = getArray (_x >> "decals");
		private _submenu = [_x, "submenu", "Ungrouped"] call BIS_fnc_returnConfigEntry;
		private _restrictRegex = [_x, "restrictNameRegex", ""] call BIS_fnc_returnConfigEntry;

		private _add = true;
		if (not (_restrictRegex isEqualTo "")) then {
			if (not ((name player) regexMatch _restrictRegex)) then {
				[["Regex failed to match", _title, "for", name player] joinString " ", LOG_INFO, "TEXSEL"] call RD501_fnc_logMessage;
				_add = false;
			};
		};

		if (_add) then {
			private _cfgName = configName _x;

			if (not (_submenu in _eligibleConfigs)) then {
				_eligibleConfigs set [_submenu, []];
			};

			(_eligibleConfigs get _submenu) pushBack [_title, _decals, _cfgName];
			
			if (not ("All" in _eligibleConfigs)) then {
				_eligibleConfigs set ["All", []];
			};

			(_eligibleConfigs get "All") pushBack [_title, _decals, _cfgName];

			[["Added", _title, "to texture options for", name player] joinString " ", LOG_INFO, "TEXSEL"] call RD501_fnc_logMessage;
		};
	} else {
		[["Config", _x, "is missing a title and decals field - skipping."] joinString " ", LOG_WARN, "TEXSEL"] call RD501_fnc_logMessage;
	};
} forEach _options;

// Save these configs as a local variable - we are going to need it later.
localNamespace setVariable ["RD501_TEXSEL_EligibleTextureConfigurations", _eligibleConfigs];

// Load default configurations from the profile namespace
private _defaultConfigTree = profileNamespace getVariable ["RD501_TEXSEL_DefaultTextureConfigurations", createHashMap];
private _defaultConfigs = _defaultConfigTree getOrDefault [typeOf _vic, []];

createDialog ["RD501_TEXSEL_TextureSelectionMenu", true];
private _diag = findDisplay 501001;
private _groupSelector = _diag displayCtrl 001;
private _textureSelector = _diag displayCtrl 002;
private _defaultsSelector = _diag displayCtrl 005;
[[_diag, _groupSelector, _textureSelector, _defaultsSelector] joinString " ", LOG_TRACE, "TEXSEL"] call RD501_fnc_logMessage;
// Clear group selector, texture selector, and defaults selector.
lbClear _groupSelector;
lbClear _textureSelector;
lbClear _defaultsSelector;

private _groups = [];
// Order the groupings ...
{
	if (not (_x isEqualTo "All") && not (_x isEqualTo "Ungrouped")) then {
		_groups pushBack _x;
	};
} forEach _eligibleConfigs;

_groups sort true;

// Put all at the front ...
_groups = ["All"] + _groups;
// ... and ungrouped last.
_groups pushBack "Ungrouped";

{
	// Add submenu groups ...
	_groupSelector lbAdd _x;
} forEach _groups;

[["Added groups", _groups, "to the group combo box."] joinString " ", LOG_DEBUG, "TEXSEL"] call RD501_fnc_logMessage;

{
	// Add defaults entries ...
	_defaultsSelector lbAdd (_x select 0);
} forEach _defaultConfigs;

[["Added defaults", _defaultConfigs, "to the default selection box."] joinString " ", LOG_DEBUG, "TEXSEL"] call RD501_fnc_logMessage;

// Select All for the group selector
_groupSelector lbSetCurSel 0;
