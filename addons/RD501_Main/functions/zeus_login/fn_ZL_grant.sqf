params ["_player"];

if(isServer) then {

	if(!RD501_ZL_Enabled) exitWith{ diag_log "ZL Disabled"; };

	private _curModule = missionnamespace getVariable ["RD501_ZL_curatorModule", objNull];

	if(isNull _curModule) then {
		diag_log text (["[RD501]", "[ZEUS LOGIN]", "DEBUG:", "No curator object found, spawning new one."] joinString " ");

		_curModule = (createGroup west) createUnit ["ModuleCurator_F", position _player, [], 0, "CAN_COLLIDE"];
		missionnamespace setVariable ["RD501_ZL_curatorModule", _curModule, true];
	};

	diag_log text (["[RD501]", "[ZEUS LOGIN]", "DEBUG:", "Assigning", _player, "to curator object", _curModule] joinString " ");

	_player assignCurator _curModule;
	["RD501_3DEN_event_ZM_setMarkerVisibilty", [1], _player] call CBA_fnc_targetEvent;

	[_player, "logged into zeus."] remoteExecCall ["RD501_fnc_ZL_globalMessage", 0];
};