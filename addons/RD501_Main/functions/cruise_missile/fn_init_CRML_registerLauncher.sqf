// A special function to register a launcher intended to be placed within
// the init section of an object in Eden.
params [
	// The launcher object.
	["_launcher", objNull],
	// True if this launcher should never run out of ammo.
	["_refreshAmmo", true],
	// The # of seconds between each allowed missile launch.
	["_reloadSpeed", 15]
];

if (isServer) then {
	if (isNil "RD501_CRML_LauncherNodes") then {
		RD501_CRML_LauncherNodes = [];
		publicVariable "RD501_CRML_LauncherNodes";
	};

	[_launcher, _refreshAmmo, _reloadSpeed] call RD501_fnc_internal_CRML_registerLauncher;
};
