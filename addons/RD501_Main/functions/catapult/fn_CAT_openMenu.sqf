// Opens the catapult launch menu.

createDialog "RD501_CAT_LaunchController";

if(localNamespace getVariable ["RD501_CAT_firstOpen", false]) then {
	[] call RD501_fnc_CAT_loadMenuDefaults;
	localNamespace setVariable ["RD501_CAT_firstOpen", true];
} else {
	private _vals = localNamespace getVariable ["RD501_CAT_launchSettings", []];

	if ((count _vals) == 0) then {
		[] call RD501_fnc_CAT_loadMenuDefaults;
	} else {
		private _pads = localNamespace getVariable ["RD501_CAT_catapultList", []];
		lbClear 1515; 
		{ 
			lbAdd [1515, (_x select 1)];
		} forEach (_pads);
		
		// Acceleration
		ctrlSetText [1501, format ["%1", (_vals select 2)]];
		// Speed
		ctrlSetText [1502, format ["%1", (_vals select 3)]];
		// Pitch
		ctrlSetText [1503, format ["%1", (_vals select 4)]];
		// Distance
		ctrlSetText [1504, format ["%1", (_vals select 6)]];

		// Hover
		ctrlSetText [1511, format ["%1", (_vals select 5)]];
		// Rotation
		ctrlSetText [1512, format ["%1", (_vals select 1)]];
		// Hover Cycles
		ctrlSetText [1513, format ["%1", (_vals select 7)]];
		// Sleep Timer
		ctrlSetText [1514, format ["%1", (_vals select 8)]];
	};
};