// Registers a catapult pad.
params [
    // The pad object to register. Finds Vics near this pos.
    "_pad",
    // The display name of this pad.
    "_disp"
];

private _catList = localNamespace getVariable ["RD501_CAT_catapultList", []];

_catList pushBack [_pad, _disp];

localNamespace setVariable ["RD501_CAT_catapultList", _catList];