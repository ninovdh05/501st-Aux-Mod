#include "functions\function_macros.hpp"


LOG("PreInit Begin");
private _addonConfig = configFile >> "CfgPatches" >> QUOTE(ADDON);
private _version = getArray(_addonConfig >> "version");
LOG(format["Version: %1",_verison]);

LOG("PREP Begin");
#include "XEH_PREP.sqf"
LOG("PREP Complete");

LOG("Searching in config for vehicles to add action to");
private _validTypesFilter = format["isClass (_x >> '%1')", QUOTE(ADDON)];
GVAR(validTypes) = (_validTypesFilter configClasses (configFile >> "CfgVehicles")) apply { configName _x };

private _configSpecified = getArray(_addonConfig >> "externalTypes");
GVAR(validTypes) append _configSpecified;

{
	LOGF_2("%1: %2", _foreachIndex, _x);
} forEach(GVAR(validTypes));

private _typesCount = count GVAR(validTypes);
if(_typesCount > 0) then {
	LOGF_1("Found %1 placeable objects", _typesCount);
}
else {
	LOG_ERRORF_1("Found no objects in CfgVehicles which had class %1 {} defined", QUOTE(ADDON));
};

[QGVAR(UpdateRate),
        "SLIDER",
        [
                "Update Rate",
                "Interval, in seconds, between awacs display updates."
        ],
        ["RD501", "AWACS"],
		[0.5, 10, 1, 1, false],
		2
] call CBA_fnc_addSetting;

LOG("PreInit Complete");