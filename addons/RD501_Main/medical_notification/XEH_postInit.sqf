#include "functions\function_macros.hpp"

if(!isPlayer this) exitWith { 
        diag_log format["RD501_MedNotif[DEBUG]: %1", "Current instance is not a player"]
};

GVAR(rateLimitTime) = 0;
_id = ["ace_medicalMenuOpened", FUNC(sendMedicalMessage)] call CBA_fnc_addEventHandler;