#include "function_macros.hpp"

params["_configEntry", "_parentClass"];

if (!isClass _configEntry) exitWith {
	LOGF_1("%1 is not a config class");
};

while { !isNull _configEntry } do {
	if ((configName _configEntry) isEqualTo _parentClass) exitWith {};
	_configEntry = inheritsFrom _configEntry;
};

!isNull _configEntry
