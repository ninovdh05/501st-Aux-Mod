/*
 * Author: M3ales
 * This function displays the jumppack energy, selected jump type, and any optional error or jump type information.
 *
 * Arguments:
 * 0: Unit to display the information for <OBJECT>
 * 1: Error message (Optional) <STRING>
 * 2: Jump type index (Optional) <NUMBER>
 *
 * Return Value:
 * None
 *
 * Example:
 * [player] call rd501_jumppack_fnc_showState;
 * [player, "Error message"] call rd501_jumppack_fnc_showState;
 * [player, "", 2] call rd501_jumppack_fnc_showState;
 *
 * Public: Yes
 */

#include "function_macros.hpp"

params["_unit", ["_error", ""], ["_jumpType", -1]];

private _jumppack = [_unit] call FUNC(getJumppack);
if (_jumppack isEqualTo false) exitWith {
	[
		"No jumppack equipped",
		false
	] call ACE_common_fnc_displayText;
};
_jumppack params ["_class", "_capacity", "_rechargeRate"];

[] call FUNC(displayHud);

private _backpack = (unitBackpack _unit);
private _currentEnergy = _backpack getVariable [QGVAR(energy), 0];

private _energyText = format["Energy %1/%2\n", _currentEnergy, _capacity];

private _cycleInfo = "";
if (GVAR(enableCycle)) then {
	_cycleInfo = format["Selected: %1\n", GVAR(jumpTypes) select GVAR(selectedJumpType) select 1];
};

private _jumpInfo = "";
if (_jumpType isNotEqualTo -1) then {
	_jumpInfo = format["%1\n", GVAR(jumpTypes) select _jumpType select 1];
};

private _text = format["%1%2%3%4", _energyText, _cycleInfo, _jumpInfo, _error];
/* [
	_text,
	false
] call ACE_common_fnc_displayText;
*/
