/*
 * Author: M3ales
 * Function to cycle through jump types for a player's jumppack.
 *
 * Arguments:
 * 0: Cycling direction <NUMBER> (positive number for forward, negative for backward)
 *
 * Example:
 * [1] call rd501_jumppack_fnc_cycleJumpType;
 *
 * Public: No
 */

#include "function_macros.hpp"

params ["_direction"];

_direction = _direction / abs _direction;
private _unit = player;

if (!GVAR(enableCycle)) exitWith {
	[_unit, "Cycling is disabled."] call FUNC(showState);
	LOG_ERROR("Cycling of jump types is disabled in the addon settings.");
};

private _jumppack = [_unit] call FUNC(getJumppack);
if (_jumppack isEqualTo false) exitWith {
	LOG("Player does not have a jumppack equipped");
	[_unit, "You do not have a jumppack equipped."] call FUNC(showState);
};
_jumppack params ["_name", "_capacity", "_rechargeRate", "_displayName", "_picture", "_allowedJumpTypes"];

if (_allowedJumpTypes isEqualTo []) exitWith {
	LOG("Jumppack does not allow any jump types");
	GVAR(selectedJumpType) = -1;
	[_unit, "This jumppack does not endorse the act of jumping", GVAR(selectedJumpType)] call FUNC(showState);
};

private _countAllowedAfterHidden = {
	((GVAR(jumpTypes) select _x) select 6) isEqualTo false
} count _allowedJumpTypes;
if (_countAllowedAfterHidden isEqualTo 0) exitWith {
	LOG("All available jump types are either disallowed or hidden from the cycle");
	GVAR(selectedJumpType) = -1;
	[_unit, "No jump types available for cycling. All jump types are hidden or unsupported.", GVAR(selectedJumpType)] call FUNC(showState);
};

private _jumpTypeCount = count GVAR(jumpTypes);
private _nextJumpType = GVAR(selectedJumpType) + _direction;

_nextJumpType = [_nextJumpType, _jumpTypeCount] call FUNC(cycleWrap);

private _recursionGuard = _nextJumpType;

private _fnc_handleRecursionLimit = {
	params ["_nextJumpType", "_unit", "_allowedJumpTypes"];

	LOGF_1("Recursion limit reached when trying to cycle from %1, keeping the current selection", GVAR(selectedJumpType));
	if (!(GVAR(selectedJumpType) in _allowedJumpTypes)) then {
		LOG("All jump types are either disallowed or hidden from the cycle");
		[_unit, "All jump types are either disallowed or hidden from the cycle", _nextJumpType] call FUNC(showState);
	};
};

while { [_nextJumpType] call FUNC(isHiddenFromCycle) } do {
	_nextJumpType = _nextJumpType + _direction;
	if (_nextJumpType isEqualTo _recursionGuard) exitWith {
		[_nextJumpType, _unit, _allowedJumpTypes] call _fnc_handleRecursionLimit;
	};
	_nextJumpType = [_nextJumpType, _jumpTypeCount] call FUNC(cycleWrap);
};

LOGF_2("Cycling jump type from %1 to %2", GVAR(selectedJumpType), _nextJumpType);

if ([_unit, _nextJumpType] call FUNC(trySetJumpType) isEqualTo false) then {
	LOG_ERRORF_1("Failed to set jump type to %1 using cycle", _nextJumpType);
};

[_unit] call FUNC(showState);
