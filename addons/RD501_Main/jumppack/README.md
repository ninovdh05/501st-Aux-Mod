# Jumppacks

Allows infantry to fly around, with a burst of energy in a direction rather than a typical jetpack that provides continuous thrust. Prevents fall damage for anyone wearing the backpack.

## Jumppack Configuration

### Enable Jumppack

Define `rd501_jumppack` class on the backpack you'd like to mark as a jumppack.

Where to define: `CfgVehicles -> Backpack class`.

```cpp
class rd501_jumppack {
    capacity = 5;
    rechargeRateSecond=1;
};
```

### Jumppack Capacity

The amount of energy the jumppack can hold at maximum.

Where to define: `CfgVehicles -> Backpack class -> rd501_jumppack`.

`capacity=number`

> `number` should be a value > 0, no support is given for logic less than 0, and this may eventually cause unexpected behaviour.

Default: `0`

### Jumppack Recharge Rate

The amount of energy regenerated every second.

Where to define: `CfgVehicles -> Backpack class -> rd501_jumppack`.

`rechargeRateSecond=number`

> `number` should be a number. If the number is negative, the energy will be drained every second. If the rate is 0, then the energy will never be regenerated.

Default: `0`

### Allowed Jump Types

`allowedJumpTypes[]` is a whitelist of jump type classnames of which jump types are supported by the jumppack. If empty, it means the jumppack doesn't allow any jump types. If it is not defined, it defaults to allowing no jump types.

Where to define: `CfgVehicles -> Backpack class -> rd501_jumppack`.

```cpp
allowedJumpTypes[] = {"JumpTypeClassName1", "JumpTypeClassName2"};
```

> `JumpTypeClassName1` and `JumpTypeClassName2` should be replaced with the desired jump type class names that you want the jumppack to support.

Default: `{}` (None allowed)

## Jump Types Configuration

### How to define

The behaviour of the jumppack can be modified by the available jump types, using config classes for each 'type'.

Where to define `JumpTypes`.

```cpp
class JumpTypes {
    class JumpTypeClassName {
        displayName="DisplayName",
        forward = 0;
        vertical = 0;
        cost = 5;
        hideFromCycle = 1;
        relativeToCamera = 1;
        generateExecuteKeybind = 1;
        generateSelectKeybind = 1;
    };
};
```

### Jump Type Properties

#### Display Name

`displayName` is the text which is displayed to he user when referring to this jump type.

Default: `configName jumpTypeClassName`

#### Forward

`forward` refers to the forward component of the jump vector. This as a raw value represents the speed in metres per second at which the jump accellerates the player. 

> Forward is determined by the model of the player, so during freefall this will not be toward the camera, but rather where the player is facing. If you need to make the movement relative to the camera, make use of the `relativeToCamera=1` property.

> If set to false (unspecified), the velocity will be inherited from the player's current velocity. Conversely setting it to 0 will effectively stop movement in the forward/backward direction.

Default: `false`

#### Vertical

`vertical` refers to the vertical component of the jump vector. This as a raw value represents the speed in metres per second at which the jump accellerates the player.

> Vertical or 'up' is determined by the model of the player, so during freefall this will not be pointing above the camera, but rather up relative to the player's model. If you need to make the movement relative to the camera, make use of the `relativeToCamera=1` property.

> If set to false (unspecified), the velocity will be inherited from the player's current velocity. Conversely setting it to 0 will effectively stop movement in the up/down direction.

Default: `false`

#### Cost

`cost` refers to the amount of energy required to execute this jump type.

Default: `0`

#### Relative To Camera

`relativeToCamera` refers to the point of reference to which the jump vector is applied. If this is enabled, it will be relative to the player's camera. If not, it will instead use the player's model as the point of reference.

Default: `0`

#### Hide From Cycle

`hideFromCycle` determines if this jump type will show up in the 'cyclable options'. This is typically used in conjunction with keybind functionality.

> If set to a value > 0, it will be enabled.

Default: `0`

#### Generate Select Keybind

`generateExecuteKeybind` determines if this jump type will have a select keybind generated. This keybind will allow someone to select the jump type using a keybind rather than through the cycle wheel. They must still press the jump keybind to execute it. It will modify the position of the cycle wheel.

> If set to a value > 0, it will be enabled.

Default: `0`

#### Generate Execute Keybind

`generateExecuteKeybind` determines if this jump type will have an execute keybind generated. This keybind will allow someone to execute the jump type using a keybind rather than through the cycle wheel and jump keybind. It will not modify the position of the cycle wheel.

> If set to a value > 0, it will be enabled.

Default: `0`

## Jumppack Dispenser Configuration

Vehicles can be marked as dispensers by adding the class `rd501_jumppackDispenser`.

Where to define: `CfgVehicles -> Vehicle class`.

```cpp
class rd501_jumppackDispenser {
    jumppack="rd501_test_jumppack_01";
};
```

### Dispenser Properties

#### Jumppack

`jumppack` is the class of the backpack that will be provided. It is required to be a jumppack, and will throw an error if it is left empty or undefined. (RPT side only error)

Default: ""
