class JumpTypes {
    rechargeIntervalSeconds = 0.625; // 5/8 8 recharge rate currently so packs will update every 5 energy recharged.
    class Forward {
        displayName = "Forward Jump";
        forward = 12;
        vertical = 20;
        cost = 50;
        generateExecuteKeybind = 1;
    };

    class Short {
        displayName = "Short Jump";
        forward = 12;
        vertical = 5;
        relativeToCamera = 1;
        cost = 30;
        generateExecuteKeybind = 1;
    };

    class Dash {
        displayName = "Dash";
        forward = 20;
        vertical = 2;
        cost = 20;
        hideFromCycle = 1;
        generateExecuteKeybind = 1;
    };

    class Cancel {
        displayName = "Stop";
        forward = 0;
        vertical = 0;
        cost = 5;
        hideFromCycle = 1;
        generateExecuteKeybind = 1;
    };
};
