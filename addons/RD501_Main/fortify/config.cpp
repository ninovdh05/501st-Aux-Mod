#include "config_macros.hpp"
class CfgPatches {
	class ADDON
	{
        addonRootClass = "rd501_main";
		version[] = { 1, 0, 0, 0 }; //Negative last digit indicates alpha/beta
		requiredAddons[] = {"cba_settings"};
		units[] = {};
		weapons[] = {};
        currencyItem = "RD501_fortify_nanobots";
        externalPlaceables[]={
            "Land_BagBunker_Small_F",
            "3as_Cover1",
            "3as_Cover2",
        };
	};
};

class Extended_PreInit_EventHandlers {
    class ADDON {
        init = QUOTE(call COMPILE_FILE(XEH_preInit));
    };
};

class Extended_PostInit_EventHandlers {
    class ADDON {
        init = QUOTE(call COMPILE_FILE(XEH_postInit));
    };
};