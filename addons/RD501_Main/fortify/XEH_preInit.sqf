#include "functions\function_macros.hpp"

LOG("PreInit Begin");
private _addonConfig = configFile >> "CfgPatches" >> QUOTE(ADDON);
private _version = getArray(_addonConfig >> "version");
LOG(format["Version: %1",_verison]);

LOG("PREP Begin");
#include "XEH_PREP.sqf"
LOG("PREP Complete");

if!(isText (_addonConfig >> "currencyItem")) exitWith {
	LOG_FATALF_1("currencyItem is not defined on CfgPatches >> %1", ADDON);
};

GVAR(currencyItem) = getText(_addonConfig >> "currencyItem");

LOG("Searching for placable objects");
private _propertyName = QGVAR(placeable);
private _placeableObjectsFilter = format["getNumber (_x >> '%1') > 0", _propertyName];
GVAR(placeableObjects) = _placeableObjectsFilter configClasses (configFile >> "CfgVehicles") apply { [_x, 0] };


private _configSpecified = getArray(_addonConfig >> "externalPlaceables") apply { [_x, 0] };
GVAR(placeableObjects) append _configSpecified;

{
	LOGF_2("%1: %2", _foreachIndex, _x select 0);
} forEach(GVAR(placeableObjects));

private _placeableCount = count GVAR(placeableObjects);
if(_placeableCount > 0) then {
	LOGF_1("Found %1 placeable objects", _placeableCount);
}
else
{
	LOG_ERRORF_1("Found no objects in CfgVehicles which had %1=1 defined", _propertyName);
};

// Add Settings to switch on/off
private _itemName = (configFile >> "CfgWeapons" >> GVAR(currencyItem) >> "displayName") call BIS_fnc_getCfgData;
[
    QGVAR(useAmmo),
    "CHECKBOX",
    ["Use item as Ammo", format["Additionally to Money, require 1 %1 to use the Fortify Tool", _itemName]],
    ["RD501", "ACE(X) Fortify Tool"],
    false,
	1 // isGlobal
] call CBA_settings_fnc_init;

[
    QGVAR(usePreset),
    "CHECKBOX",
    ["Use RD501 Fortify Preset", "If this box is checked, the RD501 Fortify Preset is loaded on mission start"],
    ["RD501", "ACE(X) Fortify Tool"],
    false,
    1, // isGlobal
    { [GVAR(placeableObjects)] call FUNC(registerPreset) }
] call CBA_settings_fnc_init;

LOG("PreInit Complete");
